ESUS SERVER A3

Este launcher para servidores es el que usamos en Esus, y que fue lanzado al poco tiempo de sacar a la luz Esus Launcher A3. 

Se encuentra en una versión estable dónde se le han corregido los fallos que han ido apareciendo, recomiendo su uso para los editores de ESUS.

CONFIGURANDO ESUS SERVER A3

Tenemos que rellenar los siguientes datos yo voy a acompañarlo con ejemplos, vosotros adaptarlo según vuestras necesidad, los ejemplos en rojo son los que no debéis de variar:

BÁSICO:

Nombre del servidor: ESUS PRIVADO
Contraseña: loquesea
Puerto:2302
Puerto de Steam: 8766
Contraseña del Administrador: loqueseaadmin
Máximo número de jugadores: 15 por ejemplo ( el número de slots que tiene nuestra misión)
Mesaje de Bienvenida: Hola jugadores.
Guardar log de consola: Lo activamos.
Modo de RPT: Lo ponemos en largo, esto sirve para ver el monitor del reporte de errores en versión extendida, cuando se esté ejecutando una misión.
Dificultad: elegimos la que queramos.
Activar Battleye: En esus no lo solemos activar, pero como queráis.
* Demás opciones a vuestra elección, con solo tener en cuenta estas es suficiente.

MODS:

Elegimos los mods necesarios para nuestra misión.

MISIONES:

Recordar empaquetar nuestra misión en forma de .pbo y alojarla dentro de nuestra carpeta de MPmisions en Arma 3. 
En esta pestaña nos saldrán todas las misiones que tengamos en esa carpeta, podemos filtrar por mapas para una búsqueda más cómoda si es que tenemos muchas.

HC:

Si no sabes que es esto consulta este post.

http://www.clanesus.com/foro/phpBB3/viewtopic.php?f=92&t=2803

Aquí pueden ocurrir dos cosas.

Si nuestra misión tiene HC:

Seleccionamos Activar HC.
Seleccionamos MODO LOCAL.

Si nuestra misión no tiene HC.

Nos Olvidamos de esta Pestaña.

CONFIGURACIÓN GENERAL DEL SERVIDOR:

Oprimimos el botón Valores de Ejemplo, nos rellenará unos datos de red para un uso general.

Rellenamos los campos de Memoria Ram, CPU, y Extra Hilos a nuestra elección.

PRESIONAMOS EL BOTÓN GUARDAR.

YA TENEMOS CONFIGURADO ESUS SERVER A3.

ENLACE DE DESCARGA
[https://krachenkoesusserver.codeplex.com/](https://krachenkoesusserver.codeplex.com/)