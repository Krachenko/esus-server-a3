﻿Imports System.Net
Imports System.IO
Imports System.Threading

Public Class CargarMods
    
    Private Sub CargarMods_Load(sender As Object, e As EventArgs) Handles MyBase.Load
    End Sub

    Private Sub ComboBox1_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ComboBox1.SelectedIndexChanged
        Dim dias As String
        dias = ComboBox1.SelectedItem.ToString
        RichTextBox1.Clear()
        If dias = "LUNES" Then
            Try
                Dim instance As WebClient = New WebClient
                Dim address As String = "https://dl.dropbox.com/s/fmixrx85uduqqa8/lunes.txt"
                Label2.Visible = True
                RichTextBox1.Text = instance.DownloadString(address)
                Label2.Visible = False
            Catch ex As WebException
                MessageBox.Show(ex.ToString)
            End Try
        End If
        If dias = "MARTES" Then
            Try
                Dim instance As WebClient = New WebClient
                Dim address As String = "https://www.dropbox.com/s/lj56r2thmzsp82y/martes.txt?raw=1"
                Label2.Visible = True
                RichTextBox1.Text = instance.DownloadString(address)
                Label2.Visible = False
            Catch ex As WebException
                MessageBox.Show(ex.ToString)
            End Try
        End If
        If dias = "MIÉRCOLES" Then
            Try
                Dim instance As WebClient = New WebClient
                Dim address As String = "https://www.dropbox.com/s/bc5t7v5ajazlcu7/miercoles.txt?raw=1"
                Label2.Visible = True
                RichTextBox1.Text = instance.DownloadString(address)
                Label2.Visible = False
            Catch ex As WebException
                MessageBox.Show(ex.ToString)
            End Try
        End If
        If dias = "JUEVES" Then
            Try
                Dim instance As WebClient = New WebClient
                Dim address As String = "https://www.dropbox.com/s/y518is09c48js2f/jueves.txt?raw=1"
                Label2.Visible = True
                RichTextBox1.Text = instance.DownloadString(address)
                Label2.Visible = False
            Catch ex As WebException
                MessageBox.Show(ex.ToString)
            End Try
        End If
        If dias = "VIERNES" Then
            Try
                Dim instance As WebClient = New WebClient
                Dim address As String = "https://www.dropbox.com/s/wcht0q0d3q08otw/viernes.txt?raw=1"
                Label2.Visible = True
                RichTextBox1.Text = instance.DownloadString(address)
                Label2.Visible = False
            Catch ex As WebException
                MessageBox.Show(ex.ToString)
            End Try
        End If
        If dias = "SÁBADO" Then
            Try
                Dim instance As WebClient = New WebClient
                Dim address As String = "https://www.dropbox.com/s/z5ecuta3t8xs8tn/sabado.txt?raw=1"
                Label2.Visible = True
                RichTextBox1.Text = instance.DownloadString(address)
                Label2.Visible = False
            Catch ex As WebException
                MessageBox.Show(ex.ToString)
            End Try
        End If
        If dias = "DOMINGO" Then
            Try

                Dim instance As WebClient = New WebClient
                Dim address As String = "https://www.dropbox.com/s/0odh9iz5lq2tl7t/domingo.txt?raw=1"
                Label2.Visible = True
                RichTextBox1.Text = instance.DownloadString(address)
                Label2.Visible = False
            Catch ex As WebException
                MessageBox.Show(ex.ToString)
            End Try
        End If
        If dias = "PERMANENTE" Then
            Try
                Dim instance As WebClient = New WebClient
                Dim address As String = "https://www.dropbox.com/s/tp1wv9fpl3rci99/s.permanente.txt?raw=1"
                Label2.Visible = True
                RichTextBox1.Text = instance.DownloadString(address)
                Label2.Visible = False
            Catch ex As WebException
                MessageBox.Show(ex.ToString)
            End Try
        End If
        If dias = "OTROS" Then
            Try
                Dim instance As WebClient = New WebClient
                Dim address As String = "https://www.dropbox.com/s/a3d4k1qpqy499ut/otros.txt?raw=1"
                Label2.Visible = True
                RichTextBox1.Text = instance.DownloadString(address)
                Label2.Visible = False
            Catch ex As WebException
                MessageBox.Show(ex.ToString)
            End Try
        End If
        If dias = "PRUEBAS" Then
            Try
                Dim instance As WebClient = New WebClient
                Dim address As String = "https://www.dropbox.com/s/7zsa1mws02pmmob/Pruebas.txt?raw=1"
                Label2.Visible = True
                RichTextBox1.Text = instance.DownloadString(address)
                Label2.Visible = False
            Catch ex As WebException
                MessageBox.Show(ex.ToString)
            End Try
        End If
        If dias = "ACADEMIA 1" Then
            Try
                Dim instance As WebClient = New WebClient
                Dim address As String = "https://www.dropbox.com/s/djz5gv25vtzmtik/academia.txt?raw=1"
                Label2.Visible = True
                RichTextBox1.Text = instance.DownloadString(address)
                Label2.Visible = False
            Catch ex As WebException
                MessageBox.Show(ex.ToString)
            End Try
        End If
        If dias = "ACADEMIA 2" Then
            Try
                Dim instance As WebClient = New WebClient
                Dim address As String = "https://www.dropbox.com/s/aio7iusa9gs788f/Academia2.txt?raw=1"
                Label2.Visible = True
                RichTextBox1.Text = instance.DownloadString(address)
                Label2.Visible = False
            Catch ex As WebException
                MessageBox.Show(ex.ToString)
            End Try
        End If
        If dias = "LOADOUT" Then
            Try
                Dim instance As WebClient = New WebClient
                Dim address As String = "https://www.dropbox.com/s/ogdckle7g2a4ryb/loadout.txt?raw=1"
                Label2.Visible = True
                RichTextBox1.Text = instance.DownloadString(address)
                Label2.Visible = False
            Catch ex As WebException
                MessageBox.Show(ex.ToString)
            End Try
        End If
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click

        Me.RichTextBox1.Lines = Me.RichTextBox1.Text.Split(New Char() {ControlChars.Lf}, _
                                                       StringSplitOptions.RemoveEmptyEntries)
        Dim lines() As String = RichTextBox1.Lines
        CheckedListBox1.Items.Clear()
        CheckedListBox1.Items.AddRange(lines)
        Dim marcar As Boolean = True
        For i As Integer = 0 To CheckedListBox1.Items.Count - 1
            CheckedListBox1.SetItemChecked(i, marcar)
        Next
        Dim elegido As String = Nothing
        steam = My.Computer.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\Wow6432Node\bohemia interactive\arma 3", "main", Nothing)
        rutaarma = steam
        exearma3server = rutaarma & "\arma3server.exe"
        exearma3 = rutaarma & "\arma3.exe"
        If IsNothing(rutaarma) Then
            Try
                rutaarma = My.Computer.Registry.GetValue("HKEY_CURRENT_USER\Software\Valve\Steam", "SteamPath", Nothing)
                rutaarma = rutaarma.Replace("/", "\")
                rutaarma = rutaarma & "\steamapps\common\Arma 3"
                exearma3server = rutaarma & "\arma3server.exe"
                exearma3 = rutaarma & "\arma3.exe"
            Catch ex As Exception
            End Try
        Else
        End If
        If RichTextBox1.Text <> "" Then
            Dim desmarcar As Boolean = False
            For i As Integer = 0 To Form1.CheckedListBox1.Items.Count - 1
                Form1.CheckedListBox1.SetItemChecked(i, desmarcar)
            Next
            For Each itemChecked In CheckedListBox1.CheckedItems
                elegido = itemChecked
                Try
                    Dim index As Integer = Form1.CheckedListBox1.FindString(elegido)
                    If (index <> -1) Then
                        Form1.CheckedListBox1.SetItemChecked(index, True)
                    End If
                Catch ex As Exception
                End Try
            Next
            Form1.ListBox1.Items.Clear()
            If Form1.CheckedListBox1.CheckedItems.Count > 0 Then
                For i As Integer = 0 To Form1.CheckedListBox1.CheckedItems.Count - 1
                    Form1.ListBox1.Items.Add(Form1.CheckedListBox1.CheckedItems(i))
                Next
            Else
                Form1.ListBox1.Items.Clear()
            End If
            Form1.ListBox2.Items.Clear()
            If Form1.ListBox1.Items.Count > 0 Then
                For i As Integer = 0 To Form1.ListBox1.Items.Count - 1
                    Form1.ListBox2.Items.Add(Form1.ListBox1.Items(i))
                Next
            Else
                Form1.ListBox2.Items.Clear()
            End If
            Form1.ListBox3.Items.Clear()
            Form1.TextBox67.Text = CheckedListBox1.CheckedItems.Count
        End If
        Dim rutaaddons As String
        For Each line In RichTextBox1.Lines
            rutaaddons = rutaarma & "\" & line
            If My.Computer.FileSystem.DirectoryExists(rutaaddons) Then
            Else
                MsgBox("El addon " & line & " no ha podido ser localizado entre sus addons  por favor revise Arma3Sync")
            End If
        Next
        Call Form1.construirparametro()
    End Sub

    Private Sub RichTextBox1_TextChanged(sender As Object, e As EventArgs) Handles RichTextBox1.TextChanged

    End Sub

    Private Sub CheckedListBox1_SelectedIndexChanged(sender As Object, e As EventArgs) Handles CheckedListBox1.SelectedIndexChanged

    End Sub


    Private Sub PegarToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles PegarToolStripMenuItem.Click
        If RichTextBox1.Text = Nothing Then
            RichTextBox1.Paste()
        Else
            RichTextBox1.Clear()
            RichTextBox1.Paste()
        End If
    End Sub

    Private Sub CopiarToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles CopiarToolStripMenuItem.Click
        If RichTextBox1.Text = Nothing Then
        Else
            RichTextBox1.Copy()
        End If
    End Sub

    Private Sub CortarToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles CortarToolStripMenuItem.Click
        If RichTextBox1.Text = Nothing Then
        Else
            RichTextBox1.Cut()
        End If
    End Sub
End Class