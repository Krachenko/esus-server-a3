﻿Imports System.Net
Imports System.IO
Imports System.Xml
Imports Microsoft.VisualBasic.FileIO
Imports System.Collections.Specialized
Imports System.Text
Imports System.IO.Path
Imports System.Environment
Imports Microsoft.VisualBasic.Devices
Imports System.Management
Imports Microsoft.Win32
Imports System.Net.Sockets
Imports System.Drawing.Icon
Imports System.Threading
Imports System.Security
Imports System.Runtime.InteropServices

Public Class Form1
    Dim steam As String
    Dim mpmision As String
    Dim esusservera3 As String
    Dim perfilpordefecto As String
    Dim esusbasico As String
    Dim esusconfig As String
    Dim misionespordefecto As String
    Dim crearperfil As System.IO.FileStream
    Dim crearperfil2 As System.IO.FileStream
    Dim creadordificultad As System.IO.FileStream
    Dim crearmod As System.IO.FileStream
    Dim crearprioridad As System.IO.FileStream
    Dim mision As System.IO.FileStream
    Dim puertost As System.IO.FileStream
    Dim crearconfig As System.IO.FileStream
    Dim exearma3server As String
    Dim carpetausuario As String
    Dim carpetauser As String
    Dim exearma3 As String
    Dim puerto As String
    Dim txtpuerto As String = "puerto.txt"
    Dim core1 As Integer
    Dim core2 As Integer
    Dim core3 As Integer
    Dim core4 As Integer
    Dim core5 As Integer
    Dim core6 As Integer
    Dim core7 As Integer
    Dim core8 As Integer
    Dim dificultad As String
    Dim config As String = "configuracion.cfg"
    Dim addon As String = "mods.txt"
    Dim prioridad As String = "mods.prioridad.txt"
    Dim mods As String
    Dim prioridadmods As String
    Dim ram As Integer
    Dim misionchecked As String
    Dim perfilcambiante As String
    Dim carpetausercambiante As String
    Dim rutacarpetaa3 As String
    Dim rutaaddons As String
    Dim carpetausuariocambiante As String
    Private programDataFilePath As String = Combine(GetFolderPath(SpecialFolder.ApplicationData), _
                                               My.Application.Info.AssemblyName)
    Private configuracion As String = Combine(programDataFilePath, config)
    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        If Not My.Computer.FileSystem.FileExists(configuracion) Then
            My.Computer.FileSystem.CreateDirectory(programDataFilePath)
            crearconfig = File.Create(configuracion)
            crearconfig.Close()
        Else
            Call leerconfig()
        End If
        If CheckBox10.Checked = True Then
            exearma3server = TextBox3.Text
            exearma3 = TextBox4.Text
            rutaaddons = TextBox44.Text
            TextBox44.Enabled = True
            TextBox4.Enabled = True
            TextBox3.Enabled = True
            Label83.Visible = False
            Label85.Visible = False
            Label86.Visible = False
            Label83.Enabled = False
            Label85.Enabled = False
            Label86.Enabled = False
            Button26.Enabled = True
            Button32.Enabled = True
            Button33.Enabled = True
        Else
            TextBox44.Enabled = False
            TextBox4.Enabled = False
            TextBox3.Enabled = False
            TextBox44.Text = Nothing
            TextBox4.Text = Nothing
            TextBox3.Text = Nothing
            Label83.Visible = True
            Label85.Visible = True
            Label86.Visible = True
            Label83.Enabled = True
            Label85.Enabled = True
            Label86.Enabled = True
            Button26.Enabled = False
            Button32.Enabled = False
            Button33.Enabled = False
            Try
                steam = My.Computer.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\Wow6432Node\bohemia interactive\arma 3", "main", Nothing)
                rutaaddons = steam
                exearma3server = rutaaddons & "\arma3server.exe"
                exearma3 = rutaaddons & "\arma3.exe"
            Catch
            End Try
            If IsNothing(rutaaddons) Then
                Try
                    rutaaddons = My.Computer.Registry.GetValue("HKEY_CURRENT_USER\Software\Valve\Steam", "SteamPath", Nothing)
                    rutaaddons = rutaaddons.Replace("/", "\")
                    rutaaddons = rutaaddons & "\steamapps\common\Arma 3"
                    exearma3server = rutaaddons & "\arma3server.exe"
                    exearma3 = rutaaddons & "\arma3.exe"
                Catch ex As Exception
                End Try
            Else
            End If
        End If
        If Not My.Computer.FileSystem.FileExists(exearma3server) Then
            MessageBox.Show("No se ha podido encontrar la ruta, del Exe de Arma 3 Server" + vbNewLine + exearma3server + vbNewLine + "Debes elegir la correcta ahora",
                                 "ATENCIÓN!!",
                                 MessageBoxButtons.OK,
                                 MessageBoxIcon.Warning,
                                 MessageBoxDefaultButton.Button1)


            Dim fd As OpenFileDialog = New OpenFileDialog()
            Dim strFileName As String
            fd.Title = "Selecciona la ruta de Arma 3 Server.exe"
            fd.Filter = "All files (*.*)|*.*|All files (*.*)|*.*"
            fd.FilterIndex = 2
            fd.RestoreDirectory = True
            If fd.ShowDialog() = DialogResult.OK Then
                strFileName = fd.FileName
                TextBox3.Text = strFileName
                exearma3server = TextBox3.Text
                Call guardarconfig()
            End If
        End If
        If Not My.Computer.FileSystem.FileExists(exearma3) Then
            MessageBox.Show("No se ha podido encontrar la ruta, del Exe de Arma 3" + vbNewLine + exearma3 + vbNewLine + "Debes elegir la correcta ahora",
                                 "ATENCIÓN!!",
                                 MessageBoxButtons.OK,
                                 MessageBoxIcon.Warning,
                                 MessageBoxDefaultButton.Button1)

            Dim fd As OpenFileDialog = New OpenFileDialog()
            Dim strFileName As String
            fd.Title = "Selecciona la ruta de Arma 3 .exe"
            fd.Filter = "All files (*.*)|*.*|All files (*.*)|*.*"
            fd.FilterIndex = 2
            fd.RestoreDirectory = True
            If fd.ShowDialog() = DialogResult.OK Then
                strFileName = fd.FileName
                TextBox4.Text = strFileName
                exearma3 = TextBox4.Text
                Call guardarconfig()
            End If
        End If
        If Not My.Computer.FileSystem.DirectoryExists(rutaaddons) Then
            MessageBox.Show("No se ha podido encontrar la ruta, de Addons de Arma 3" + vbNewLine + rutaaddons + vbNewLine + "Debes elegir la correcta ahora",
                                 "ATENCIÓN!!",
                                 MessageBoxButtons.OK,
                                 MessageBoxIcon.Warning,
                                 MessageBoxDefaultButton.Button1)
            FolderBrowserDialog1.Description = "Selecciona un directorio de addons de Arma 3"
            If FolderBrowserDialog1.ShowDialog() = DialogResult.OK Then
                TextBox44.Text = FolderBrowserDialog1.SelectedPath
                rutaaddons = TextBox44.Text
                Call guardarconfig()
            End If
        End If

        Try
            rutacarpetaa3 = Path.GetDirectoryName(exearma3)
        Catch
        End Try
        Label83.Text = exearma3server
        Label85.Text = exearma3
        Label86.Text = rutaaddons
        steam = steam & "\Steam.exe"
        mpmision = rutacarpetaa3 & "\MPMissions"
        esusservera3 = rutacarpetaa3 & "\EsusServerA3"
        perfilpordefecto = esusservera3 & "\PorDefecto"
        carpetauser = perfilpordefecto & "\Users"
        puerto = Combine(perfilpordefecto, txtpuerto)
        Try
            Dim ceropuerto As String = System.IO.File.ReadAllLines(puerto)(0)
            TextBox45.Text = ceropuerto
        Catch ex As Exception
            TextBox45.Text = Nothing
        End Try
        Try
            Dim unopuerto As String = System.IO.File.ReadAllLines(puerto)(1)
            If unopuerto.Contains("0") Then
                CheckBox21.Checked = False
            Else
                CheckBox21.Checked = True
            End If
        Catch
            CheckBox21.Checked = False
        End Try
        If ComboBox1.SelectedItem <> "" Then
            perfilcambiante = esusservera3 & "\" & ComboBox1.SelectedItem.ToString
        End If

        carpetausuario = carpetauser & "\" & "PorDefecto"
        dificultad = Combine(carpetausuario, "PorDefecto" & ".Arma3Profile")
        esusbasico = Combine(perfilpordefecto, "Esus_basico.cfg")
        esusconfig = Combine(perfilpordefecto, "Esus_config.cfg")
        mods = Combine(perfilpordefecto, addon)
        prioridadmods = Combine(perfilpordefecto, prioridad)

        If (Not System.IO.Directory.Exists(esusservera3)) Then
            System.IO.Directory.CreateDirectory(esusservera3)
            System.IO.Directory.CreateDirectory(perfilpordefecto)
            System.IO.Directory.CreateDirectory(carpetauser)
            System.IO.Directory.CreateDirectory(carpetausuario)

        End If
        If (Not System.IO.File.Exists(mods)) Then
            crearmod = File.Create(mods)
            crearmod.Close()
        End If
        If (Not System.IO.File.Exists(esusconfig)) Then
            crearperfil2 = File.Create(esusconfig)
            crearperfil2.Close()
            crearperfil2.Dispose()
        End If
        If (Not System.IO.File.Exists(prioridadmods)) Then
            crearprioridad = File.Create(prioridadmods)
            crearprioridad.Close()
        End If
        If (Not System.IO.File.Exists(dificultad)) Then
            Try
                creadordificultad = File.Create(dificultad)
                creadordificultad.Close()
            Catch ex As Exception

            End Try
            Call creardificultad()
        End If
        If (Not System.IO.File.Exists(esusbasico)) Then
            crearperfil = File.Create(esusbasico)
            crearperfil.Close()
            crearperfil.Dispose()
            puertost = System.IO.File.Create(puerto)
            puertost.Close()
            puertost.Dispose()
        End If



        For Each folder In System.IO.Directory.GetDirectories(rutaaddons, "@*")
            CheckedListBox1.Items.Add(Path.GetFileName(folder))
        Next
        For Each File In System.IO.Directory.GetFiles(mpmision, "*.pbo")
            Dim primeraparte As String = Path.GetFileName(File).Substring(0, Path.GetFileName(File).LastIndexOf("."))
            CheckedListBox2.Items.Add(Path.GetFileName(primeraparte))
        Next
        For Each folder In System.IO.Directory.GetDirectories(mpmision)
            CheckedListBox2.Items.Add(Path.GetFileName(folder))
        Next
        For Each perfiles As String In My.Computer.FileSystem.GetDirectories _
       (esusservera3)
            ComboBox1.Items.Add(System.IO.Path.GetFileNameWithoutExtension(perfiles))
        Next
        For Each File In System.IO.Directory.GetFiles(mpmision, "*.pbo")
            Dim entre As Integer = Path.GetFileName(File).IndexOf(".")
            Dim mapas As String = Path.GetFileName(File).Substring(entre + 1)
            Dim upper1 As String = mapas.ToUpper()
            ListBox4.Items.Add(Path.GetFileNameWithoutExtension(upper1))
            ListBox4.Items.Remove("PBO")
        Next
        For Each folder In System.IO.Directory.GetDirectories(mpmision)
            Dim entre As Integer = Path.GetFileName(folder).IndexOf(".")
            Dim mapas As String = Path.GetFileName(folder).Substring(entre + 1)
            Dim upper1 As String = mapas.ToUpper()
            ListBox4.Items.Add(Path.GetFileNameWithoutExtension(upper1))
        Next
        Call quitarmapasduplicados()
        If ListBox4.Items.Count > 0 Then
            For i As Integer = 0 To ListBox4.Items.Count - 1
                ComboBox2.Items.Add(ListBox4.Items(i))
            Next
        Else
        End If

        CheckedListBox3.Visible = False
        CheckedListBox2.Visible = True
        TextBox68.Text = CheckedListBox1.Items.Count
        TextBox65.Text = CheckedListBox2.Items.Count
        TextBox67.Text = CheckedListBox1.CheckedItems.Count
        TextBox66.Text = CheckedListBox2.CheckedItems.Count
        Try
            ComboBox4.SelectedIndex = 0
            ComboBox5.SelectedIndex = 0
            ComboBox6.SelectedIndex = 0
            ComboBox3.SelectedIndex = 0
            ComboBox7.SelectedIndex = 0
            ComboBox1.SelectedIndex = 0
        Catch ex As Exception

        End Try

        If CheckBox2.Checked = True Then
            Label22.Visible = False
            NumericUpDown7.Visible = False
        Else
            Label22.Visible = True
            NumericUpDown7.Visible = True
        End If
        Dim dificultadhilo As Thread
        Dim leerperfilconfighilo As Thread
        Dim leerperfilbasicohilo As Thread
        dificultadhilo = New Thread(AddressOf leerdificultad)
        leerperfilbasicohilo = New Thread(AddressOf leerperfilbasico)
        leerperfilconfighilo = New Thread(AddressOf leerperfilconfig)
        dificultadhilo.Start()
        leerperfilbasicohilo.Start()
        leerperfilconfighilo.Start()
        TextBox43.Text = System.Text.RegularExpressions.Regex.Replace(TextBox43.Text, "\s{2,}", " ")
        TextBox39.Text = TextBox39.Text.Replace(" ", "")
        TextBox38.Text = TextBox38.Text.Replace(" ", "")
        TextBox37.Text = TextBox37.Text.Replace(" ", "")
        TextBox36.Text = TextBox36.Text.Replace(" ", "")
        TextBox35.Text = TextBox35.Text.Replace(" ", "")
        TextBox34.Text = TextBox34.Text.Replace(" ", "")
        TextBox33.Text = TextBox33.Text.Replace(" ", "")
        TextBox21.Text = TextBox21.Text.Replace(" ", "")
        TextBox7.Text = TextBox7.Text.Replace(" ", "")
        TextBox8.Text = TextBox8.Text.Replace(" ", "")
        TextBox9.Text = TextBox9.Text.Replace(" ", "")
        TextBox10.Text = TextBox10.Text.Replace(" ", "")
        TextBox11.Text = TextBox11.Text.Replace(" ", "")
        TextBox12.Text = TextBox12.Text.Replace(" ", "")
        TextBox13.Text = TextBox13.Text.Replace(" ", "")
        TextBox14.Text = TextBox14.Text.Replace(" ", "")
        TextBox15.Text = TextBox15.Text.Replace(" ", "")
        Call hc()
        ListBox1.Items.Clear()
        If CheckedListBox1.CheckedItems.Count > 0 Then
            For i As Integer = 0 To CheckedListBox1.CheckedItems.Count - 1
                ListBox1.Items.Add(CheckedListBox1.CheckedItems(i))
            Next
        Else
            ListBox1.Items.Clear()
        End If
        ListBox2.Items.Clear()
        If ListBox1.Items.Count > 0 Then
            For i As Integer = 0 To ListBox1.Items.Count - 1
                ListBox2.Items.Add(ListBox1.Items(i))
            Next
        Else
            ListBox2.Items.Clear()
        End If
        ListBox3.Items.Clear()
        Call leermods()
        TextBox67.Text = CheckedListBox1.CheckedItems.Count
        CheckedListBox7.Items.Remove("armor")
        CheckedListBox7.Items.Remove("autoAim")
        CheckedListBox7.Items.Remove("autoGuideAT")
        CheckedListBox7.Items.Remove("autoSpot")
        CheckedListBox7.Items.Remove("enemyTag")
        CheckedListBox7.Items.Remove("friendlyTag")
        CheckedListBox7.Items.Remove("hudPerm")
        CheckedListBox7.Items.Remove("hudWpPerm")
        CheckedListBox7.Items.Remove("tracers")
        CheckedListBox7.Items.Remove("3RdPersonView")
        CheckedListBox7.Items.Remove("cameraShake")
        CheckedListBox7.Items.Remove("map")
        CheckedListBox7.Items.Remove("unlimitedSaves")
        CheckedListBox7.Items.Remove("weaponCursor")
        CheckedListBox7.Items.Remove("clockIndicator")
        CheckedListBox7.Items.Remove("hudGroupInfo")
        CheckedListBox7.Items.Remove("hudWp")
        CheckedListBox7.Items.Remove("hud")
        CheckedListBox7.Items.Remove("tracers")
        CheckedListBox6.Items.Remove("armor")
        CheckedListBox6.Items.Remove("autoAim")
        CheckedListBox6.Items.Remove("autoGuideAT")
        CheckedListBox6.Items.Remove("autoSpot")
        CheckedListBox6.Items.Remove("enemyTag")
        CheckedListBox6.Items.Remove("friendlyTag")
        CheckedListBox6.Items.Remove("hudPerm")
        CheckedListBox6.Items.Remove("hudWpPerm")
        CheckedListBox6.Items.Remove("tracers")
    End Sub
    Sub leerconfig()
        Try
            Dim cero As String = System.IO.File.ReadAllLines(configuracion)(0)
            Dim uno As String = System.IO.File.ReadAllLines(configuracion)(1)
            Dim dos As String = System.IO.File.ReadAllLines(configuracion)(2)
            Dim tres As String = System.IO.File.ReadAllLines(configuracion)(3)
            Dim cuatro As String = System.IO.File.ReadAllLines(configuracion)(4)
            Dim cinco As String = System.IO.File.ReadAllLines(configuracion)(5)
            Dim seis As String = System.IO.File.ReadAllLines(configuracion)(6)
            Dim siete As String = System.IO.File.ReadAllLines(configuracion)(7)
            Dim ocho As String = System.IO.File.ReadAllLines(configuracion)(8)
            Dim nueve As String = System.IO.File.ReadAllLines(configuracion)(9)
            Dim diez As String = System.IO.File.ReadAllLines(configuracion)(10)
            Dim once As String = System.IO.File.ReadAllLines(configuracion)(11)
            Dim doce As String = System.IO.File.ReadAllLines(configuracion)(12)
            Dim trece As String = System.IO.File.ReadAllLines(configuracion)(13)
            Dim catorce As String = System.IO.File.ReadAllLines(configuracion)(14)
            Dim quince As String = System.IO.File.ReadAllLines(configuracion)(15)
            Dim dieciseis As String = System.IO.File.ReadAllLines(configuracion)(16)
            Dim diecisiete As String = System.IO.File.ReadAllLines(configuracion)(17)
            Dim dieciocho As String = System.IO.File.ReadAllLines(configuracion)(18)

            If cero.Contains("0") Then
                CheckBox1.Checked = False
            Else
                CheckBox1.Checked = True
            End If
            If uno.Contains("nada") Then
                TextBox5.Text = ""
            Else
                TextBox5.Text = uno
            End If
            If dos.Contains("nada") Then
                TextBox16.Text = ""
            Else
                TextBox16.Text = dos
            End If
            If tres.Contains("nada") Then
                TextBox17.Text = ""
            Else
                TextBox17.Text = tres
            End If
            If cuatro.Contains("nada") Then
                TextBox2.Text = ""
            Else
                TextBox2.Text = cuatro
            End If
            If cinco.Contains("0") Then
                CheckBox9.Checked = False
            Else
                CheckBox9.Checked = True
            End If
            If seis.Contains("0") Then
                CheckBox7.Checked = False
            Else
                CheckBox7.Checked = True
            End If

            If ocho.Contains("0") Then
                CheckBox10.Checked = False
            Else
                CheckBox10.Checked = True
            End If
            If nueve.Contains("rutaporregistroserver") Then

            Else
                TextBox3.Text = nueve
            End If
            If diez.Contains("rutaporregistroarma3") Then

            Else
                TextBox4.Text = diez
            End If
            If once.Contains("rutaporregistroaddons") Then

            Else
                TextBox44.Text = once
            End If
            If doce.Contains("0") Then
                CheckBox11.Checked = False
            Else
                CheckBox11.Checked = True
            End If
            If trece.Contains("nada") Then
            Else
                TextBox30.Text = trece
            End If
            If CheckBox10.Checked = True Then
                TextBox44.Enabled = True
                TextBox4.Enabled = True
                TextBox3.Enabled = True
                Label83.Visible = False
                Label85.Visible = False
                Label86.Visible = False
                Label83.Enabled = False
                Label85.Enabled = False
                Label86.Enabled = False
                Button26.Enabled = True
                Button32.Enabled = True
                Button33.Enabled = True
            Else
                TextBox44.Enabled = False
                TextBox4.Enabled = False
                TextBox3.Enabled = False
                TextBox44.Text = Nothing
                TextBox4.Text = Nothing
                TextBox3.Text = Nothing
                Label83.Visible = True
                Label85.Visible = True
                Label86.Visible = True
                Label83.Enabled = True
                Label85.Enabled = True
                Label86.Enabled = True
                Button26.Enabled = False
                Button32.Enabled = False
                Button33.Enabled = False
            End If
            If dieciocho.Contains("0") Then
                CheckBox3.Checked = False
            Else
                CheckBox3.Checked = True
            End If
            If CheckBox3.Checked = True Then
                TextBox60.Enabled = False
                TextBox62.Enabled = False
                TextBox62.Text = "127.0.0.1"
                TextBox63.Enabled = False
                TextBox18.Enabled = False
                TextBox61.Enabled = False
                Call hc()
            Else
                TextBox60.Enabled = True
                TextBox62.Enabled = True
                TextBox62.Text = Nothing
                TextBox63.Enabled = True
                TextBox18.Enabled = True
                TextBox61.Enabled = True
            End If
            If catorce.Contains("nada") Then
            Else
                TextBox60.Text = catorce
            End If
            If quince.Contains("nada") Then
            Else
                TextBox61.Text = quince
            End If
            If dieciseis.Contains("nada") Then
            Else
                TextBox63.Text = dieciseis
            End If
            If diecisiete.Contains("nada") Then
            Else
                TextBox18.Text = diecisiete
            End If

        Catch ex As Exception

        End Try


    End Sub
    Private Sub quitarmapasduplicados()
        '-- Declare local variable '
        Dim List As New ArrayList

        '-- Loop through each Listbox item '
        For Each item1 As String In ListBox4.Items
            '-- Load each non-duplicate item into List (Arraylist) '
            If Not List.Contains(item1) Then
                List.Add(item1)
            End If
        Next

        '-- Clear and reload the Listbox with non-duplicate items from List (Arraylist)'
        ListBox4.Items.Clear()
        For Each item2 As String In List
            ListBox4.Items.Add(item2)
        Next
        If ListBox4.Items.Contains("2") Then
            ListBox4.Items.Remove("2")
        End If
    End Sub

    Sub hc()
        If ComboBox1.SelectedItem = Nothing Then
            perfilcambiante = esusservera3 & "\" & "PorDefecto"
            carpetausercambiante = perfilcambiante & "\" & "Users"
            carpetausuariocambiante = carpetausercambiante & "\" & "PorDefecto"
            puerto = Combine(perfilcambiante, txtpuerto)
            esusbasico = Combine(perfilcambiante, "Esus_basico.cfg")
            esusconfig = Combine(perfilcambiante, "Esus_config.cfg")
            dificultad = Combine(carpetausuariocambiante, "PorDefecto" & ".Arma3Profile")
        Else
            perfilcambiante = esusservera3 & "\" & ComboBox1.SelectedItem.ToString
            carpetausercambiante = perfilcambiante & "\" & "Users"
            carpetausuariocambiante = carpetausercambiante & "\" & ComboBox1.SelectedItem.ToString
            puerto = Combine(perfilcambiante, txtpuerto)
            esusbasico = Combine(perfilcambiante, "Esus_basico.cfg")
            esusconfig = Combine(perfilcambiante, "Esus_config.cfg")
            dificultad = Combine(carpetausuariocambiante, ComboBox1.SelectedItem.ToString & ".Arma3Profile")
        End If
        Dim builder As New StringBuilder()
        For Each i As Object In ListBox2.Items
            i = i + ";"
            builder.Append(i.ToString())
        Next
        Dim parametros As String = Nothing
        If CheckBox3.Checked = True Then
            TextBox19.Text = "-connect= localhost" & " " & "-port=" & TextBox45.Text & " " & "-client -nosound" & " " & "-password=" & TextBox46.Text & " " & "-name=HC -profile=HC" & " " & "-Mod=" & builder.ToString
        Else
            TextBox19.Text = "-connect=" & TextBox63.Text & " " & "-port=" & TextBox18.Text & " " & "-client -nosound" & " " & "-password=" & TextBox61.Text & " " & "-name=" & TextBox60.Text & " " & "-profile=" & TextBox60.Text & " " & "-Mod=" & builder.ToString
        End If
        If CheckBox11.Checked = True Then
            TextBox19.Text = "-nologs" & " " & TextBox19.Text
        End If
        

    End Sub
    Sub leerdificultad()
        Try
            Dim seis As String = System.IO.File.ReadAllLines(dificultad)(6)
            Dim siete As String = System.IO.File.ReadAllLines(dificultad)(7)
            Dim ocho As String = System.IO.File.ReadAllLines(dificultad)(8)
            Dim nueve As String = System.IO.File.ReadAllLines(dificultad)(9)
            Dim diez As String = System.IO.File.ReadAllLines(dificultad)(10)
            Dim once As String = System.IO.File.ReadAllLines(dificultad)(11)
            Dim doce As String = System.IO.File.ReadAllLines(dificultad)(12)
            Dim trece As String = System.IO.File.ReadAllLines(dificultad)(13)
            Dim catorce As String = System.IO.File.ReadAllLines(dificultad)(14)
            Dim quince As String = System.IO.File.ReadAllLines(dificultad)(15)
            Dim dieciseis As String = System.IO.File.ReadAllLines(dificultad)(16)
            Dim diecisiete As String = System.IO.File.ReadAllLines(dificultad)(17)
            Dim dieciocho As String = System.IO.File.ReadAllLines(dificultad)(18)
            Dim diecinueve As String = System.IO.File.ReadAllLines(dificultad)(19)
            Dim veinte As String = System.IO.File.ReadAllLines(dificultad)(20)
            Dim veintiuno As String = System.IO.File.ReadAllLines(dificultad)(21)
            Dim veintidos As String = System.IO.File.ReadAllLines(dificultad)(22)
            Dim veintitres As String = System.IO.File.ReadAllLines(dificultad)(23)
            Dim veinticuatro As String = System.IO.File.ReadAllLines(dificultad)(24)
            Dim veinticinco As String = System.IO.File.ReadAllLines(dificultad)(25)
            Dim veintiseis As String = System.IO.File.ReadAllLines(dificultad)(26)
            Dim veintisiete As String = System.IO.File.ReadAllLines(dificultad)(27)
            Dim veintiocho As String = System.IO.File.ReadAllLines(dificultad)(28)
            Dim veintinueve As String = System.IO.File.ReadAllLines(dificultad)(29)
            If seis.Contains("3RdPersonView=1;") Then
                CheckedListBox4.SetItemCheckState(0, CheckState.Checked)
            Else
                CheckedListBox4.SetItemCheckState(0, CheckState.Unchecked)
            End If
            If siete.Contains("armor=1;") Then
                CheckedListBox4.SetItemCheckState(1, CheckState.Checked)
            Else
                CheckedListBox4.SetItemCheckState(1, CheckState.Unchecked)
            End If
            If ocho.Contains("autoAim=1;") Then
                CheckedListBox4.SetItemCheckState(2, CheckState.Checked)
            Else
                CheckedListBox4.SetItemCheckState(2, CheckState.Unchecked)
            End If
            If nueve.Contains("autoGuideAT=1;") Then
                CheckedListBox4.SetItemCheckState(3, CheckState.Checked)
            Else
                CheckedListBox4.SetItemCheckState(3, CheckState.Unchecked)
            End If
            If diez.Contains("autoSpot=1;") Then
                CheckedListBox4.SetItemCheckState(4, CheckState.Checked)
            Else
                CheckedListBox4.SetItemCheckState(4, CheckState.Unchecked)
            End If
            If once.Contains("cameraShake=1;") Then
                CheckedListBox4.SetItemCheckState(5, CheckState.Checked)
            Else
                CheckedListBox4.SetItemCheckState(5, CheckState.Unchecked)
            End If
            If doce.Contains("clockIndicator=1;") Then
                CheckedListBox4.SetItemCheckState(6, CheckState.Checked)
            Else
                CheckedListBox4.SetItemCheckState(6, CheckState.Unchecked)
            End If

            If trece.Contains("deathMessages=1;") Then
                CheckedListBox4.SetItemCheckState(7, CheckState.Checked)
            Else
                CheckedListBox4.SetItemCheckState(7, CheckState.Unchecked)
            End If
            If catorce.Contains("enemyTag=1;") Then
                CheckedListBox4.SetItemCheckState(8, CheckState.Checked)
            Else
                CheckedListBox4.SetItemCheckState(8, CheckState.Unchecked)
            End If
            If quince.Contains("friendlyTag=1;") Then
                CheckedListBox4.SetItemCheckState(9, CheckState.Checked)
            Else
                CheckedListBox4.SetItemCheckState(9, CheckState.Unchecked)
            End If
            If dieciseis.Contains("hud=1;") Then
                CheckedListBox4.SetItemCheckState(10, CheckState.Checked)
            Else
                CheckedListBox4.SetItemCheckState(10, CheckState.Unchecked)
            End If
            If diecisiete.Contains("hudGroupInfo=1;") Then
                CheckedListBox4.SetItemCheckState(11, CheckState.Checked)
            Else
                CheckedListBox4.SetItemCheckState(11, CheckState.Unchecked)
            End If
            If dieciocho.Contains("hudPerm=1;") Then
                CheckedListBox4.SetItemCheckState(12, CheckState.Checked)
            Else
                CheckedListBox4.SetItemCheckState(12, CheckState.Unchecked)
            End If
            If diecinueve.Contains("hudWp=1;") Then
                CheckedListBox4.SetItemCheckState(13, CheckState.Checked)
            Else
                CheckedListBox4.SetItemCheckState(13, CheckState.Unchecked)
            End If
            If veinte.Contains("hudWpPerm=1;") Then
                CheckedListBox4.SetItemCheckState(14, CheckState.Checked)
            Else
                CheckedListBox4.SetItemCheckState(14, CheckState.Unchecked)
            End If
            If veintiuno.Contains("map=1;") Then
                CheckedListBox4.SetItemCheckState(15, CheckState.Checked)
            Else
                CheckedListBox4.SetItemCheckState(15, CheckState.Unchecked)
            End If
            If veintidos.Contains("netStats=1;") Then
                CheckedListBox4.SetItemCheckState(16, CheckState.Checked)
            Else
                CheckedListBox4.SetItemCheckState(16, CheckState.Unchecked)
            End If
            If veintitres.Contains("tracers=1;") Then
                CheckedListBox4.SetItemCheckState(17, CheckState.Checked)
            Else
                CheckedListBox4.SetItemCheckState(17, CheckState.Unchecked)
            End If
            If veinticuatro.Contains("ultraAI=1;") Then
                CheckedListBox4.SetItemCheckState(18, CheckState.Checked)
            Else
                CheckedListBox4.SetItemCheckState(18, CheckState.Unchecked)
            End If
            If veinticinco.Contains("unlimitedSaves=1;") Then
                CheckedListBox4.SetItemCheckState(19, CheckState.Checked)
            Else
                CheckedListBox4.SetItemCheckState(19, CheckState.Unchecked)
            End If
            If veintiseis.Contains("vonID=1;") Then
                CheckedListBox4.SetItemCheckState(20, CheckState.Checked)
            Else
                CheckedListBox4.SetItemCheckState(20, CheckState.Unchecked)
            End If
            If veintisiete.Contains("weaponCursor=1;") Then
                CheckedListBox4.SetItemCheckState(21, CheckState.Checked)
            Else
                CheckedListBox4.SetItemCheckState(21, CheckState.Unchecked)
            End If
            If veintiocho.Contains("ExtendetInfoType=1;") Then
                CheckedListBox4.SetItemCheckState(22, CheckState.Checked)
            Else
                CheckedListBox4.SetItemCheckState(22, CheckState.Unchecked)
            End If
            If veintinueve.Contains("MineTag=1;") Then
                CheckedListBox4.SetItemCheckState(23, CheckState.Checked)
            Else
                CheckedListBox4.SetItemCheckState(23, CheckState.Unchecked)
            End If

            Dim cuarenta As String = System.IO.File.ReadAllLines(dificultad)(40)
            Dim cuarentayuno As String = System.IO.File.ReadAllLines(dificultad)(41)
            Dim cuarentaydos As String = System.IO.File.ReadAllLines(dificultad)(42)
            Dim cuarentaytres As String = System.IO.File.ReadAllLines(dificultad)(43)
            Dim cuarentaycuatro As String = System.IO.File.ReadAllLines(dificultad)(44)
            Dim cuarentaycino As String = System.IO.File.ReadAllLines(dificultad)(45)
            Dim cuarentayseis As String = System.IO.File.ReadAllLines(dificultad)(46)
            Dim cuarentaysiete As String = System.IO.File.ReadAllLines(dificultad)(47)
            Dim cuarentayocho As String = System.IO.File.ReadAllLines(dificultad)(48)
            Dim cuarentaynueve As String = System.IO.File.ReadAllLines(dificultad)(49)
            Dim cincuenta As String = System.IO.File.ReadAllLines(dificultad)(50)
            Dim cincuentayuno As String = System.IO.File.ReadAllLines(dificultad)(51)
            Dim cincuentaydos As String = System.IO.File.ReadAllLines(dificultad)(52)
            Dim cincuentaytres As String = System.IO.File.ReadAllLines(dificultad)(53)
            Dim cincuentaycuatro As String = System.IO.File.ReadAllLines(dificultad)(54)
            Dim cincuentaycinco As String = System.IO.File.ReadAllLines(dificultad)(55)
            Dim cincuentayseis As String = System.IO.File.ReadAllLines(dificultad)(56)
            Dim cincuentaysiete As String = System.IO.File.ReadAllLines(dificultad)(57)
            Dim cincuentayocho As String = System.IO.File.ReadAllLines(dificultad)(58)
            Dim cincuentaynueve As String = System.IO.File.ReadAllLines(dificultad)(59)
            Dim sesenta As String = System.IO.File.ReadAllLines(dificultad)(60)
            Dim sesentayuno As String = System.IO.File.ReadAllLines(dificultad)(61)
            Dim sesentaydos As String = System.IO.File.ReadAllLines(dificultad)(62)
            Dim sesentaytres As String = System.IO.File.ReadAllLines(dificultad)(63)
            If cuarenta.Contains("3RdPersonView=1;") Then
                CheckedListBox5.SetItemCheckState(0, CheckState.Checked)
            Else
                CheckedListBox5.SetItemCheckState(0, CheckState.Unchecked)
            End If
            If cuarentayuno.Contains("armor=1;") Then
                CheckedListBox5.SetItemCheckState(1, CheckState.Checked)
            Else
                CheckedListBox5.SetItemCheckState(1, CheckState.Unchecked)
            End If
            If cuarentaydos.Contains("autoAim=1;") Then
                CheckedListBox5.SetItemCheckState(2, CheckState.Checked)
            Else
                CheckedListBox5.SetItemCheckState(2, CheckState.Unchecked)
            End If
            If cuarentaytres.Contains("autoGuideAT=1;") Then
                CheckedListBox5.SetItemCheckState(3, CheckState.Checked)
            Else
                CheckedListBox5.SetItemCheckState(3, CheckState.Unchecked)
            End If
            If cuarentaycuatro.Contains("autoSpot=1;") Then
                CheckedListBox5.SetItemCheckState(4, CheckState.Checked)
            Else
                CheckedListBox5.SetItemCheckState(4, CheckState.Unchecked)
            End If
            If cuarentaycino.Contains("cameraShake=1;") Then
                CheckedListBox5.SetItemCheckState(5, CheckState.Checked)
            Else
                CheckedListBox5.SetItemCheckState(5, CheckState.Unchecked)
            End If
            If cuarentayseis.Contains("clockIndicator=1;") Then
                CheckedListBox5.SetItemCheckState(6, CheckState.Checked)
            Else
                CheckedListBox5.SetItemCheckState(6, CheckState.Unchecked)
            End If

            If cuarentaysiete.Contains("deathMessages=1;") Then
                CheckedListBox5.SetItemCheckState(7, CheckState.Checked)
            Else
                CheckedListBox5.SetItemCheckState(7, CheckState.Unchecked)
            End If
            If cuarentayocho.Contains("enemyTag=1;") Then
                CheckedListBox5.SetItemCheckState(8, CheckState.Checked)
            Else
                CheckedListBox5.SetItemCheckState(8, CheckState.Unchecked)
            End If
            If cuarentaynueve.Contains("friendlyTag=1;") Then
                CheckedListBox5.SetItemCheckState(9, CheckState.Checked)
            Else
                CheckedListBox5.SetItemCheckState(9, CheckState.Unchecked)
            End If
            If cincuenta.Contains("hud=1;") Then
                CheckedListBox5.SetItemCheckState(10, CheckState.Checked)
            Else
                CheckedListBox5.SetItemCheckState(10, CheckState.Unchecked)
            End If
            If cincuentayuno.Contains("hudGroupInfo=1;") Then
                CheckedListBox5.SetItemCheckState(11, CheckState.Checked)
            Else
                CheckedListBox5.SetItemCheckState(11, CheckState.Unchecked)
            End If
            If cincuentaydos.Contains("hudPerm=1;") Then
                CheckedListBox5.SetItemCheckState(12, CheckState.Checked)
            Else
                CheckedListBox5.SetItemCheckState(12, CheckState.Unchecked)
            End If
            If cincuentaytres.Contains("hudWp=1;") Then
                CheckedListBox5.SetItemCheckState(13, CheckState.Checked)
            Else
                CheckedListBox5.SetItemCheckState(13, CheckState.Unchecked)
            End If
            If cincuentaycuatro.Contains("hudWpPerm=1;") Then
                CheckedListBox5.SetItemCheckState(14, CheckState.Checked)
            Else
                CheckedListBox5.SetItemCheckState(14, CheckState.Unchecked)
            End If
            If cincuentaycinco.Contains("map=1;") Then
                CheckedListBox5.SetItemCheckState(15, CheckState.Checked)
            Else
                CheckedListBox5.SetItemCheckState(15, CheckState.Unchecked)
            End If
            If cincuentayseis.Contains("netStats=1;") Then
                CheckedListBox5.SetItemCheckState(16, CheckState.Checked)
            Else
                CheckedListBox5.SetItemCheckState(16, CheckState.Unchecked)
            End If
            If cincuentaysiete.Contains("tracers=1;") Then
                CheckedListBox5.SetItemCheckState(17, CheckState.Checked)
            Else
                CheckedListBox5.SetItemCheckState(17, CheckState.Unchecked)
            End If
            If cincuentayocho.Contains("ultraAI=1;") Then
                CheckedListBox5.SetItemCheckState(18, CheckState.Checked)
            Else
                CheckedListBox5.SetItemCheckState(18, CheckState.Unchecked)
            End If
            If cincuentaynueve.Contains("unlimitedSaves=1;") Then
                CheckedListBox5.SetItemCheckState(19, CheckState.Checked)
            Else
                CheckedListBox5.SetItemCheckState(19, CheckState.Unchecked)
            End If
            If sesenta.Contains("vonID=1;") Then
                CheckedListBox5.SetItemCheckState(20, CheckState.Checked)
            Else
                CheckedListBox5.SetItemCheckState(20, CheckState.Unchecked)
            End If
            If sesentayuno.Contains("weaponCursor=1;") Then
                CheckedListBox5.SetItemCheckState(21, CheckState.Checked)
            Else
                CheckedListBox5.SetItemCheckState(21, CheckState.Unchecked)
            End If
            If sesentaydos.Contains("ExtendetInfoType=1;") Then
                CheckedListBox5.SetItemCheckState(22, CheckState.Checked)
            Else
                CheckedListBox5.SetItemCheckState(22, CheckState.Unchecked)
            End If
            If sesentaytres.Contains("MineTag=1;") Then
                CheckedListBox5.SetItemCheckState(23, CheckState.Checked)
            Else
                CheckedListBox5.SetItemCheckState(23, CheckState.Unchecked)
            End If



            Dim setentaycuatro As String = System.IO.File.ReadAllLines(dificultad)(74)
            Dim setentaycinco As String = System.IO.File.ReadAllLines(dificultad)(75)
            Dim setentayseis As String = System.IO.File.ReadAllLines(dificultad)(76)
            Dim setentaysiete As String = System.IO.File.ReadAllLines(dificultad)(77)
            Dim setentayocho As String = System.IO.File.ReadAllLines(dificultad)(78)
            Dim setentaynueve As String = System.IO.File.ReadAllLines(dificultad)(79)
            Dim ochenta As String = System.IO.File.ReadAllLines(dificultad)(80)
            Dim ochentayuno As String = System.IO.File.ReadAllLines(dificultad)(81)
            Dim ochentaydos As String = System.IO.File.ReadAllLines(dificultad)(82)
            Dim ochentaytres As String = System.IO.File.ReadAllLines(dificultad)(83)
            Dim ochentaycuatro As String = System.IO.File.ReadAllLines(dificultad)(84)
            Dim ochentaycinco As String = System.IO.File.ReadAllLines(dificultad)(85)
            Dim ochentayseis As String = System.IO.File.ReadAllLines(dificultad)(86)
            Dim ochentaysiete As String = System.IO.File.ReadAllLines(dificultad)(87)
            Dim ochentayocho As String = System.IO.File.ReadAllLines(dificultad)(88)
            Dim ochentaynueve As String = System.IO.File.ReadAllLines(dificultad)(89)
            Dim noventa As String = System.IO.File.ReadAllLines(dificultad)(90)
            Dim noventayuno As String = System.IO.File.ReadAllLines(dificultad)(91)
            Dim noventaydos As String = System.IO.File.ReadAllLines(dificultad)(92)
            Dim noventaytres As String = System.IO.File.ReadAllLines(dificultad)(93)
            Dim noventaycuatro As String = System.IO.File.ReadAllLines(dificultad)(94)
            Dim noventaycinco As String = System.IO.File.ReadAllLines(dificultad)(95)
            Dim noventayseis As String = System.IO.File.ReadAllLines(dificultad)(96)
            Dim noventaysiete As String = System.IO.File.ReadAllLines(dificultad)(97)
            If setentaycuatro.Contains("3RdPersonView=1;") Then
                CheckedListBox6.SetItemCheckState(0, CheckState.Checked)
            Else
                CheckedListBox6.SetItemCheckState(0, CheckState.Unchecked)
            End If
            If setentaynueve.Contains("cameraShake=1;") Then
                CheckedListBox6.SetItemCheckState(1, CheckState.Checked)
            Else
                CheckedListBox6.SetItemCheckState(1, CheckState.Unchecked)
            End If
            If ochenta.Contains("clockIndicator=1;") Then
                CheckedListBox6.SetItemCheckState(2, CheckState.Checked)
            Else
                CheckedListBox6.SetItemCheckState(2, CheckState.Unchecked)
            End If

            If ochentayuno.Contains("deathMessages=1;") Then
                CheckedListBox6.SetItemCheckState(3, CheckState.Checked)
            Else
                CheckedListBox6.SetItemCheckState(3, CheckState.Unchecked)
            End If
            If ochentaycuatro.Contains("hud=1;") Then
                CheckedListBox6.SetItemCheckState(4, CheckState.Checked)
            Else
                CheckedListBox6.SetItemCheckState(4, CheckState.Unchecked)
            End If
            If ochentaycinco.Contains("hudGroupInfo=1;") Then
                CheckedListBox6.SetItemCheckState(5, CheckState.Checked)
            Else
                CheckedListBox6.SetItemCheckState(5, CheckState.Unchecked)
            End If
            If ochentayseis.Contains("hudPerm=1;") Then
                CheckedListBox6.SetItemCheckState(6, CheckState.Checked)
            Else
                CheckedListBox6.SetItemCheckState(6, CheckState.Unchecked)
            End If
            If ochentaysiete.Contains("hudWp=1;") Then
                CheckedListBox6.SetItemCheckState(7, CheckState.Checked)
            Else
                CheckedListBox6.SetItemCheckState(7, CheckState.Unchecked)
            End If
            If ochentaynueve.Contains("map=1;") Then
                CheckedListBox6.SetItemCheckState(8, CheckState.Checked)
            Else
                CheckedListBox6.SetItemCheckState(8, CheckState.Unchecked)
            End If
            If noventa.Contains("netStats=1;") Then
                CheckedListBox6.SetItemCheckState(9, CheckState.Checked)
            Else
                CheckedListBox6.SetItemCheckState(9, CheckState.Unchecked)
            End If
            If noventaydos.Contains("ultraAI=1;") Then
                CheckedListBox6.SetItemCheckState(10, CheckState.Checked)
            Else
                CheckedListBox6.SetItemCheckState(10, CheckState.Unchecked)
            End If
            If noventaytres.Contains("unlimitedSaves=1;") Then
                CheckedListBox6.SetItemCheckState(11, CheckState.Checked)
            Else
                CheckedListBox6.SetItemCheckState(11, CheckState.Unchecked)
            End If
            If noventaycuatro.Contains("vonID=1;") Then
                CheckedListBox6.SetItemCheckState(12, CheckState.Checked)
            Else
                CheckedListBox6.SetItemCheckState(12, CheckState.Unchecked)
            End If
            If noventaycinco.Contains("weaponCursor=1;") Then
                CheckedListBox6.SetItemCheckState(13, CheckState.Checked)
            Else
                CheckedListBox6.SetItemCheckState(13, CheckState.Unchecked)
            End If
            If noventayseis.Contains("ExtendetInfoType=1;") Then
                CheckedListBox6.SetItemCheckState(14, CheckState.Checked)
            Else
                CheckedListBox6.SetItemCheckState(14, CheckState.Unchecked)
            End If
            If noventaysiete.Contains("MineTag=1;") Then
                CheckedListBox6.SetItemCheckState(15, CheckState.Checked)
            Else
                CheckedListBox6.SetItemCheckState(15, CheckState.Unchecked)
            End If





            Dim cientocho As String = System.IO.File.ReadAllLines(dificultad)(108)
            Dim cientonueve As String = System.IO.File.ReadAllLines(dificultad)(109)
            Dim cientodiez As String = System.IO.File.ReadAllLines(dificultad)(110)
            Dim cientonce As String = System.IO.File.ReadAllLines(dificultad)(111)
            Dim cientodoce As String = System.IO.File.ReadAllLines(dificultad)(112)
            Dim cientotrece As String = System.IO.File.ReadAllLines(dificultad)(113)
            Dim cientocatorce As String = System.IO.File.ReadAllLines(dificultad)(114)
            Dim cientoquince As String = System.IO.File.ReadAllLines(dificultad)(115)
            Dim cientodieciseis As String = System.IO.File.ReadAllLines(dificultad)(116)
            Dim cientodiecisiete As String = System.IO.File.ReadAllLines(dificultad)(117)
            Dim cientodieciocho As String = System.IO.File.ReadAllLines(dificultad)(118)
            Dim cientodiecinueve As String = System.IO.File.ReadAllLines(dificultad)(119)
            Dim cientoveinte As String = System.IO.File.ReadAllLines(dificultad)(120)
            Dim cientoveintiuno As String = System.IO.File.ReadAllLines(dificultad)(121)
            Dim cientoveintidos As String = System.IO.File.ReadAllLines(dificultad)(122)
            Dim cientoveintitres As String = System.IO.File.ReadAllLines(dificultad)(123)
            Dim cientoveinticuatro As String = System.IO.File.ReadAllLines(dificultad)(124)
            Dim cientoveinticinco As String = System.IO.File.ReadAllLines(dificultad)(125)
            Dim cientoveintiseis As String = System.IO.File.ReadAllLines(dificultad)(126)
            Dim cientoveintisiete As String = System.IO.File.ReadAllLines(dificultad)(127)
            Dim cientoveintiocho As String = System.IO.File.ReadAllLines(dificultad)(128)
            Dim cientoveintinueve As String = System.IO.File.ReadAllLines(dificultad)(129)
            Dim cientotreinta As String = System.IO.File.ReadAllLines(dificultad)(130)
            Dim cientotreintayuno As String = System.IO.File.ReadAllLines(dificultad)(131)
            If cientoquince.Contains("deathMessages=1;") Then
                CheckedListBox7.SetItemCheckState(0, CheckState.Checked)
            Else
                CheckedListBox7.SetItemCheckState(0, CheckState.Unchecked)
            End If
            If cientoveinticuatro.Contains("netStats=1;") Then
                CheckedListBox7.SetItemCheckState(1, CheckState.Checked)
            Else
                CheckedListBox7.SetItemCheckState(1, CheckState.Unchecked)
            End If
            If cientoveintiseis.Contains("ultraAI=1;") Then
                CheckedListBox7.SetItemCheckState(2, CheckState.Checked)
            Else
                CheckedListBox7.SetItemCheckState(2, CheckState.Unchecked)
            End If
            If cientoveintiocho.Contains("vonID=1;") Then
                CheckedListBox7.SetItemCheckState(3, CheckState.Checked)
            Else
                CheckedListBox7.SetItemCheckState(3, CheckState.Unchecked)
            End If
            If cientotreinta.Contains("ExtendetInfoType=1;") Then
                CheckedListBox7.SetItemCheckState(4, CheckState.Checked)
            Else
                CheckedListBox7.SetItemCheckState(4, CheckState.Unchecked)
            End If
            If cientotreintayuno.Contains("MineTag=1;") Then
                CheckedListBox7.SetItemCheckState(5, CheckState.Checked)
            Else
                CheckedListBox7.SetItemCheckState(5, CheckState.Unchecked)
            End If
            Dim treintayuno As String = System.IO.File.ReadAllLines(dificultad)(31)
            Dim treintaydos As String = System.IO.File.ReadAllLines(dificultad)(32)
            Dim treintaytres As String = System.IO.File.ReadAllLines(dificultad)(33)
            Dim treintaycuatro As String = System.IO.File.ReadAllLines(dificultad)(34)
            Dim cientrotreintaynueve As String = System.IO.File.ReadAllLines(dificultad)(139)
            treintayuno = Replace(treintayuno, "aiLevelPreset=", "")
            treintayuno = Replace(treintayuno, ";", "")
            treintaydos = Replace(treintaydos, "skillAI=", "")
            treintaydos = Replace(treintaydos, ";", "")
            treintaytres = Replace(treintaytres, "precisionAI=", "")
            treintaytres = Replace(treintaytres, ";", "")
            treintayuno = System.Text.RegularExpressions.Regex.Replace(treintayuno, "\s{2,}", " ")
            ComboBox8.SelectedIndex = treintayuno
            TextBox22.Text = treintaydos
            TextBox26.Text = treintaytres

            treintaydos = System.Text.RegularExpressions.Regex.Replace(treintaydos, "\s{2,}", " ")
            treintaytres = System.Text.RegularExpressions.Regex.Replace(treintaytres, "\s{2,}", " ")
            Dim sesentaycinco As String = System.IO.File.ReadAllLines(dificultad)(65)
            Dim sesentayseis As String = System.IO.File.ReadAllLines(dificultad)(66)
            Dim sesentaysiete As String = System.IO.File.ReadAllLines(dificultad)(67)
            Dim sesentayocho As String = System.IO.File.ReadAllLines(dificultad)(68)
            sesentaycinco = Replace(sesentaycinco, "aiLevelPreset=", "")
            sesentaycinco = Replace(sesentaycinco, ";", "")
            sesentayseis = Replace(sesentayseis, "skillAI=", "")
            sesentayseis = Replace(sesentayseis, ";", "")
            sesentaysiete = Replace(sesentaysiete, "precisionAI=", "")
            sesentaysiete = Replace(sesentaysiete, ";", "")
            sesentaycinco = System.Text.RegularExpressions.Regex.Replace(sesentaycinco, "\s{2,}", " ")
            sesentayseis = System.Text.RegularExpressions.Regex.Replace(sesentayseis, "\s{2,}", " ")
            sesentaysiete = System.Text.RegularExpressions.Regex.Replace(sesentaysiete, "\s{2,}", " ")
            ComboBox9.SelectedIndex = sesentaycinco

            TextBox23.Text = sesentayseis
            TextBox27.Text = sesentaysiete

            Dim noventaynueve As String = System.IO.File.ReadAllLines(dificultad)(99)
            Dim cien As String = System.IO.File.ReadAllLines(dificultad)(100)
            Dim cientouno As String = System.IO.File.ReadAllLines(dificultad)(101)
            Dim cientodos As String = System.IO.File.ReadAllLines(dificultad)(102)
            noventaynueve = Replace(noventaynueve, "aiLevelPreset=", "")
            noventaynueve = Replace(noventaynueve, ";", "")
            cien = Replace(cien, "skillAI=", "")
            cien = Replace(cien, ";", "")
            cientouno = Replace(cientouno, "precisionAI=", "")
            cientouno = Replace(cientouno, ";", "")
            noventaynueve = System.Text.RegularExpressions.Regex.Replace(noventaynueve, "\s{2,}", " ")
            cien = System.Text.RegularExpressions.Regex.Replace(cien, "\s{2,}", " ")
            cientouno = System.Text.RegularExpressions.Regex.Replace(cientouno, "\s{2,}", " ")
            cientodos = System.Text.RegularExpressions.Regex.Replace(cientodos, "\s{2,}", " ")
            ComboBox10.SelectedIndex = noventaynueve
            TextBox24.Text = cien
            TextBox28.Text = cientouno

           

            Dim cientotreintaytres As String = System.IO.File.ReadAllLines(dificultad)(133)
            Dim cientotreintaycuatro As String = System.IO.File.ReadAllLines(dificultad)(134)
            Dim cientotreintaycinco As String = System.IO.File.ReadAllLines(dificultad)(135)
            Dim cientotreintayseis As String = System.IO.File.ReadAllLines(dificultad)(136)
            cientotreintaytres = Replace(cientotreintaytres, "aiLevelPreset=", "")
            cientotreintaytres = Replace(cientotreintaytres, ";", "")
            cientotreintaycuatro = Replace(cientotreintaycuatro, "skillAI=", "")
            cientotreintaycuatro = Replace(cientotreintaycuatro, ";", "")
            cientotreintaycinco = Replace(cientotreintaycinco, "precisionAI=", "")
            cientotreintaycinco = Replace(cientotreintaycinco, ";", "")
            cientotreintaytres = System.Text.RegularExpressions.Regex.Replace(cientotreintaytres, "\s{2,}", " ")
            cientotreintaycuatro = System.Text.RegularExpressions.Regex.Replace(cientotreintaycuatro, "\s{2,}", " ")
            cientotreintaycinco = System.Text.RegularExpressions.Regex.Replace(cientotreintaycinco, "\s{2,}", " ")
            cientotreintayseis = System.Text.RegularExpressions.Regex.Replace(cientotreintayseis, "\s{2,}", " ")
            ComboBox11.SelectedIndex = cientotreintaytres
        Catch ex As Exception

        End Try
    End Sub
    Public Sub SetItemChecked( _
    index As Integer, _
    value As Boolean _
)

    End Sub
    Sub creardificultad()
        If ComboBox1.SelectedItem = "PorDefecto" Then
            perfilcambiante = esusservera3 & "\" & "PorDefecto"
            carpetausercambiante = perfilcambiante & "\" & "Users"
            carpetausuariocambiante = carpetausercambiante & "\" & "PorDefecto"
            puerto = Combine(perfilcambiante, txtpuerto)
            esusbasico = Combine(perfilcambiante, "Esus_basico.cfg")
            esusconfig = Combine(perfilcambiante, "Esus_config.cfg")
            dificultad = Combine(carpetausuariocambiante, "PorDefecto" & ".Arma3Profile")
        Else
            If ComboBox1.SelectedItem = Nothing Then

            Else
                perfilcambiante = esusservera3 & "\" & ComboBox1.SelectedItem.ToString
                carpetausercambiante = perfilcambiante & "\" & "Users"
                carpetausuariocambiante = carpetausercambiante & "\" & ComboBox1.SelectedItem.ToString
                puerto = Combine(perfilcambiante, txtpuerto)
                esusbasico = Combine(perfilcambiante, "Esus_basico.cfg")
                esusconfig = Combine(perfilcambiante, "Esus_config.cfg")
                dificultad = Combine(carpetausuariocambiante, ComboBox1.SelectedItem.ToString & ".Arma3Profile")
            End If

        End If
        Dim aryText(139) As String
        Dim x As Integer
        aryText(0) = "class Difficulties"
        aryText(1) = "{"
        aryText(2) = "class Recruit"
        aryText(3) = "{"
        aryText(4) = "class Flags"
        aryText(5) = "{"
        aryText(6) = "3RdPersonView=1;"
        aryText(7) = "armor=1;"
        aryText(8) = "autoAim=0;"
        aryText(9) = "autoGuideAT=1;"
        aryText(10) = "autoSpot=1;"
        aryText(11) = "cameraShake=0;"
        aryText(12) = "clockIndicator=1;"
        aryText(13) = "deathMessages=1;"
        aryText(14) = "enemyTag=0;"
        aryText(15) = "friendlyTag=1;"
        aryText(16) = "hud=1;"
        aryText(17) = "hudGroupInfo=1"
        aryText(18) = "hudPerm=1;"
        aryText(19) = "hudWp=1;"
        aryText(20) = "hudWpPerm=1;"
        aryText(21) = "map=1;"
        aryText(22) = "netStats=1;"
        aryText(23) = "tracers=1;"
        aryText(24) = "ultraAI=0;"
        aryText(25) = "unlimitedSaves=1;"
        aryText(26) = "vonID=1;"
        aryText(27) = "weaponCursor=1;"
        aryText(28) = "ExtendetInfoType=1;"
        aryText(29) = "MineTag=1;"
        aryText(30) = "};"
        aryText(31) = "aiLevelPreset=0;"
        aryText(32) = "skillAI=0.50;"
        aryText(33) = "precisionAI=0.20;"
        aryText(35) = "};"
        aryText(36) = "class Regular"
        aryText(37) = "{"
        aryText(38) = "class Flags"
        aryText(39) = "{"
        aryText(40) = "3RdPersonView=1;"
        aryText(41) = "armor=1;"
        aryText(42) = "autoAim=0;"
        aryText(43) = "autoGuideAT=1;"
        aryText(44) = "autoSpot=1;"
        aryText(45) = "cameraShake=1;"
        aryText(46) = "clockIndicator=1;"
        aryText(47) = "deathMessages=1;"
        aryText(48) = "enemyTag=0;"
        aryText(49) = "friendlyTag=1;"
        aryText(50) = "hud=1;"
        aryText(51) = "hudGroupInfo=1;"
        aryText(52) = "hudPerm=1;"
        aryText(53) = "hudWp=1;"
        aryText(54) = "hudWpPerm=1"
        aryText(55) = "map=1;"
        aryText(56) = "netStats=1;"
        aryText(57) = "tracers=1;"
        aryText(58) = "ultraAI=0;"
        aryText(59) = "unlimitedSaves=1;"
        aryText(60) = "vonID=1;"
        aryText(61) = "weaponCursor=1;"
        aryText(62) = "ExtendetInfoType=0;"
        aryText(63) = "MineTag=0;"
        aryText(64) = "};"
        aryText(65) = "aiLevelPreset=0;"
        aryText(66) = "skillAI=0.50;"
        aryText(67) = "precisionAI=0.20;"
        aryText(69) = "};"
        aryText(70) = "class Veteran"
        aryText(71) = "{"
        aryText(72) = "class Flags"
        aryText(73) = "{"
        aryText(74) = "3RdPersonView=1;"
        aryText(75) = "armor=0;"
        aryText(76) = "autoAim=0;"
        aryText(77) = "autoGuideAT=0;"
        aryText(78) = "autoSpot=0;"
        aryText(79) = "cameraShake=1;"
        aryText(80) = "clockIndicator=0;"
        aryText(81) = "deathMessages=1;"
        aryText(82) = "enemyTag=0;"
        aryText(83) = "friendlyTag=0;"
        aryText(84) = "hud=1;"
        aryText(85) = "hudGroupInfo=0;"
        aryText(86) = "hudPerm=0;"
        aryText(87) = "hudWp=1;"
        aryText(88) = "hudWpPerm=0;"
        aryText(89) = "map=0;"
        aryText(90) = "netStats=1;"
        aryText(91) = "tracers=0;"
        aryText(92) = "ultraAI=0;"
        aryText(93) = "unlimitedSaves=0;"
        aryText(94) = "vonID=0;"
        aryText(95) = "weaponCursor=1;"
        aryText(96) = "ExtendetInfoType=0;"
        aryText(97) = "MineTag=0;"
        aryText(98) = "};"
        aryText(99) = "aiLevelPreset=0;"
        aryText(100) = "skillAI=0.50;"
        aryText(101) = "precisionAI=0.20;"
        aryText(103) = "};"
        aryText(104) = "class Mercenary"
        aryText(105) = "{"
        aryText(106) = "class Flags"
        aryText(107) = "{"
        aryText(108) = "3RdPersonView=0;"
        aryText(109) = "armor=0;"
        aryText(110) = "autoAim=0;"
        aryText(111) = "autoGuideAT=0;"
        aryText(112) = "autoSpot=0;"
        aryText(113) = "cameraShake=1;"
        aryText(114) = "clockIndicator=0;"
        aryText(115) = "deathMessages=0;"
        aryText(116) = "enemyTag=0;"
        aryText(117) = "friendlyTag=0;"
        aryText(118) = "hud=0;"
        aryText(119) = "hudGroupInfo=0;"
        aryText(120) = "hudPerm=0;"
        aryText(121) = "hudWp=0;"
        aryText(122) = "hudWpPerm=0;"
        aryText(123) = "map=0;"
        aryText(124) = "netStats=0;"
        aryText(125) = "tracers=0;"
        aryText(126) = "ultraAI=0;"
        aryText(127) = "unlimitedSaves=0;"
        aryText(128) = "vonID=0;"
        aryText(129) = "weaponCursor=0;"
        aryText(130) = "ExtendetInfoType=0;"
        aryText(131) = "MineTag=0;"
        aryText(132) = "};"
        aryText(133) = "aiLevelPreset=0;"
        aryText(134) = "skillAI=0.50;"
        aryText(135) = "precisionAI=0.20;"
        aryText(137) = "};"
        aryText(138) = "};"
        aryText(139) = "difficulty=""recruit"";"

        Try
            Using sw As New StreamWriter(dificultad)
                For x = 0 To 139
                    sw.WriteLine(aryText(x))
                Next
            End Using
        Catch ex As Exception

        End Try
     


    End Sub
    Sub leermods()
        Try
            perfilcambiante = esusservera3 & "\" & ComboBox1.SelectedItem.ToString
            mods = Combine(perfilcambiante, addon)
        Catch ex As Exception

        End Try

        If My.Computer.FileSystem.FileExists(mods) Then
            Try
                Using rdr As New System.IO.StreamReader(mods)
                    Do While rdr.Peek() >= 0
                        Dim itm As String = rdr.ReadLine
                        itm = itm.Trim

                        If itm <> "" Then
                            For i As Integer = 0 To CheckedListBox1.Items.Count - 1
                                If CheckedListBox1.Items(i).ToString = itm Then
                                    CheckedListBox1.SetItemCheckState(i, CheckState.Checked)
                                    Exit For
                                End If
                            Next
                        End If
                    Loop
                End Using
            Catch ex As Exception
            End Try
        End If
        If IO.File.Exists(prioridadmods) Then
            Try
                Dim r As New IO.StreamReader(prioridadmods)
                Dim txt As New TextBox
                txt.Text = r.ReadToEnd
                ListBox1.Items.Clear()
                ListBox2.Items.Clear()
                For i As Integer = 0 To txt.Lines.Count - 2
                    If CheckedListBox1.Items.Contains(txt.Lines(i)) = True Then
                        ListBox1.Items.Add(txt.Lines(i))
                        ListBox2.Items.Add(txt.Lines(i))
                    End If
                Next
                r.Close()
            Catch ex As Exception

            End Try
            Call construirparametro()
        End If
        TextBox67.Text = CheckedListBox1.CheckedItems.Count
        TextBox68.Text = CheckedListBox1.Items.Count
    End Sub
    Sub construirparametro()

        If ComboBox1.SelectedItem <> "" Then
            perfilcambiante = esusservera3 & "\" & ComboBox1.SelectedItem.ToString
            carpetausercambiante = perfilcambiante & "\" & "Users"
            carpetausuariocambiante = carpetausercambiante & "\" & ComboBox1.SelectedItem.ToString
            puerto = Combine(perfilcambiante, txtpuerto)
            esusbasico = Combine(perfilcambiante, "Esus_basico.cfg")
            esusconfig = Combine(perfilcambiante, "Esus_config.cfg")
            dificultad = Combine(carpetausuariocambiante, ComboBox1.SelectedItem.ToString & ".Arma3Profile")
        Else
            perfilcambiante = esusservera3 & "\" & "PorDefecto"
            carpetausercambiante = perfilcambiante & "\" & "Users"
            carpetausuariocambiante = carpetausercambiante & "\" & "PorDefecto"
            puerto = Combine(perfilcambiante, txtpuerto)
            esusbasico = Combine(perfilcambiante, "Esus_basico.cfg")
            esusconfig = Combine(perfilcambiante, "Esus_config.cfg")
            dificultad = Combine(carpetausuariocambiante, "PorDefecto" & ".Arma3Profile")
        End If

        Dim builder As New StringBuilder()
        If rutaaddons = rutacarpetaa3 Then
            For Each i As Object In ListBox1.Items
                i = i + ";"
                builder.Append(i.ToString())
            Next
        Else
            For Each i As Object In ListBox1.Items
                i = rutaaddons + "\" + i + ";"
                builder.Append(i.ToString())
            Next
        End If
     
        Dim parametros As String = Nothing
        If CheckBox1.Checked = True Then
            parametros = "-loadMissionToMemory"
        Else
            parametros = Nothing

        End If
  
        If TextBox5.Text = Nothing Then
        Else
            parametros = parametros & " " & "-maxMem=" & TextBox5.Text
        End If
        If TextBox16.Text = Nothing Then
        Else
            parametros = parametros & " " & "-cpuCount=" & TextBox16.Text
        End If
        If TextBox17.Text = Nothing Then
        Else
            parametros = parametros & " " & "-exThreads=" & TextBox17.Text
        End If
        If CheckBox21.Checked = True Then
            parametros = parametros & " " & "-nologs"
        Else
            parametros = parametros

        End If
        If TextBox2.Text = Nothing Then
        Else
            parametros = parametros & " " & TextBox2.Text
        End If
        Try
            TextBox1.Text = "-port=" & TextBox45.Text & " ""-config=" & esusconfig & """" & " " & " ""-cfg=" & esusbasico & """" & " " & " ""-profiles=" & perfilcambiante & """" & " " & " -name=" & ComboBox1.SelectedItem.ToString & " " & parametros & " " & "-mod=" & builder.ToString()
        Catch ex As Exception
        End Try
    End Sub


    Sub leerperfilbasico()
        Try
            Dim cero As String = System.IO.File.ReadAllLines(esusbasico)(0)
            Dim uno As String = System.IO.File.ReadAllLines(esusbasico)(1)
            Dim dos As String = System.IO.File.ReadAllLines(esusbasico)(2)
            Dim tres As String = System.IO.File.ReadAllLines(esusbasico)(3)
            Dim cuatro As String = System.IO.File.ReadAllLines(esusbasico)(4)
            Dim cinco As String = System.IO.File.ReadAllLines(esusbasico)(5)
            Dim seis As String = System.IO.File.ReadAllLines(esusbasico)(6)
            Dim siete As String = System.IO.File.ReadAllLines(esusbasico)(7)
            Dim ocho As String = System.IO.File.ReadAllLines(esusbasico)(8)
            Dim nueve As String = System.IO.File.ReadAllLines(esusbasico)(9)
            Dim catorce As String = System.IO.File.ReadAllLines(esusbasico)(14)
            Dim quince As String = System.IO.File.ReadAllLines(esusbasico)(15)
            Dim kbyte As Integer
            Dim megabyte As Integer
            Dim cara As Integer
            Dim aryText(16) As String
            uno = Replace(uno, "MinBandwidth =", "")
            uno = Replace(uno, ";", "")
            dos = Replace(dos, "MaxBandwidth =", "")
            dos = Replace(dos, ";", "")
            tres = Replace(tres, "MaxMsgSend =", "")
            tres = Replace(tres, ";", "")
            cuatro = Replace(cuatro, "MaxSizeGuaranteed = ", "")
            cuatro = Replace(cuatro, ";", "")
            cinco = Replace(cinco, "MaxSizeNonguaranteed =", "")
            cinco = Replace(cinco, ";", "")
            seis = Replace(seis, "MinErrorToSend = ", "")
            seis = Replace(seis, ";", "")
            siete = Replace(siete, "MinErrorToSendNear =", "")
            siete = Replace(siete, ";", "")
            ocho = Replace(ocho, "MaxCustomFileSize = ", "")
            ocho = Replace(ocho, ";", "")
            nueve = Replace(nueve, "class sockets{maxPacketSize =", "")
            nueve = Replace(nueve, ";};", "")
            catorce = Replace(catorce, "terrainGrid=", "")
            catorce = Replace(catorce, ";", "")
            quince = Replace(quince, "viewDistance=", "")
            quince = Replace(quince, ";", "")
            kbyte = uno
            kbyte = kbyte / 1024
            TextBox7.Text = kbyte
            megabyte = dos
            megabyte = megabyte / 1024 / 1024
            TextBox8.Text = megabyte
            TextBox9.Text = tres
            TextBox10.Text = cuatro
            TextBox11.Text = cinco
            TextBox12.Text = seis
            TextBox13.Text = siete
            cara = ocho
            cara = cara / 1024 / 8
            TextBox14.Text = cara
            TextBox15.Text = nueve
            NumericUpDown1.Text = catorce
            NumericUpDown2.Text = quince
            TextBox9.Text = TextBox9.Text.Replace(" ", "")
            TextBox11.Text = TextBox11.Text.Replace(" ", "")
            TextBox13.Text = TextBox13.Text.Replace(" ", "")
            TextBox15.Text = TextBox15.Text.Replace(" ", "")
        Catch ex As Exception

        End Try

    End Sub
    Sub leerperfilconfig()
        Try
            Dim cero As String = System.IO.File.ReadAllLines(esusconfig)(0)
            Dim uno As String = System.IO.File.ReadAllLines(esusconfig)(1)
            Dim dos As String = System.IO.File.ReadAllLines(esusconfig)(2)
            Dim tres As String = System.IO.File.ReadAllLines(esusconfig)(3)
            Dim cuatro As String = System.IO.File.ReadAllLines(esusconfig)(4)
            Dim cinco As String = System.IO.File.ReadAllLines(esusconfig)(5)
            Dim seis As String = System.IO.File.ReadAllLines(esusconfig)(6)
            Dim siete As String = System.IO.File.ReadAllLines(esusconfig)(7)
            Dim ocho As String = System.IO.File.ReadAllLines(esusconfig)(8)
            Dim nueve As String = System.IO.File.ReadAllLines(esusconfig)(9)
            Dim diez As String = System.IO.File.ReadAllLines(esusconfig)(10)
            Dim once As String = System.IO.File.ReadAllLines(esusconfig)(11)
            Dim doce As String = System.IO.File.ReadAllLines(esusconfig)(12)
            Dim trece As String = System.IO.File.ReadAllLines(esusconfig)(13)
            Dim catorce As String = System.IO.File.ReadAllLines(esusconfig)(14)
            Dim quince As String = System.IO.File.ReadAllLines(esusconfig)(15)
            Dim dieciseis As String = System.IO.File.ReadAllLines(esusconfig)(16)
            Dim diecisiete As String = System.IO.File.ReadAllLines(esusconfig)(17)
            Dim dieciocho As String = System.IO.File.ReadAllLines(esusconfig)(18)
            Dim diecinueve As String = System.IO.File.ReadAllLines(esusconfig)(19)
            Dim veinte As String = System.IO.File.ReadAllLines(esusconfig)(20)
            Dim veintiuno As String = System.IO.File.ReadAllLines(esusconfig)(21)
            Dim veintidos As String = System.IO.File.ReadAllLines(esusconfig)(22)
            Dim veintitres As String = System.IO.File.ReadAllLines(esusconfig)(23)
            Dim veinticuatro As String = System.IO.File.ReadAllLines(esusconfig)(24)
            Dim veinticinco As String = System.IO.File.ReadAllLines(esusconfig)(25)
            Dim veintiseis As String = System.IO.File.ReadAllLines(esusconfig)(26)
            Dim veintisiete As String = System.IO.File.ReadAllLines(esusconfig)(27)
            Dim veintiocho As String = System.IO.File.ReadAllLines(esusconfig)(28)
            Dim veintinueve As String = System.IO.File.ReadAllLines(esusconfig)(29)
            Dim treinta As String = System.IO.File.ReadAllLines(esusconfig)(30)
            Dim treintayuno As String = System.IO.File.ReadAllLines(esusconfig)(31)
            Dim treintaydos As String = System.IO.File.ReadAllLines(esusconfig)(32)
            Dim treintaytres As String = System.IO.File.ReadAllLines(esusconfig)(33)
            Dim treintaycuatro As String = System.IO.File.ReadAllLines(esusconfig)(34)
            Dim treintaycinco As String = System.IO.File.ReadAllLines(esusconfig)(35)
            Dim treintayseis As String = System.IO.File.ReadAllLines(esusconfig)(36)
            Dim treintaysiete As String = System.IO.File.ReadAllLines(esusconfig)(37)
            Dim treintayocho As String = System.IO.File.ReadAllLines(esusconfig)(38)
            Dim treintaynueve As String = System.IO.File.ReadAllLines(esusconfig)(39)
            Dim cuarenta As String = System.IO.File.ReadAllLines(esusconfig)(40)
            Dim cuarentayuno As String = System.IO.File.ReadAllLines(esusconfig)(41)
            Dim cuarentaydos As String = System.IO.File.ReadAllLines(esusconfig)(42)
            Dim cuarentaytres As String = System.IO.File.ReadAllLines(esusconfig)(43)
            Dim cuarentaycuatro As String = System.IO.File.ReadAllLines(esusconfig)(44)
            Dim cuarentaycinco As String = System.IO.File.ReadAllLines(esusconfig)(45)
            Dim cuarentayseis As String = System.IO.File.ReadAllLines(esusconfig)(46)
            Dim cuarentaysiete As String = System.IO.File.ReadAllLines(esusconfig)(47)
            Dim cuarentayocho As String = System.IO.File.ReadAllLines(esusconfig)(48)
            cero = Replace(cero, "steamPort =", "")
            cero = Replace(cero, ";", "")
            uno = Replace(uno, Chr(34), "")
            uno = Replace(uno, ";", "")
            uno = Replace(uno, "=", "")
            dos = Replace(dos, Chr(34), "")
            dos = Replace(dos, ";", "")
            dos = Replace(dos, "=", "")
            tres = Replace(tres, Chr(34), "")
            tres = Replace(tres, ";", "")
            tres = Replace(tres, "=", "")
            If cuatro = Nothing Then
                CheckBox8.Checked = False
            Else
                CheckBox8.Checked = True
            End If
            cinco = Replace(cinco, Chr(34), "")
            cinco = Replace(cinco, "motd[] = {", "")
            cinco = Replace(cinco, ",", "")
            seis = Replace(seis, Chr(34), "")
            seis = Replace(seis, ",", "")
            siete = Replace(siete, Chr(34), "")
            siete = Replace(siete, ",", "")
            ocho = Replace(ocho, Chr(34), "")
            ocho = Replace(ocho, ",", "")
            nueve = Replace(nueve, Chr(34), "")
            nueve = Replace(nueve, ",", "")
            diez = Replace(diez, Chr(34), "")
            diez = Replace(diez, ",", "")
            once = Replace(once, Chr(34), "")
            once = Replace(once, ",", "")
            doce = Replace(doce, Chr(34), "")
            doce = Replace(doce, ",", "")
            trece = Replace(trece, Chr(34), "")
            trece = Replace(trece, ",", "")
            catorce = Replace(catorce, Chr(34), "")
            catorce = Replace(catorce, ",", "")
            TextBox42.Text = cinco & vbNewLine & seis & vbNewLine & siete & vbNewLine & ocho & vbNewLine & nueve & vbNewLine & diez & vbNewLine & once & vbNewLine & doce & vbNewLine & trece & vbNewLine & catorce & vbNewLine & quince

            diecisiete = Replace(diecisiete, "motdInterval =", "")
            diecisiete = Replace(diecisiete, ";", "")

            NumericUpDown4.Text = diecisiete
            diecinueve = Replace(diecinueve, "maxPlayers =", "")
            diecinueve = Replace(diecinueve, ";", "")

            If veintitres = "kickduplicate = 0;" Then
                CheckBox5.Checked = False
            Else
                CheckBox5.Checked = True
            End If
            TextBox43.Text = cero
            TextBox48.Text = Trim(Mid(uno, InStr(uno, " ")))
            TextBox46.Text = Trim(Mid(dos, InStr(dos, " ")))
            TextBox47.Text = Trim(Mid(tres, InStr(tres, " ")))
            NumericUpDown3.Text = diecinueve
            veinticuatro = Replace(veinticuatro, "voteMissionPlayers =", "")
            veinticuatro = Replace(veinticuatro, ";", "")
            NumericUpDown5.Text = veinticuatro
            If veintiuno.Contains("0") Then
                ComboBox4.SelectedIndex = 0
            Else
                ComboBox4.SelectedIndex = 1
            End If
            If veintidos = "requiredSecureId = 0;" Then
                ComboBox5.SelectedIndex = 0
            Else
                ComboBox5.SelectedIndex = 1
            End If
            If veinticinco = "disableVoN = 1;" Then
                CheckBox2.Checked = True
            Else
                CheckBox2.Checked = False
            End If
            If veintiseis = "Headlessclients[]={};" Then
            ElseIf veintiseis = "Headlessclients[]={  };" Then
                CheckBox15.Checked = False
            Else
                CheckBox15.Checked = True
                veintiseis = Replace(veintiseis, "Headlessclients[]={", "")
                veintiseis = Replace(veintiseis, "};", "")
                TextBox62.Text = veintiseis
            End If
            veintisiete = Replace(veintisiete, "voteThreshold =", "")
            veintisiete = Replace(veintisiete, ";", "")
            veintisiete = Replace(veintisiete, "0.", "")
            NumericUpDown6.Text = veintisiete

            veintiocho = Replace(veintiocho, "vonCodecQuality =", "")
            veintiocho = Replace(veintiocho, ";", "")
            NumericUpDown7.Text = veintiocho

            If veintinueve = "persistent = 0;" Then
                CheckBox4.Checked = False
            Else
                CheckBox4.Checked = True
            End If

            If treinta = "timeStampFormat = ""none"";" Then
                ComboBox6.SelectedIndex = 0
            Else
                If treinta = "timeStampFormat = ""short"";" Then
                    ComboBox6.SelectedIndex = 1
                Else
                    ComboBox6.SelectedIndex = 2
                End If

            End If
            If treintayuno = "BattlEye = 0;" Then
                CheckBox6.Checked = False
            Else

                CheckBox6.Checked = True
            End If

            treintaydos = Replace(treintaydos, "doubleIdDetected =", "")
            treintaydos = Replace(treintaydos, ";", "")
            treintaytres = Replace(treintaytres, "onUserConnected =", "")
            treintaytres = Replace(treintaytres, ";", "")
            treintaycuatro = Replace(treintaycuatro, "onUserDisconnected =", "")
            treintaycuatro = Replace(treintaycuatro, ";", "")
            treintaycinco = Replace(treintaycinco, "onHackedData =", "")
            treintaycinco = Replace(treintaycinco, ";", "")
            treintayseis = Replace(treintayseis, "onDifferentData =", "")
            treintayseis = Replace(treintayseis, ";", "")
            treintaysiete = Replace(treintaysiete, "onUnsignedData =", "")
            treintaysiete = Replace(treintaysiete, ";", "")
            treintayocho = Replace(treintayocho, "regularCheck =", "")
            treintayocho = Replace(treintayocho, ";", "")
            TextBox39.Text = treintaydos
            TextBox38.Text = treintaytres
            TextBox37.Text = treintaycuatro
            TextBox36.Text = treintaycinco
            TextBox35.Text = treintayseis
            TextBox34.Text = treintaysiete
            TextBox33.Text = treintayocho
            If treintaynueve = "forceRotorLibSimulation = 0;" Then
                ComboBox3.SelectedIndex = 0
            Else
                ComboBox3.SelectedIndex = 1
            End If
            cuarenta = Replace(cuarenta, "requiredBuild =", "")
            cuarenta = Replace(cuarenta, ";", "")
            TextBox21.Text = cuarenta
            cuarentaycinco = Replace(cuarentaycinco, Chr(34), "")
            cuarentaycinco = Replace(cuarentaycinco, ";", "")
            cuarentaycinco = Replace(cuarentaycinco, "=", "")
            misionchecked = Trim(Mid(cuarentaycinco, InStr(cuarentaycinco, " ")))
            If misionchecked.Contains("&vacio&") Then
                CheckedListBox2.Items.Clear()
                For Each File In System.IO.Directory.GetFiles(mpmision, "*.pbo")
                    Dim primeraparte As String = Path.GetFileName(File).Substring(0, Path.GetFileName(File).LastIndexOf("."))
                    CheckedListBox2.Items.Add(Path.GetFileName(primeraparte))
                Next
                For Each folder In System.IO.Directory.GetDirectories(mpmision)
                    CheckedListBox2.Items.Add(Path.GetFileName(folder))
                Next
            Else
                Dim I As Integer
                For I = 0 To CheckedListBox2.Items.Count - 1
                    If CheckedListBox2.Items(I).IndexOf(misionchecked) <> -1 Then
                        CheckedListBox2.SetItemChecked(I, True)
                    Else

                    End If
                Next
                TextBox66.Text = CheckedListBox2.CheckedItems.Count
            End If
            If cuarentayseis = "difficulty = ""recruit"";" Then
                ComboBox8.Visible = True
                ComboBox7.SelectedIndex = 0
                ComboBox8.Visible = True
                TextBox22.Visible = True
                TextBox26.Visible = True
                ComboBox9.Visible = False
                ComboBox10.Visible = False
                ComboBox11.Visible = False
                TextBox23.Visible = False
                TextBox24.Visible = False
                TextBox25.Visible = False
                TextBox27.Visible = False
                TextBox28.Visible = False
                TextBox29.Visible = False

            ElseIf cuarentayseis = "difficulty = ""regular"";" Then
                ComboBox7.SelectedIndex = 1
                ComboBox8.Visible = False
                TextBox22.Visible = False
                TextBox26.Visible = False
                ComboBox9.Visible = True
                ComboBox10.Visible = False
                ComboBox11.Visible = False
                TextBox23.Visible = True
                TextBox24.Visible = False
                TextBox25.Visible = False
                TextBox27.Visible = True
                TextBox28.Visible = False
                TextBox29.Visible = False

            End If
            If cuarentayseis = "difficulty = ""veteran"";" Then
                ComboBox7.SelectedIndex = 2
                ComboBox8.Visible = False
                TextBox22.Visible = False
                TextBox26.Visible = False
                ComboBox9.Visible = False
                ComboBox10.Visible = True
                ComboBox11.Visible = False
                TextBox23.Visible = False
                TextBox24.Visible = True
                TextBox25.Visible = False
                TextBox27.Visible = False
                TextBox28.Visible = True
                TextBox29.Visible = False
            ElseIf cuarentayseis = "difficulty = ""mercenary"";" Then
                ComboBox7.SelectedIndex = 3
                ComboBox8.Visible = False
                TextBox22.Visible = False
                TextBox26.Visible = False
                ComboBox9.Visible = False
                ComboBox10.Visible = False
                ComboBox11.Visible = True
                TextBox23.Visible = False
                TextBox24.Visible = False
                TextBox25.Visible = True
                TextBox27.Visible = False
                TextBox28.Visible = False
                TextBox29.Visible = True
            End If
        Catch ex As Exception
        End Try

    End Sub
    Private Sub Button9_Click(sender As Object, e As EventArgs) Handles Button9.Click
        Dim checked As Boolean = False
        For i As Integer = 0 To CheckedListBox1.Items.Count - 1
            CheckedListBox1.SetItemChecked(i, checked)
        Next
        ListBox1.Items.Clear()
        ListBox2.Items.Clear()
        If ListBox1.Items.Count > 0 Then
            For i As Integer = 0 To ListBox1.Items.Count - 1
                ListBox2.Items.Add(ListBox1.Items(i))
            Next
        Else
            ListBox2.Items.Clear()
        End If
        ListBox3.Items.Clear()
        TextBox67.Text = CheckedListBox1.CheckedItems.Count
        Call construirparametro()

    End Sub

    Private Sub Button7_Click(sender As Object, e As EventArgs) Handles Button7.Click
        For i As Integer = 0 To CheckedListBox1.Items.Count - 1
            If CheckedListBox1.GetItemChecked(i) = True Then
                CheckedListBox1.SetItemChecked(i, False)
            ElseIf CheckedListBox1.GetItemChecked(i) = False Then
                CheckedListBox1.SetItemChecked(i, True)
            End If
        Next
        ListBox1.Items.Clear()
        If CheckedListBox1.CheckedItems.Count > 0 Then
            For i As Integer = 0 To CheckedListBox1.CheckedItems.Count - 1
                ListBox1.Items.Add(CheckedListBox1.CheckedItems(i))
            Next
        End If
        ListBox2.Items.Clear()
        If ListBox1.Items.Count > 0 Then
            For i As Integer = 0 To ListBox1.Items.Count - 1
                ListBox2.Items.Add(ListBox1.Items(i))
            Next
        Else
            ListBox2.Items.Clear()
        End If
        ListBox3.Items.Clear()
        TextBox67.Text = CheckedListBox1.CheckedItems.Count
        Call construirparametro()


    End Sub

    Private Sub Button8_Click(sender As Object, e As EventArgs) Handles Button8.Click
        Dim checked As Boolean = True
        For i As Integer = 0 To CheckedListBox1.Items.Count - 1
            CheckedListBox1.SetItemChecked(i, checked)
        Next
        ListBox1.Items.Clear()
        If CheckedListBox1.CheckedItems.Count > 0 Then
            For i As Integer = 0 To CheckedListBox1.CheckedItems.Count - 1
                ListBox1.Items.Add(CheckedListBox1.CheckedItems(i))
            Next
        End If
        ListBox2.Items.Clear()
        If ListBox1.Items.Count > 0 Then
            For i As Integer = 0 To ListBox1.Items.Count - 1
                ListBox2.Items.Add(ListBox1.Items(i))
            Next
        Else
            ListBox2.Items.Clear()
        End If
        ListBox3.Items.Clear()
        TextBox67.Text = CheckedListBox1.CheckedItems.Count
        Call construirparametro()


    End Sub

    Private Sub Button6_Click(sender As Object, e As EventArgs) Handles Button6.Click
        CheckedListBox1.Items.Clear()
        For Each folder In System.IO.Directory.GetDirectories(rutaaddons, "@*")
            CheckedListBox1.Items.Add(Path.GetFileName(folder))
        Next
        ListBox2.Items.Clear()
        If ListBox1.Items.Count > 0 Then
            For i As Integer = 0 To ListBox1.Items.Count - 1
                ListBox2.Items.Add(ListBox1.Items(i))
            Next
        Else
            ListBox2.Items.Clear()
        End If
        ListBox3.Items.Clear()
        ListBox2.Items.Clear()
        ListBox1.Items.Clear()
        TextBox67.Text = CheckedListBox1.CheckedItems.Count
        TextBox68.Text = CheckedListBox1.Items.Count
        Call construirparametro()
        Call leermods()

    End Sub


    Private Sub CheckedListBox1_SelectedIndexChanged(sender As Object, e As EventArgs) Handles CheckedListBox1.SelectedIndexChanged
        ListBox1.Items.Clear()
        If CheckedListBox1.CheckedItems.Count > 0 Then
            For i As Integer = 0 To CheckedListBox1.CheckedItems.Count - 1
                ListBox1.Items.Add(CheckedListBox1.CheckedItems(i))
            Next
        Else
            ListBox1.Items.Clear()
        End If
        ListBox2.Items.Clear()
        If ListBox1.Items.Count > 0 Then
            For i As Integer = 0 To ListBox1.Items.Count - 1
                ListBox2.Items.Add(ListBox1.Items(i))
            Next
        Else
            ListBox2.Items.Clear()
        End If
        ListBox3.Items.Clear()
        TextBox67.Text = CheckedListBox1.CheckedItems.Count
        Call construirparametro()
    End Sub

    Private Sub ComboBox2_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ComboBox2.SelectedIndexChanged
        Try
            CheckedListBox3.Visible = True
            CheckedListBox3.Items.Clear()
            For Each File In System.IO.Directory.GetFiles(mpmision, "*.pbo")
                If Path.GetFileName(File).IndexOf(ComboBox2.SelectedItem.ToString, 0, StringComparison.CurrentCultureIgnoreCase) > -1 Then
                    Dim primeraparte As String = Path.GetFileName(File).Substring(0, Path.GetFileName(File).LastIndexOf("."))
                    CheckedListBox3.Items.Add(Path.GetFileName(primeraparte))
                End If
            Next
            For Each folder In System.IO.Directory.GetDirectories(mpmision)
                CheckedListBox3.Items.Add(Path.GetFileName(folder))
            Next
            Dim itemChecked As Object = Nothing
            Dim elegido As String = Nothing
            For Each itemChecked In CheckedListBox2.CheckedItems
                elegido = itemChecked
            Next
            For a = 0 To CheckedListBox3.Items.Count - 1

                If CheckedListBox3.Items(a).indexof(elegido) <> -1 Then
                    CheckedListBox3.SetItemChecked(a, True)
                End If
            Next
            CheckedListBox2.Visible = False
        Catch ex As Exception

        End Try
    End Sub

    Private Sub CheckedListBox3_SelectedIndexChanged(sender As Object, e As EventArgs) Handles CheckedListBox3.SelectedIndexChanged
        CheckedListBox3.Visible = True
        CheckedListBox2.Visible = False

        Dim itemChecked As Object = Nothing
        Dim elegido As String = Nothing
        For Each itemChecked In CheckedListBox3.CheckedItems
            elegido = itemChecked
        Next
        Dim checked As Boolean = False
        For i As Integer = 0 To CheckedListBox2.Items.Count - 1
            CheckedListBox2.SetItemChecked(i, checked)
        Next
        TextBox66.Text = CheckedListBox3.CheckedItems.Count
    End Sub

    Private Sub CheckedListBox2_ItemCheck(ByVal sender As Object, ByVal e As System.Windows.Forms.ItemCheckEventArgs) Handles CheckedListBox2.ItemCheck
        If e.NewValue = CheckState.Checked Then
            For i As Integer = 0 To Me.CheckedListBox2.Items.Count - 1 Step 1
                If i <> e.Index Then
                    Me.CheckedListBox2.SetItemChecked(i, False)
                End If
            Next i
        End If
    End Sub
    Private Sub CheckedListBox3_ItemCheck(ByVal sender As Object, ByVal e As System.Windows.Forms.ItemCheckEventArgs) Handles CheckedListBox3.ItemCheck
        If e.NewValue = CheckState.Checked Then
            For i As Integer = 0 To Me.CheckedListBox3.Items.Count - 1 Step 1
                If i <> e.Index Then
                    Me.CheckedListBox3.SetItemChecked(i, False)
                End If
            Next i
        End If
    End Sub

    Private Sub CheckedListBox2_SelectedIndexChanged(sender As Object, e As EventArgs) Handles CheckedListBox2.SelectedIndexChanged

        TextBox66.Text = CheckedListBox2.CheckedItems.Count
    End Sub

    Private Sub Button10_Click(sender As Object, e As EventArgs) Handles Button10.Click
        Call leerperfilconfig()
        ComboBox2.SelectedItem = Nothing
        Dim itemChecked As Object = Nothing
        Dim elegido As String = Nothing
        For Each itemChecked In CheckedListBox3.CheckedItems
            elegido = itemChecked
        Next
        For a = 0 To CheckedListBox2.Items.Count - 1
            Try
                If CheckedListBox2.Items(a).indexof(elegido) <> -1 Then
                    CheckedListBox2.SetItemChecked(a, True)
                End If
            Catch ex As Exception

            End Try

        Next
        CheckedListBox2.Visible = True
        CheckedListBox3.Visible = False
    End Sub

    Private Sub arriba_Click(sender As Object, e As EventArgs) Handles arriba.Click
        If ListBox1.SelectedItem = Nothing Then
        Else
            If ListBox1.SelectedIndex > 0 Then
                Dim I = ListBox1.SelectedIndex - 1
                ListBox1.Items.Insert(I, ListBox1.SelectedItem)
                ListBox1.Items.RemoveAt(ListBox1.SelectedIndex)
                ListBox1.SelectedIndex = I
            End If
        End If
        ListBox2.Items.Clear()
        If ListBox1.Items.Count > 0 Then
            For i As Integer = 0 To ListBox1.Items.Count - 1
                ListBox2.Items.Add(ListBox1.Items(i))
            Next
        Else
            ListBox2.Items.Clear()
        End If
        ListBox3.Items.Clear()
        Call construirparametro()
    End Sub

    Private Sub abajo_Click(sender As Object, e As EventArgs) Handles abajo.Click
        If ListBox1.SelectedItem = Nothing Then
        Else
            If ListBox1.SelectedIndex < ListBox1.Items.Count - 1 Then
                Dim I = ListBox1.SelectedIndex + 2
                ListBox1.Items.Insert(I, ListBox1.SelectedItem)
                ListBox1.Items.RemoveAt(ListBox1.SelectedIndex)
                ListBox1.SelectedIndex = I - 1
            End If
        End If
        ListBox2.Items.Clear()
        If ListBox1.Items.Count > 0 Then
            For i As Integer = 0 To ListBox1.Items.Count - 1
                ListBox2.Items.Add(ListBox1.Items(i))
            Next
        Else
            ListBox2.Items.Clear()
        End If
        ListBox3.Items.Clear()
        Call construirparametro()
    End Sub

    Private Sub moverarriba_Click(sender As Object, e As EventArgs) Handles moverarriba.Click
        Dim item As String
        If ListBox1.SelectedItem = Nothing Then
            item = Nothing
        Else
            item = ListBox1.SelectedItem
            ListBox1.Items.Remove(item)
            ListBox1.Items.Insert(0, item)
            ListBox1.SelectedIndex = 0
        End If
        ListBox2.Items.Clear()
        If ListBox1.Items.Count > 0 Then
            For i As Integer = 0 To ListBox1.Items.Count - 1
                ListBox2.Items.Add(ListBox1.Items(i))
            Next
        Else
            ListBox2.Items.Clear()
        End If
        ListBox3.Items.Clear()
        Call construirparametro()
    End Sub

    Private Sub moverabajo_Click(sender As Object, e As EventArgs) Handles moverabajo.Click
        Dim limite As Integer = (ListBox1.Items.Count - 1)
        Dim item As String
        If ListBox1.SelectedItem = Nothing Then
            item = Nothing
        Else
            item = ListBox1.SelectedItem
            ListBox1.Items.Remove(item)
            ListBox1.Items.Insert(limite, item)
            ListBox1.SelectedIndex = limite
        End If
        ListBox2.Items.Clear()
        If ListBox1.Items.Count > 0 Then
            For i As Integer = 0 To ListBox1.Items.Count - 1
                ListBox2.Items.Add(ListBox1.Items(i))
            Next
        Else
            ListBox2.Items.Clear()
        End If
        ListBox3.Items.Clear()
        Call construirparametro()
    End Sub

    Private Sub ListBox1_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ListBox1.SelectedIndexChanged

    End Sub

    Private Sub Button14_Click(sender As Object, e As EventArgs) Handles Button14.Click
        Dim item As String
        If ListBox2.SelectedItem = Nothing Then
            item = Nothing
        Else
            item = ListBox2.SelectedItem
            ListBox2.Items.Remove(item)
            ListBox2.Items.Insert(0, item)
            ListBox2.SelectedIndex = 0
        End If
    End Sub

    Private Sub Button13_Click(sender As Object, e As EventArgs) Handles Button13.Click
        If ListBox2.SelectedItem = Nothing Then
        Else
            If ListBox2.SelectedIndex > 0 Then
                Dim I = ListBox2.SelectedIndex - 1
                ListBox2.Items.Insert(I, ListBox2.SelectedItem)
                ListBox2.Items.RemoveAt(ListBox2.SelectedIndex)
                ListBox2.SelectedIndex = I
            End If
        End If
    End Sub

    Private Sub Button12_Click(sender As Object, e As EventArgs) Handles Button12.Click
        If ListBox2.SelectedItem = Nothing Then
        Else
            If ListBox2.SelectedIndex < ListBox2.Items.Count - 1 Then
                Dim I = ListBox2.SelectedIndex + 2
                ListBox2.Items.Insert(I, ListBox2.SelectedItem)
                ListBox2.Items.RemoveAt(ListBox2.SelectedIndex)
                ListBox2.SelectedIndex = I - 1
            End If
        End If
    End Sub

    Private Sub Button11_Click(sender As Object, e As EventArgs) Handles Button11.Click
        Dim limite As Integer = (ListBox2.Items.Count - 1)
        Dim item As String
        If ListBox2.SelectedItem = Nothing Then
            item = Nothing
        Else
            item = ListBox2.SelectedItem
            ListBox2.Items.Remove(item)
            ListBox2.Items.Insert(limite, item)
            ListBox2.SelectedIndex = limite
        End If
    End Sub

    Private Sub Button15_Click(sender As Object, e As EventArgs) Handles Button15.Click
        If ListBox2.SelectedItem = Nothing Then
        Else
            ListBox2.SelectedItem = ListBox3.Items.Add(ListBox2.SelectedItem)
            ListBox2.Items.Remove(ListBox2.SelectedItem)
        End If
    End Sub

    Private Sub Button16_Click(sender As Object, e As EventArgs) Handles Button16.Click
        If ListBox3.SelectedItem = Nothing Then
        Else
            ListBox3.SelectedItem = ListBox2.Items.Add(ListBox3.SelectedItem)
            ListBox3.Items.Remove(ListBox3.SelectedItem)
        End If
    End Sub


    Private Sub Button1_Click(sender As Object, e As EventArgs)



    End Sub

    Private Sub Label9_Click(sender As Object, e As EventArgs) Handles Label9.Click
    End Sub


    Private Sub Button5_Click(sender As Object, e As EventArgs) Handles Button5.Click
        Label19.Visible = True
        Label19.Refresh()
        System.Threading.Thread.Sleep(250)

        If ComboBox1.SelectedItem = Nothing Then
        Else
            perfilcambiante = esusservera3 & "\" & ComboBox1.SelectedItem.ToString
            mods = Combine(perfilcambiante, addon)
        End If
        If System.IO.File.Exists(esusbasico) = True Then
            Dim kbyte As Integer
            Dim megabyte As Integer
            Dim aryText(16) As String
            Dim cara As Integer

            If TextBox7.Text = Nothing Then
            Else
                kbyte = TextBox7.Text
                kbyte = kbyte * 1024
            End If
            If TextBox8.Text = Nothing Then
            Else
                megabyte = TextBox8.Text
                megabyte = megabyte * 1024 * 1024
            End If
            If TextBox14.Text = Nothing Then
            Else
                cara = TextBox14.Text
                cara = cara * 1024 * 8
            End If
            aryText(0) = "adapter=-1;"
            aryText(1) = "MinBandwidth = " + kbyte.ToString & ";"
            aryText(2) = "MaxBandwidth = " + megabyte.ToString & ";"
            aryText(3) = "MaxMsgSend = " + TextBox9.Text & ";"
            aryText(4) = "MaxSizeGuaranteed = " + TextBox10.Text & ";"
            aryText(5) = "MaxSizeNonguaranteed = " + TextBox11.Text & ";"
            aryText(6) = "MinErrorToSend = " + TextBox12.Text & ";"
            aryText(7) = "MinErrorToSendNear = " + TextBox13.Text & ";"
            aryText(8) = "MaxCustomFileSize = " + cara.ToString & ";"
            aryText(9) = "class sockets{maxPacketSize = " & TextBox15.Text & ";};"
            aryText(10) = "3D_Performance=1;"
            aryText(11) = "Resolution_W=0;"
            aryText(12) = "Resolution_H=0;"
            aryText(13) = "Resolution_Bpp=32;"
            aryText(14) = "terrainGrid=" + NumericUpDown1.Text & ";"
            aryText(15) = "viewDistance=" + NumericUpDown2.Text & ";"
            aryText(16) = "Windowed=0;"

            Using sw As New StreamWriter(esusbasico)
                For x = 0 To 16
                    sw.WriteLine(aryText(x))
                Next
                sw.Close()
            End Using
        Else
            MsgBox("Ha habido un problema y no ha podido realizarse el guardado")
        End If

        If System.IO.File.Exists(dificultad) = True Then
            Dim aryText(139) As String
            Dim x As Integer
            Dim y As String = ""
            For x = 0 To CheckedListBox4.CheckedItems.Count - 1
                y = y & CheckedListBox4.CheckedItems(x).ToString & ControlChars.CrLf
            Next x
            aryText(0) = "class Difficulties"
            aryText(1) = "{"
            aryText(2) = "class Recruit"
            aryText(3) = "{"
            aryText(4) = "class Flags"
            aryText(5) = "{"

            If y.Contains("3rdPersonView") Then
                aryText(6) = "3rdPersonView=1;"
            Else
                aryText(6) = "3rdPersonView=0;"
            End If
            If y.Contains("armor") Then
                aryText(7) = "armor=1;"

            Else
                aryText(7) = "armor=0;"

            End If
            If y.Contains("autoAim") Then
                aryText(8) = "autoAim=1;"

            Else
                aryText(8) = "autoAim=0;"

            End If
            If y.Contains("autoGuideAT") Then
                aryText(9) = "autoGuideAT=1;"

            Else
                aryText(9) = "autoGuideAT=0;"

            End If
            If y.Contains("autoSpot") Then
                aryText(10) = "autoSpot=1;"

            Else
                aryText(10) = "autoSpot=0;"

            End If
            If y.Contains("cameraShake") Then
                aryText(11) = "cameraShake=1;"

            Else
                aryText(11) = "cameraShake=0;"

            End If
            If y.Contains("clockIndicator") Then
                aryText(12) = "clockIndicator=1;"

            Else
                aryText(12) = "clockIndicator=0;"

            End If
            If y.Contains("deathMessages") Then
                aryText(13) = "deathMessages=1;"

            Else
                aryText(13) = "deathMessages=0;"

            End If
            If y.Contains("enemyTag") Then
                aryText(14) = "enemyTag=1;"

            Else
                aryText(14) = "enemyTag=0;"


            End If
            If y.Contains("friendlyTag") Then
                aryText(15) = "friendlyTag=1;"

            Else
                aryText(15) = "friendlyTag=0;"

            End If
            If y.Contains("hud") Then
                aryText(16) = "hud=1;"

            Else
                aryText(16) = "hud=0;"

            End If
            If y.Contains("hudGroupInfo") Then
                aryText(17) = "hudGroupInfo=1;"

            Else
                aryText(17) = "hudGroupInfo=0;"

            End If
            If y.Contains("hudPerm") Then
                aryText(18) = "hudPerm=1;"

            Else
                aryText(18) = "hudPerm=0;"

            End If
            If y.Contains("hudWp") Then
                aryText(19) = "hudWp=1;"

            Else
                aryText(19) = "hudWp=0;"

            End If
            If y.Contains("hudWpPerm") Then
                aryText(20) = "hudWpPerm=1;"

            Else
                aryText(20) = "hudWpPerm=0;"

            End If
            If y.Contains("map") Then
                aryText(21) = "map=1;"

            Else
                aryText(21) = "map=0;"

            End If
            If y.Contains("netStats") Then
                aryText(22) = "netStats=1;"

            Else
                aryText(22) = "netStats=0;"

            End If
            If y.Contains("tracers") Then
                aryText(23) = "tracers=1;"

            Else
                aryText(23) = "tracers=0;"

            End If
            If y.Contains("ultraAI") Then
                aryText(24) = "ultraAI=1;"

            Else
                aryText(24) = "ultraAI=0;"

            End If
            If y.Contains("unlimitedSaves") Then
                aryText(25) = "unlimitedSaves=1;"

            Else
                aryText(25) = "unlimitedSaves=0;"

            End If
            If y.Contains("vonID") Then
                aryText(26) = "vonID=1;"

            Else
                aryText(26) = "vonID=0;"

            End If
            If y.Contains("weaponCursor") Then
                aryText(27) = "weaponCursor=1;"

            Else
                aryText(27) = "weaponCursor=0;"

            End If
            If y.Contains("ExtendetInfoType") Then
                aryText(28) = "ExtendetInfoType=1;"

            Else
                aryText(28) = "ExtendetInfoType=0;"

            End If
            If y.Contains("MineTag") Then
                aryText(29) = "MineTag=1;"

            Else
                aryText(29) = "MineTag=0;"

            End If

            aryText(30) = "};"
            Try
                aryText(31) = "aiLevelPreset=" + ComboBox8.SelectedIndex.ToString & ";"

            Catch ex As Exception

            End Try
            aryText(32) = "skillAI=" + TextBox22.Text & ";"
            aryText(33) = "precisionAI=" + TextBox26.Text & ";"
            aryText(35) = "};"
            aryText(36) = "class Regular"
            aryText(37) = "{"
            aryText(38) = "class Flags"
            aryText(39) = "{"
            Dim r As Integer
            Dim t As String = ""
            For r = 0 To CheckedListBox5.CheckedItems.Count - 1
                t = t & CheckedListBox5.CheckedItems(r).ToString & ControlChars.CrLf
            Next r
            If t.Contains("3rdPersonView") Then
                aryText(40) = "3rdPersonView=1;"
            Else
                aryText(40) = "3rdPersonView=0;"
            End If
            If t.Contains("armor") Then
                aryText(41) = "armor=1;"

            Else
                aryText(41) = "armor=0;"

            End If
            If t.Contains("autoAim") Then
                aryText(42) = "autoAim=1;"

            Else
                aryText(42) = "autoAim=0;"

            End If
            If t.Contains("autoGuideAT") Then
                aryText(43) = "autoGuideAT=1;"

            Else
                aryText(43) = "autoGuideAT=0;"

            End If
            If t.Contains("autoSpot") Then
                aryText(44) = "autoSpot=1;"

            Else
                aryText(44) = "autoSpot=0;"

            End If
            If t.Contains("cameraShake") Then
                aryText(45) = "cameraShake=1;"

            Else
                aryText(45) = "cameraShake=0;"

            End If
            If t.Contains("clockIndicator") Then
                aryText(46) = "clockIndicator=1;"

            Else
                aryText(46) = "clockIndicator=0;"

            End If
            If t.Contains("deathMessages") Then
                aryText(47) = "deathMessages=1;"

            Else
                aryText(47) = "deathMessages=0;"

            End If
            If t.Contains("enemyTag") Then
                aryText(48) = "enemyTag=1;"

            Else
                aryText(48) = "enemyTag=0;"


            End If
            If t.Contains("friendlyTag") Then
                aryText(49) = "friendlyTag=1;"

            Else
                aryText(49) = "friendlyTag=0;"

            End If
            If t.Contains("hud") Then
                aryText(50) = "hud=1;"

            Else
                aryText(50) = "hud=0;"

            End If
            If t.Contains("hudGroupInfo") Then
                aryText(51) = "hudGroupInfo=1;"

            Else
                aryText(51) = "hudGroupInfo=0;"

            End If
            If t.Contains("hudPerm") Then
                aryText(52) = "hudPerm=1;"

            Else
                aryText(52) = "hudPerm=0;"

            End If
            If t.Contains("hudWp") Then
                aryText(53) = "hudWp=1;"

            Else
                aryText(53) = "hudWp=0;"

            End If
            If t.Contains("hudWpPerm") Then
                aryText(54) = "hudWpPerm=1;"

            Else
                aryText(54) = "hudWpPerm=0;"

            End If
            If t.Contains("map") Then
                aryText(55) = "map=1;"

            Else
                aryText(55) = "map=0;"

            End If
            If t.Contains("netStats") Then
                aryText(56) = "netStats=1;"

            Else
                aryText(56) = "netStats=0;"

            End If
            If t.Contains("tracers") Then
                aryText(57) = "tracers=1;"

            Else
                aryText(57) = "tracers=0;"

            End If
            If t.Contains("ultraAI") Then
                aryText(58) = "ultraAI=1;"

            Else
                aryText(58) = "ultraAI=0;"

            End If
            If t.Contains("unlimitedSaves") Then
                aryText(59) = "unlimitedSaves=1;"

            Else
                aryText(59) = "unlimitedSaves=0;"

            End If
            If t.Contains("vonID") Then
                aryText(60) = "vonID=1;"

            Else
                aryText(60) = "vonID=0;"

            End If
            If t.Contains("weaponCursor") Then
                aryText(61) = "weaponCursor=1;"

            Else
                aryText(61) = "weaponCursor=0;"

            End If
            If t.Contains("ExtendetInfoType") Then
                aryText(62) = "ExtendetInfoType=1;"

            Else
                aryText(62) = "ExtendetInfoType=0;"

            End If
            If t.Contains("MineTag") Then
                aryText(63) = "MineTag=1;"

            Else
                aryText(63) = "MineTag=0;"

            End If
            aryText(64) = "};"
            Try

                aryText(65) = "aiLevelPreset=" + ComboBox9.SelectedIndex.ToString & ";"
              
            Catch ex As Exception

        End Try
            aryText(66) = "skillAI=" + TextBox23.Text & ";"
            aryText(67) = "precisionAI=" + TextBox27.Text & ";"
            aryText(69) = "};"
            aryText(70) = "class Veteran"
            aryText(71) = "{"
            aryText(72) = "class Flags"
            aryText(73) = "{"
            Dim a As Integer
            Dim c As String = ""
            For a = 0 To CheckedListBox6.CheckedItems.Count - 1
                c = c & CheckedListBox6.CheckedItems(a).ToString & ControlChars.CrLf
            Next a
            If c.Contains("3rdPersonView") Then
                aryText(74) = "3rdPersonView=1;"
            Else
                aryText(74) = "3rdPersonView=0;"
            End If

            aryText(75) = "armor=0;"
            aryText(76) = "autoAim=0;"
            aryText(77) = "autoGuideAT=0;"
            aryText(78) = "autoSpot=0;"

            If c.Contains("cameraShake") Then
                aryText(79) = "cameraShake=1;"

            Else
                aryText(79) = "cameraShake=0;"

            End If
            If c.Contains("clockIndicator") Then
                aryText(80) = "clockIndicator=1;"

            Else
                aryText(80) = "clockIndicator=0;"

            End If
            If c.Contains("deathMessages") Then
                aryText(81) = "deathMessages=1;"

            Else
                aryText(81) = "deathMessages=0;"

            End If

            aryText(82) = "enemyTag=0;"



            aryText(83) = "friendlyTag=0;"

            If c.Contains("hud") Then
                aryText(84) = "hud=1;"

            Else
                aryText(84) = "hud=0;"

            End If
            If c.Contains("hudGroupInfo") Then
                aryText(85) = "hudGroupInfo=1;"

            Else
                aryText(85) = "hudGroupInfo=0;"

            End If
            If c.Contains("hudPerm") Then
                aryText(86) = "hudPerm=1;"

            Else
                aryText(86) = "hudPerm=0;"

            End If
            If c.Contains("hudWp") Then
                aryText(87) = "hudWp=1;"

            Else
                aryText(87) = "hudWp=0;"

            End If

            aryText(88) = "hudWpPerm=0;"

            If c.Contains("map") Then
                aryText(89) = "map=1;"

            Else
                aryText(89) = "map=0;"

            End If
            If c.Contains("netStats") Then
                aryText(90) = "netStats=1;"

            Else
                aryText(90) = "netStats=0;"

            End If

            aryText(91) = "tracers=0;"
            If c.Contains("ultraAI") Then
                aryText(92) = "ultraAI=1;"

            Else
                aryText(92) = "ultraAI=0;"

            End If
            If c.Contains("unlimitedSaves") Then
                aryText(93) = "unlimitedSaves=1;"

            Else
                aryText(93) = "unlimitedSaves=0;"

            End If
            If c.Contains("vonID") Then
                aryText(94) = "vonID=1;"

            Else
                aryText(94) = "vonID=0;"

            End If
            If c.Contains("weaponCursor") Then
                aryText(95) = "weaponCursor=1;"

            Else
                aryText(95) = "weaponCursor=0;"

            End If
            If c.Contains("ExtendetInfoType") Then
                aryText(96) = "ExtendetInfoType=1;"

            Else
                aryText(96) = "ExtendetInfoType=0;"

            End If
            If c.Contains("MineTag") Then
                aryText(97) = "MineTag=1;"

            Else
                aryText(97) = "MineTag=0;"

            End If
            aryText(98) = "};"
            Try
                aryText(99) = "aiLevelPreset=" + ComboBox10.SelectedIndex.ToString & ";"

            Catch ex As Exception

            End Try
            aryText(100) = "skillAI=" + TextBox24.Text & ";"
            aryText(101) = "precisionAI=" + TextBox28.Text & ";"
            aryText(103) = "};"
            aryText(104) = "class Mercenary"
            aryText(105) = "{"
            aryText(106) = "class Flags"
            aryText(107) = "{"


       

            aryText(108) = "3rdPersonView=0;"

            aryText(109) = "armor=0;"

            aryText(110) = "autoAim=0;"

            aryText(111) = "autoGuideAT=0;"

            aryText(112) = "autoSpot=0;"

            aryText(113) = "cameraShake=0;"
            aryText(114) = "clockIndicator=0;"




            Dim p As Integer
            Dim q As String = ""

            For p = 0 To CheckedListBox7.CheckedItems.Count - 1
                q = q & CheckedListBox7.CheckedItems(y).ToString & ControlChars.CrLf
            Next p
            If q.Contains("deathMessages") Then
                aryText(115) = "deathMessages=1;"

            Else
                aryText(115) = "deathMessages=0;"

            End If




            aryText(116) = "enemyTag=0;"


            aryText(117) = "friendlyTag=0;"


            aryText(118) = "hud=0;"


            aryText(119) = "hudGroupInfo=0;"

            aryText(120) = "hudPerm=0;"


            aryText(121) = "hudWp=0;"

            aryText(122) = "hudWpPerm=0;"


            aryText(123) = "map=0;"


            If q.Contains("netStats") Then
                aryText(124) = "netStats=1;"

            Else
                aryText(124) = "netStats=0;"

            End If

            aryText(125) = "tracers=0;"

            If q.Contains("ultraAI") Then
                aryText(126) = "ultraAI=1;"

            Else
                aryText(126) = "ultraAI=0;"

            End If

            aryText(127) = "unlimitedSaves=0;"

            If q.Contains("vonID") Then
                aryText(128) = "vonID=1;"

            Else
                aryText(128) = "vonID=0;"

            End If

            aryText(129) = "weaponCursor=0;"

            If q.Contains("ExtendetInfoType") Then
                aryText(130) = "ExtendetInfoType=1;"

            Else
                aryText(130) = "ExtendetInfoType=0;"

            End If
            If q.Contains("MineTag") Then
                aryText(131) = "MineTag=1;"

            Else
                aryText(131) = "MineTag=0;"

            End If
            aryText(132) = "};"
            Try
                aryText(133) = "aiLevelPreset=" + ComboBox11.SelectedIndex.ToString & ";"

            Catch ex As Exception

            End Try
            aryText(134) = "skillAI=" + TextBox25.Text & ";"
            aryText(135) = "precisionAI=" + TextBox29.Text & ";"
            aryText(137) = "};"
            aryText(138) = "};"
            Dim index7 As Integer
            index7 = ComboBox7.SelectedIndex
            Select Case index7
                Case 0
                    aryText(139) = "difficulty = """ & "recruit" & """" & ";"
                Case 1
                    aryText(139) = "difficulty = """ & "regular" & """" & ";"

                Case 2
                    aryText(139) = "difficulty = """ & "veteran" & """" & ";"

                Case 3
                    aryText(139) = "difficulty = """ & "mercenary" & """" & ";"

            End Select

            Using sw As New StreamWriter(dificultad)
                For x = 0 To 139
                    sw.WriteLine(aryText(x))
                Next
                sw.Close()

            End Using

        End If

        If System.IO.File.Exists(esusconfig) = True Then

            Dim i As Integer
            Dim aryText(48) As String
            aryText(0) = "steamPort = " + TextBox43.Text + ";"
            aryText(1) = "hostName = """ + TextBox48.Text + """;"
            aryText(2) = "password = """ + TextBox46.Text & """;"
            aryText(3) = "passwordAdmin = """ + TextBox47.Text & """;"
            If CheckBox8.Checked = True Then
                aryText(4) = "logFile  = """ + "logfile_consola.log" & """;"
            Else
            End If

            Try
                aryText(5) = "motd[] = {" + """" + TextBox42.Lines(0) + """" + ","
                aryText(6) = """" + TextBox42.Lines(1) + """" + ","
                aryText(7) = """" + TextBox42.Lines(2) + """" + ","
                aryText(8) = """" + TextBox42.Lines(3) + """" + ","
                aryText(9) = """" + TextBox42.Lines(4) + """" + ","
                aryText(10) = """" + TextBox42.Lines(5) + """" + ","
                aryText(11) = """" + TextBox42.Lines(6) + """" + ","
                aryText(12) = """" + TextBox42.Lines(7) + """" + ","
                aryText(13) = """" + TextBox42.Lines(8) + """" + ","
                aryText(14) = """" + TextBox42.Lines(9) + """" + ","
                aryText(15) = """" + TextBox42.Lines(10) + """" + ","
            Catch ex As Exception

            End Try

            aryText(16) = "};"
            aryText(17) = "motdInterval = " + NumericUpDown4.Text & ";"
            aryText(18) = ""
            aryText(19) = "maxPlayers =" & NumericUpDown3.Text & ";"
            Dim index = ComboBox4.SelectedIndex
            Select Case index
                Case 0
                    aryText(21) = "verifySignatures = 0;"
                Case 1
                    aryText(21) = "verifySignatures = 2;"
            End Select
            Dim index2 = ComboBox5.SelectedIndex
            Select Case index2
                Case 0
                    aryText(22) = "requiredSecureId = 0;"
                Case 1
                    aryText(22) = "requiredSecureId = 1;"
            End Select
            If CheckBox5.Checked = True Then
                aryText(23) = "kickduplicate = 1;"
            Else
                aryText(23) = "kickduplicate = 0;"
            End If
            aryText(24) = "voteMissionPlayers = " + NumericUpDown5.Text + ";"
            If CheckBox2.Checked = True Then
                aryText(25) = "disableVoN = 1;"
            Else
                aryText(25) = "disableVoN = 0;"
            End If
            If CheckBox15.Checked = True Then
                aryText(26) = "Headlessclients[]={" & TextBox62.Text & "};"
            Else
                aryText(26) = "Headlessclients[]={};"
            End If
            Dim porcentaje As String = Nothing
            Convert.ToDecimal(NumericUpDown6.Text)
            porcentaje = "0." + NumericUpDown6.Text

            aryText(27) = "voteThreshold = " + porcentaje & ";"
            aryText(28) = "vonCodecQuality = " + NumericUpDown7.Text & ";"
            If CheckBox4.Checked = True Then
                aryText(29) = "persistent = 1;"
            Else
                aryText(29) = "persistent = 0;"
            End If
            Dim index3 = ComboBox6.SelectedIndex
            Select Case index3
                Case 0
                    aryText(30) = "timeStampFormat = ""none"";"
                Case 1
                    aryText(30) = "timeStampFormat = ""short"";"
                Case 2
                    aryText(30) = "timeStampFormat = ""full"";"
            End Select
            If CheckBox6.Checked = True Then
                aryText(31) = "BattlEye = 1;"
            Else
                aryText(31) = "BattlEye = 0;"
            End If
            aryText(32) = "doubleIdDetected = " + TextBox39.Text + ";"
            aryText(33) = "onUserConnected = " + TextBox38.Text + ";"
            aryText(34) = "onUserDisconnected = " + TextBox37.Text + ";"
            aryText(35) = "onHackedData = " + TextBox36.Text + ";"
            aryText(36) = "onDifferentData = " + TextBox35.Text + ";"
            aryText(37) = "onUnsignedData = " + TextBox34.Text + ";"
            aryText(38) = "regularCheck = " + TextBox33.Text + ";"
            Dim index4 = ComboBox3.SelectedIndex
            Select Case index4
                Case 0
                    aryText(39) = "forceRotorLibSimulation = 0;"
                Case 1
                    aryText(39) = "forceRotorLibSimulation = 1;"
            End Select

            aryText(40) = "requiredBuild = " + TextBox21.Text + ";"
            aryText(41) = "class Missions"
            aryText(42) = "{"
            aryText(43) = "class Mission_1"
            aryText(44) = "{"


            Dim itemChecked As Object = Nothing
            For Each itemChecked In CheckedListBox3.CheckedItems
                If itemChecked.ToString.EndsWith(".pbo") Then
                    itemChecked = Replace(itemChecked, ".pbo", "")
                End If
            Next
            If itemChecked = "" Then
                aryText(45) = "template = """ + "&vacio&" & """;"
            Else

                aryText(45) = "template = """ + itemChecked.ToString & """;"
            End If
            For Each itemChecked In CheckedListBox2.CheckedItems
                If itemChecked.ToString.EndsWith(".pbo") Then
                    itemChecked = Replace(itemChecked, ".pbo", "")
                End If
            Next
            If itemChecked = "" Then
                aryText(45) = "template = """ + "&vacio&" & """;"
            Else

                aryText(45) = "template = """ + itemChecked.ToString & """;"
            End If


            Dim index5 = ComboBox7.SelectedIndex
            Select Case index5
                Case 0
                    aryText(46) = "difficulty = ""recruit"";"
                Case 1
                    aryText(46) = "difficulty = ""regular"";"
                Case 2
                    aryText(46) = "difficulty = ""veteran"";"
                Case 3
                    aryText(46) = "difficulty = ""mercenary"";"
            End Select
            aryText(47) = "};"
            aryText(48) = "};"
            Dim objWriter As New System.IO.StreamWriter(esusconfig)
            For i = 0 To 48
                objWriter.WriteLine(aryText(i))
            Next
            objWriter.Close()
        Else
            MsgBox("Ha ocurrido un error el archivo no ha podido ser guardado")
        End If
        Dim v As Integer
        Dim writterpuerto(1) As String
        writterpuerto(0) = TextBox45.Text
        writterpuerto(1) = CheckBox21.CheckState
        Dim objWriter7 As New System.IO.StreamWriter(puerto)
        For v = 0 To 1
            objWriter7.WriteLine(writterpuerto(v))
        Next
        objWriter7.Close()
        Dim sb As New System.Text.StringBuilder
        For Each itm As String In CheckedListBox1.CheckedItems
            sb.AppendLine(itm)
        Next
        If sb.ToString.Length >= 0 Then
            My.Computer.FileSystem.WriteAllText(mods, sb.ToString, False)
        End If
        Dim s As New IO.StreamWriter(prioridadmods)
        For i As Integer = 0 To ListBox1.Items.Count - 1
            s.Write(ListBox1.Items(i) & vbNewLine)
        Next
        s.Close()
        If File.Exists(configuracion) Then
            Dim arytext(18) As String
            arytext(0) = CheckBox1.CheckState
            If TextBox5.Text <> "" Then
                arytext(1) = TextBox5.Text
            Else
                arytext(1) = "nada"
            End If
            If TextBox16.Text <> "" Then
                arytext(2) = TextBox16.Text
            Else
                arytext(2) = "nada"
            End If
            If TextBox17.Text <> "" Then
                arytext(3) = TextBox17.Text
            Else
                arytext(3) = "nada"
            End If
            If TextBox2.Text <> "" Then
                arytext(4) = TextBox2.Text
            Else
                arytext(4) = "nada"
            End If
            arytext(5) = CheckBox9.CheckState
            arytext(6) = CheckBox7.CheckState
            arytext(8) = CheckBox10.CheckState
            If CheckBox10.Checked = True Then
                arytext(9) = TextBox3.Text
                arytext(10) = TextBox4.Text
                arytext(11) = TextBox44.Text
            Else
                arytext(9) = "rutaporregistroserver"
                arytext(10) = "rutaporregistroarma3"
                arytext(11) = "rutaporregistroaddons"
            End If
            arytext(12) = CheckBox11.CheckState
            If TextBox30.Text <> "" Then
                arytext(13) = TextBox30.Text

            Else
                arytext(13) = "nada"
            End If
            If TextBox60.Text <> "" Then
                arytext(14) = TextBox60.Text

            Else
                arytext(14) = "nada"
            End If
            If TextBox61.Text <> "" Then
                arytext(15) = TextBox61.Text

            Else
                arytext(15) = "nada"
            End If
            If TextBox63.Text <> "" Then
                arytext(16) = TextBox63.Text

            Else
                arytext(16) = "nada"
            End If
            If TextBox18.Text <> "" Then
                arytext(17) = TextBox18.Text

            Else
                arytext(17) = "nada"
            End If
            arytext(18) = CheckBox3.CheckState

            Dim objWriter As New System.IO.StreamWriter(configuracion)
            For a = 0 To 18
                objWriter.WriteLine(arytext(a))
            Next
            objWriter.Close()
        End If
        Label19.Visible = False
        Call construirparametro()
    End Sub


    Sub guardarconfig()
        If File.Exists(configuracion) Then
            Dim arytext(13) As String
            arytext(0) = CheckBox1.CheckState
            If TextBox5.Text <> "" Then
                arytext(1) = TextBox5.Text
            Else
                arytext(1) = "nada"
            End If
            If TextBox16.Text <> "" Then
                arytext(2) = TextBox16.Text
            Else
                arytext(2) = "nada"
            End If
            If TextBox17.Text <> "" Then
                arytext(3) = TextBox17.Text
            Else
                arytext(3) = "nada"
            End If
            If TextBox2.Text <> "" Then
                arytext(4) = TextBox2.Text
            Else
                arytext(4) = "nada"
            End If
            arytext(5) = CheckBox9.CheckState
            arytext(6) = CheckBox7.CheckState
            arytext(8) = CheckBox10.CheckState
            If CheckBox10.Checked = True Then
                arytext(9) = TextBox3.Text
                arytext(10) = TextBox4.Text
                arytext(11) = TextBox44.Text
            Else
                arytext(9) = "rutaporregistroserver"
                arytext(10) = "rutaporregistroarma3"
                arytext(11) = "rutaporregistroaddons"
            End If
            arytext(12) = CheckBox11.CheckState
            If TextBox30.Text <> "" Then
                arytext(13) = TextBox30.Text

            Else
                arytext(13) = "nada"
            End If
            Dim objWriter As New System.IO.StreamWriter(configuracion)
            For a = 0 To 13
                objWriter.WriteLine(arytext(a))
            Next
            objWriter.Close()
        End If
    End Sub
    Private Sub Label31_Click(sender As Object, e As EventArgs)

    End Sub

    Private Sub Label27_Click(sender As Object, e As EventArgs)

    End Sub

    Private Sub Label34_Click(sender As Object, e As EventArgs)

    End Sub

    Private Sub Label30_Click(sender As Object, e As EventArgs)

    End Sub

    Private Sub Label35_Click(sender As Object, e As EventArgs)

    End Sub

    Private Sub Label33_Click(sender As Object, e As EventArgs)

    End Sub

    Private Sub Label32_Click(sender As Object, e As EventArgs)

    End Sub

    Private Sub TabPage6_Click(sender As Object, e As EventArgs)

    End Sub

    Private Sub ComboBox1_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ComboBox1.SelectedIndexChanged
        Try
            VisordeLogs.Close()
            VisordeRpt.Close()
        Catch ex As Exception

        End Try

        If ComboBox1.SelectedItem <> "" Then
            TextBox20.Text = ComboBox1.SelectedItem
            perfilcambiante = esusservera3 & "\" & ComboBox1.SelectedItem.ToString
            carpetausercambiante = perfilcambiante & "\" & "Users"
            carpetausuariocambiante = carpetausercambiante & "\" & ComboBox1.SelectedItem.ToString
            puerto = Combine(perfilcambiante, txtpuerto)
            esusbasico = Combine(perfilcambiante, "Esus_basico.cfg")
            esusconfig = Combine(perfilcambiante, "Esus_config.cfg")
            dificultad = Combine(carpetausuariocambiante, ComboBox1.SelectedItem.ToString & ".Arma3Profile")
            mods = Combine(perfilcambiante, addon)
            prioridadmods = Combine(perfilcambiante, prioridad)
        Else
            TextBox20.Text = ComboBox1.SelectedItem
            perfilcambiante = esusservera3 & "\" & "PorDefecto"
            carpetausercambiante = perfilcambiante & "\" & "Users"
            carpetausuariocambiante = carpetausercambiante & "\" & "PorDefecto"
            puerto = Combine(perfilcambiante, txtpuerto)
            esusbasico = Combine(perfilcambiante, "Esus_basico.cfg")
            esusconfig = Combine(perfilcambiante, "Esus_config.cfg")
            dificultad = Combine(carpetausuariocambiante, "PorDefecto" & ".Arma3Profile")
            mods = Combine(perfilcambiante, addon)
            prioridadmods = Combine(perfilcambiante, prioridad)
        End If
        Try
            Dim ceropuerto As String = System.IO.File.ReadAllLines(puerto)(0)
            TextBox45.Text = ceropuerto
        Catch ex As Exception
            TextBox45.Text = Nothing
        End Try
        Try
            Dim unopuerto As String = System.IO.File.ReadAllLines(puerto)(1)
            If unopuerto.Contains("0") Then
                CheckBox21.Checked = False
            Else
                CheckBox21.Checked = True
            End If
        Catch
            CheckBox21.Checked = False
        End Try

        CheckedListBox1.Items.Clear()
        CheckedListBox3.Items.Clear()
        CheckedListBox2.Items.Clear()
        For Each folder In System.IO.Directory.GetDirectories(rutaaddons, "@*")
            CheckedListBox1.Items.Add(Path.GetFileName(folder))
        Next
        For Each File In System.IO.Directory.GetFiles(mpmision, "*.pbo")
            Dim primeraparte As String = Path.GetFileName(File).Substring(0, Path.GetFileName(File).LastIndexOf("."))
            CheckedListBox2.Items.Add(Path.GetFileName(primeraparte))
        Next
        For Each folder In System.IO.Directory.GetDirectories(mpmision)
            CheckedListBox2.Items.Add(Path.GetFileName(folder))
        Next
        TextBox68.Text = CheckedListBox1.Items.Count
        TextBox65.Text = CheckedListBox2.Items.Count
        TextBox67.Text = CheckedListBox1.CheckedItems.Count
        TextBox66.Text = CheckedListBox2.CheckedItems.Count
        ComboBox4.SelectedIndex = 0
        ComboBox5.SelectedIndex = 0
        ComboBox6.SelectedIndex = 0
        ComboBox3.SelectedIndex = 0
        ComboBox7.SelectedIndex = 0
        If CheckBox2.Checked = True Then
            Label22.Visible = False
            NumericUpDown7.Visible = False
        Else
            Label22.Visible = True
            NumericUpDown7.Visible = True
        End If


        Call leermods()
        Call leerdificultad()
        Call leerperfilbasico()
        Call leerperfilconfig()
        TextBox43.Text = System.Text.RegularExpressions.Regex.Replace(TextBox43.Text, "\s{2,}", " ")
        TextBox43.Text = TextBox43.Text.Replace(" ", "")
        TextBox39.Text = TextBox39.Text.Replace(" ", "")
        TextBox38.Text = TextBox38.Text.Replace(" ", "")
        TextBox37.Text = TextBox37.Text.Replace(" ", "")
        TextBox36.Text = TextBox36.Text.Replace(" ", "")
        TextBox35.Text = TextBox35.Text.Replace(" ", "")
        TextBox34.Text = TextBox34.Text.Replace(" ", "")
        TextBox33.Text = TextBox33.Text.Replace(" ", "")
        TextBox21.Text = TextBox21.Text.Replace(" ", "")
        TextBox7.Text = TextBox7.Text.Replace(" ", "")
        TextBox8.Text = TextBox8.Text.Replace(" ", "")
        TextBox9.Text = TextBox9.Text.Replace(" ", "")
        TextBox10.Text = TextBox10.Text.Replace(" ", "")
        TextBox11.Text = TextBox11.Text.Replace(" ", "")
        TextBox12.Text = TextBox12.Text.Replace(" ", "")
        TextBox13.Text = TextBox13.Text.Replace(" ", "")
        TextBox14.Text = TextBox14.Text.Replace(" ", "")
        TextBox15.Text = TextBox15.Text.Replace(" ", "")
        Call construirparametro()
        Call hc()
        Call seleccionardificultad()
        CheckedListBox2.Visible = True
        ComboBox2.SelectedItem = Nothing
        Dim itemChecked As Object = Nothing
        Dim elegido As String = Nothing
        For Each itemChecked In CheckedListBox3.CheckedItems
            elegido = itemChecked
        Next
        For a = 0 To CheckedListBox2.Items.Count - 1
            Try
                If CheckedListBox2.Items(a).indexof(elegido) <> -1 Then
                    CheckedListBox2.SetItemChecked(a, True)
                End If
            Catch ex As Exception

            End Try

        Next
        CheckedListBox3.Visible = False
    End Sub

    Private Sub Button19_Click(sender As Object, e As EventArgs) Handles Button19.Click
        VisordeRpt.Show()
    End Sub

    Private Sub TabPage1_Click(sender As Object, e As EventArgs) Handles TabPage1.Click

    End Sub

    Private Sub Label12_Click(sender As Object, e As EventArgs)

    End Sub



    Private Sub Button29_Click(sender As Object, e As EventArgs)

    End Sub

    Private Sub Button29_Click_1(sender As Object, e As EventArgs)


    End Sub

    Private Sub Button30_Click(sender As Object, e As EventArgs) Handles Button30.Click

    End Sub

    Private Sub GroupBox12_Enter(sender As Object, e As EventArgs) Handles GroupBox12.Enter

    End Sub

    Private Sub CheckBox2_CheckedChanged(sender As Object, e As EventArgs) Handles CheckBox2.CheckedChanged
        If CheckBox2.Checked = True Then
            Label22.Visible = False
            NumericUpDown7.Visible = False
        Else
            Label22.Visible = True
            NumericUpDown7.Visible = True
        End If
    End Sub

    Private Sub Button27_Click(sender As Object, e As EventArgs) Handles Button27.Click
        Process.Start(rutacarpetaa3)
    End Sub

    Private Sub Button23_Click(sender As Object, e As EventArgs) Handles Button23.Click
        Process.Start(esusservera3)

    End Sub

    Private Sub TabPage8_Click(sender As Object, e As EventArgs) Handles TabPage8.Click

    End Sub




    Private Sub TabPage3_Click(sender As Object, e As EventArgs) Handles TabPage3.Click

    End Sub



    Private Sub Button17_Click(sender As Object, e As EventArgs) Handles Button17.Click
        TextBox7.Text = "7200"
        TextBox8.Text = "24"
        TextBox9.Text = "1024"
        TextBox10.Text = "1024"
        TextBox11.Text = "256"
        TextBox12.Text = "0.003"
        TextBox13.Text = "0.0099999998"
        TextBox14.Text = "160"
        TextBox15.Text = "1400"
    End Sub

    Private Sub TabPage2_Click(sender As Object, e As EventArgs) Handles TabPage2.Click

    End Sub

    Private Sub Button1_Click_1(sender As Object, e As EventArgs) Handles Button1.Click
        Try
            Clipboard.SetText(TextBox1.Text)
            MessageBox.Show("Los parámetros se han copiado correctamente a su portapapeles.")
        Catch ex As Exception

        End Try

    End Sub

    Private Sub Button28_Click(sender As Object, e As EventArgs) Handles Button28.Click
        Call leerperfilbasico()
    End Sub


    Private Sub ComboBox7_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ComboBox7.SelectedIndexChanged
        Call seleccionardificultad()
    End Sub
    Sub seleccionardificultad()
        Dim treintayuno As String = Nothing
        Dim treintaydos As String = Nothing
        Dim treintaytres As String = Nothing
        Dim treintaycuatro As String = Nothing
        Dim sesentaycinco As String = Nothing
        Dim sesentayseis As String = Nothing
        Dim sesentaysiete As String = Nothing
        Dim sesentayocho As String = Nothing
        Dim noventaynueve As String = Nothing
        Dim cien As String = Nothing
        Dim cientouno As String = Nothing
        Dim cientodos As String = Nothing

        Dim cientrotreintaynueve As String = Nothing
        Dim index5 = ComboBox7.SelectedIndex
        Select Case index5
            Case 0

                treintayuno = System.IO.File.ReadAllLines(dificultad)(31)
                If treintayuno.Contains("aiLevelPreset=") Then
                    treintayuno = Replace(treintayuno, "aiLevelPreset=", "")
                    treintayuno = Replace(treintayuno, ";", "")
                    treintayuno = System.Text.RegularExpressions.Regex.Replace(treintayuno, "\s{2,}", " ")
                    ComboBox9.SelectedIndex = treintayuno
                Else
                    Dim cientotreintaycinco2 As String
                    cientotreintaycinco2 = System.IO.File.ReadAllLines(dificultad)(29)
                    cientotreintaycinco2 = Replace(cientotreintaycinco2, "aiLevelPreset=", "")
                    cientotreintaycinco2 = Replace(cientotreintaycinco2, ";", "")
                    cientotreintaycinco2 = System.Text.RegularExpressions.Regex.Replace(cientotreintaycinco2, "\s{2,}", " ")
                    ComboBox9.SelectedIndex = cientotreintaycinco2
                End If
                treintaydos = System.IO.File.ReadAllLines(dificultad)(32)
                treintaytres = System.IO.File.ReadAllLines(dificultad)(33)
                treintaycuatro = System.IO.File.ReadAllLines(dificultad)(34)
                treintayuno = Replace(treintayuno, "aiLevelPreset=", "")
                treintayuno = Replace(treintayuno, ";", "")
                treintaydos = Replace(treintaydos, "skillAI=", "")
                treintaydos = Replace(treintaydos, ";", "")
                treintaytres = Replace(treintaytres, "precisionAI=", "")
                treintaytres = Replace(treintaytres, ";", "")
                treintayuno = System.Text.RegularExpressions.Regex.Replace(treintayuno, "\s{2,}", " ")
                treintaydos = System.Text.RegularExpressions.Regex.Replace(treintaydos, "\s{2,}", " ")
                treintaytres = System.Text.RegularExpressions.Regex.Replace(treintaytres, "\s{2,}", " ")
                treintaycuatro = System.Text.RegularExpressions.Regex.Replace(treintaycuatro, "\s{2,}", " ")
                ComboBox8.Visible = True
                TextBox22.Visible = True
                TextBox26.Visible = True
                TextBox22.Text = treintaydos
                TextBox26.Text = treintaytres
                ComboBox9.Visible = False
                ComboBox10.Visible = False
                ComboBox11.Visible = False
                TextBox23.Visible = False
                TextBox24.Visible = False
                TextBox25.Visible = False
                TextBox27.Visible = False
                TextBox28.Visible = False
                TextBox29.Visible = False
                If ComboBox8.SelectedIndex = 3 Then
                    TextBox22.Enabled = True
                    TextBox23.Enabled = True
                    TextBox24.Enabled = True
                    TextBox25.Enabled = True
                    TextBox26.Enabled = True
                    TextBox27.Enabled = True
                    TextBox28.Enabled = True
                    TextBox29.Enabled = True
                Else
                    TextBox22.Enabled = False
                    TextBox23.Enabled = False
                    TextBox24.Enabled = False
                    TextBox25.Enabled = False
                    TextBox26.Enabled = False
                    TextBox27.Enabled = False
                    TextBox28.Enabled = False
                    TextBox29.Enabled = False
                End If
                CheckedListBox4.Visible = True
                CheckedListBox5.Visible = False
                CheckedListBox6.Visible = False
                CheckedListBox7.Visible = False
            Case 1


                sesentaycinco = System.IO.File.ReadAllLines(dificultad)(65)
                If sesentaycinco.Contains("aiLevelPreset=") Then
                    sesentaycinco = Replace(sesentaycinco, "aiLevelPreset=", "")
                    sesentaycinco = Replace(sesentaycinco, ";", "")
                    sesentaycinco = System.Text.RegularExpressions.Regex.Replace(sesentaycinco, "\s{2,}", " ")
                    ComboBox9.SelectedIndex = sesentaycinco
                Else
                    Dim cientotreintaycinco2 As String
                    cientotreintaycinco2 = System.IO.File.ReadAllLines(dificultad)(64)
                    cientotreintaycinco2 = Replace(cientotreintaycinco2, "aiLevelPreset=", "")
                    cientotreintaycinco2 = Replace(cientotreintaycinco2, ";", "")
                    cientotreintaycinco2 = System.Text.RegularExpressions.Regex.Replace(cientotreintaycinco2, "\s{2,}", " ")
                    ComboBox9.SelectedIndex = cientotreintaycinco2
                End If
                sesentayseis = System.IO.File.ReadAllLines(dificultad)(66)
                sesentaysiete = System.IO.File.ReadAllLines(dificultad)(67)
                sesentayocho = System.IO.File.ReadAllLines(dificultad)(68)
                sesentaycinco = Replace(sesentaycinco, "aiLevelPreset=", "")
                sesentaycinco = Replace(sesentaycinco, ";", "")
                sesentayseis = Replace(sesentayseis, "skillAI=", "")
                sesentayseis = Replace(sesentayseis, ";", "")
                sesentaysiete = Replace(sesentaysiete, "precisionAI=", "")
                sesentaysiete = Replace(sesentaysiete, ";", "")
                sesentaycinco = System.Text.RegularExpressions.Regex.Replace(sesentaycinco, "\s{2,}", " ")
                sesentayseis = System.Text.RegularExpressions.Regex.Replace(sesentayseis, "\s{2,}", " ")
                sesentaysiete = System.Text.RegularExpressions.Regex.Replace(sesentaysiete, "\s{2,}", " ")
                sesentayocho = System.Text.RegularExpressions.Regex.Replace(sesentayocho, "\s{2,}", " ")

                ComboBox8.Visible = False
                ComboBox9.Visible = True
                ComboBox10.Visible = False
                ComboBox11.Visible = False
                TextBox22.Visible = False
                TextBox23.Visible = True
                TextBox23.Text = sesentayseis
                TextBox24.Visible = False
                TextBox25.Visible = False
                TextBox26.Visible = False
                TextBox27.Visible = True
                TextBox27.Text = sesentaysiete
                TextBox28.Visible = False
                TextBox29.Visible = False
                If ComboBox9.SelectedIndex = 3 Then
                    TextBox22.Enabled = True
                    TextBox23.Enabled = True
                    TextBox24.Enabled = True
                    TextBox25.Enabled = True
                    TextBox26.Enabled = True
                    TextBox27.Enabled = True
                    TextBox28.Enabled = True
                    TextBox29.Enabled = True
                Else
                    TextBox22.Enabled = False
                    TextBox23.Enabled = False
                    TextBox24.Enabled = False
                    TextBox25.Enabled = False
                    TextBox26.Enabled = False
                    TextBox27.Enabled = False
                    TextBox28.Enabled = False
                    TextBox29.Enabled = False
                End If

                CheckedListBox4.Visible = False
                CheckedListBox5.Visible = True
                CheckedListBox6.Visible = False
                CheckedListBox7.Visible = False


            Case 2

                noventaynueve = System.IO.File.ReadAllLines(dificultad)(99)
                If noventaynueve.Contains("aiLevelPreset=") Then
                    noventaynueve = Replace(noventaynueve, "aiLevelPreset=", "")
                    noventaynueve = Replace(noventaynueve, ";", "")
                    noventaynueve = System.Text.RegularExpressions.Regex.Replace(noventaynueve, "\s{2,}", " ")
                    ComboBox10.SelectedIndex = noventaynueve


                Else
                    Dim cientotreintaycinco2 As String
                    cientotreintaycinco2 = System.IO.File.ReadAllLines(dificultad)(97)
                    cientotreintaycinco2 = Replace(cientotreintaycinco2, "aiLevelPreset=", "")
                    cientotreintaycinco2 = Replace(cientotreintaycinco2, ";", "")
                    cientotreintaycinco2 = System.Text.RegularExpressions.Regex.Replace(cientotreintaycinco2, "\s{2,}", " ")
                    ComboBox10.SelectedIndex = cientotreintaycinco2
                End If
                cien = System.IO.File.ReadAllLines(dificultad)(100)
                cientouno = System.IO.File.ReadAllLines(dificultad)(101)
                cientodos = System.IO.File.ReadAllLines(dificultad)(102)
                noventaynueve = Replace(noventaynueve, "aiLevelPreset=", "")
                noventaynueve = Replace(noventaynueve, ";", "")
                cien = Replace(cien, "skillAI=", "")
                cien = Replace(cien, ";", "")
                cientouno = Replace(cientouno, "precisionAI=", "")
                cientouno = Replace(cientouno, ";", "")
                noventaynueve = System.Text.RegularExpressions.Regex.Replace(noventaynueve, "\s{2,}", " ")
                cien = System.Text.RegularExpressions.Regex.Replace(cien, "\s{2,}", " ")
                cientouno = System.Text.RegularExpressions.Regex.Replace(cientouno, "\s{2,}", " ")
                cientodos = System.Text.RegularExpressions.Regex.Replace(cientodos, "\s{2,}", " ")
                ComboBox8.Visible = False
                ComboBox9.Visible = False
                ComboBox10.Visible = True
                ComboBox11.Visible = False


                If ComboBox10.SelectedIndex = 3 Then
                    TextBox22.Enabled = True
                    TextBox23.Enabled = True
                    TextBox24.Enabled = True
                    TextBox25.Enabled = True
                    TextBox26.Enabled = True
                    TextBox27.Enabled = True
                    TextBox28.Enabled = True
                    TextBox29.Enabled = True
                Else
                    TextBox22.Enabled = False
                    TextBox23.Enabled = False
                    TextBox24.Enabled = False
                    TextBox25.Enabled = False
                    TextBox26.Enabled = False
                    TextBox27.Enabled = False
                    TextBox28.Enabled = False
                    TextBox29.Enabled = False
                End If
                TextBox22.Visible = False
                TextBox23.Visible = False
                TextBox24.Visible = True
                TextBox24.Text = cien
                TextBox25.Visible = False

                TextBox26.Visible = False
                TextBox27.Visible = False
                TextBox28.Visible = True
                TextBox28.Text = cientouno
                TextBox29.Visible = False

                CheckedListBox4.Visible = False
                CheckedListBox5.Visible = False
                CheckedListBox6.Visible = True
                CheckedListBox7.Visible = False
                CheckedListBox6.Items.Remove("armor")
                CheckedListBox6.Items.Remove("autoAim")
                CheckedListBox6.Items.Remove("autoGuideAT")
                CheckedListBox6.Items.Remove("autoSpot")
                CheckedListBox6.Items.Remove("enemyTag")
                CheckedListBox6.Items.Remove("friendlyTag")
                CheckedListBox6.Items.Remove("hudPerm")
                CheckedListBox6.Items.Remove("hudWpPerm")
                CheckedListBox6.Items.Remove("tracers")


            Case 3

                Dim cientotreintaytres As String
                Dim cientotreintaycuatro As String
                Dim cientotreintaycinco As String
                Dim cientotreintayseis As String
                cientotreintaytres = System.IO.File.ReadAllLines(dificultad)(133)
                If cientotreintaytres.Contains("aiLevelPreset=") Then
                    cientotreintaytres = Replace(cientotreintaytres, "aiLevelPreset=", "")
                    cientotreintaytres = Replace(cientotreintaytres, ";", "")
                    cientotreintaytres = System.Text.RegularExpressions.Regex.Replace(cientotreintaytres, "\s{2,}", " ")
                    ComboBox11.SelectedIndex = cientotreintaytres
                Else
                    Dim cientotreintaycinco2 As String
                    cientotreintaycinco2 = System.IO.File.ReadAllLines(dificultad)(130)
                    cientotreintaycinco2 = Replace(cientotreintaycinco2, "aiLevelPreset=", "")
                    cientotreintaycinco2 = Replace(cientotreintaycinco2, ";", "")
                    cientotreintaycinco2 = System.Text.RegularExpressions.Regex.Replace(cientotreintaycinco2, "\s{2,}", " ")
                    ComboBox11.SelectedIndex = cientotreintaycinco2
                End If
                cientotreintaycuatro = System.IO.File.ReadAllLines(dificultad)(134)
                cientotreintaycinco = System.IO.File.ReadAllLines(dificultad)(135)
                cientotreintayseis = System.IO.File.ReadAllLines(dificultad)(136)
                cientotreintaytres = Replace(cientotreintaytres, "aiLevelPreset=", "")
                cientotreintaytres = Replace(cientotreintaytres, ";", "")
                cientotreintaytres = System.Text.RegularExpressions.Regex.Replace(cientotreintaytres, "\s{2,}", " ")
                cientotreintaycuatro = Replace(cientotreintaycuatro, "skillAI=", "")
                cientotreintaycuatro = Replace(cientotreintaycuatro, ";", "")
                cientotreintaycinco = Replace(cientotreintaycinco, "precisionAI=", "")
                cientotreintaycinco = Replace(cientotreintaycinco, ";", "")
                cientotreintaycuatro = System.Text.RegularExpressions.Regex.Replace(cientotreintaycuatro, "\s{2,}", " ")
                cientotreintaycinco = System.Text.RegularExpressions.Regex.Replace(cientotreintaycinco, "\s{2,}", " ")
                cientotreintayseis = System.Text.RegularExpressions.Regex.Replace(cientotreintayseis, "\s{2,}", " ")
                ComboBox8.Visible = False
                ComboBox9.Visible = False
                ComboBox10.Visible = False
                ComboBox11.Visible = True
                TextBox22.Visible = False
                TextBox23.Visible = False
                TextBox24.Visible = False
                TextBox25.Visible = True
                TextBox26.Visible = False
                TextBox27.Visible = False
                TextBox28.Visible = False
                TextBox29.Visible = True



                CheckedListBox4.Visible = False
                CheckedListBox5.Visible = False
                CheckedListBox6.Visible = False
                CheckedListBox7.Visible = True
                CheckedListBox7.Items.Remove("armor")
                CheckedListBox7.Items.Remove("autoAim")
                CheckedListBox7.Items.Remove("autoGuideAT")
                CheckedListBox7.Items.Remove("autoSpot")
                CheckedListBox7.Items.Remove("enemyTag")
                CheckedListBox7.Items.Remove("friendlyTag")
                CheckedListBox7.Items.Remove("hudPerm")
                CheckedListBox7.Items.Remove("hudWpPerm")
                CheckedListBox7.Items.Remove("tracers")
                CheckedListBox7.Items.Remove("3RdPersonView")
                CheckedListBox7.Items.Remove("cameraShake")
                CheckedListBox7.Items.Remove("map")
                CheckedListBox7.Items.Remove("unlimitedSaves")
                CheckedListBox7.Items.Remove("weaponCursor")
                CheckedListBox7.Items.Remove("clockIndicator")
                CheckedListBox7.Items.Remove("hudGroupInfo")
                CheckedListBox7.Items.Remove("hudWp")
                CheckedListBox7.Items.Remove("hud")
                CheckedListBox7.Items.Remove("tracers")
                If ComboBox11.SelectedIndex = 3 Then
                    TextBox22.Enabled = True
                    TextBox23.Enabled = True
                    TextBox24.Enabled = True
                    TextBox25.Enabled = True
                    TextBox26.Enabled = True
                    TextBox27.Enabled = True
                    TextBox28.Enabled = True
                    TextBox29.Enabled = True
                Else
                    TextBox22.Enabled = False
                    TextBox23.Enabled = False
                    TextBox24.Enabled = False
                    TextBox25.Enabled = False
                    TextBox26.Enabled = False
                    TextBox27.Enabled = False
                    TextBox28.Enabled = False
                    TextBox29.Enabled = False
                End If

        End Select
        Call inicioperfil()
    End Sub
    Private Sub Button4_Click(sender As Object, e As EventArgs) Handles Button4.Click
        Dim texto As String
        If TextBox6.Text = Nothing Then
            MsgBox("Escribe un nombre para el perfil")
        Else
            texto = TextBox6.Text
            perfilcambiante = esusservera3 & "\" & texto
            carpetausercambiante = perfilcambiante & "\" & "Users"
            carpetausuariocambiante = carpetausercambiante & "\" & texto
            puerto = Combine(perfilcambiante, txtpuerto)
            esusbasico = Combine(perfilcambiante, "Esus_basico.cfg")
            esusconfig = Combine(perfilcambiante, "Esus_config.cfg")
            dificultad = Combine(carpetausuariocambiante, texto & ".Arma3Profile")
            mods = Combine(perfilcambiante, addon)
            prioridadmods = Combine(perfilcambiante, prioridad)
            System.IO.Directory.CreateDirectory(perfilcambiante)
            System.IO.Directory.CreateDirectory(carpetausercambiante)
            System.IO.Directory.CreateDirectory(carpetausuariocambiante)
            crearmod = File.Create(mods)
            crearmod.Close()
            crearmod.Dispose()
            crearprioridad = File.Create(prioridadmods)
            crearprioridad.Close()
            crearprioridad.Dispose()
            crearperfil = File.Create(esusbasico)
            crearperfil.Close()
            crearperfil.Dispose()
            puertost = System.IO.File.Create(puerto)
            puertost.Close()
            puertost.Dispose()
            creadordificultad = File.Create(dificultad)
            creadordificultad.Close()
            crearperfil2 = File.Create(esusconfig)
            crearperfil2.Close()
            crearperfil2.Dispose()
            ComboBox1.Items.Clear()
            Call creardificultad()
            For Each perfiles As String In My.Computer.FileSystem.GetDirectories _
      (esusservera3)
                ComboBox1.Items.Add(System.IO.Path.GetFileNameWithoutExtension(perfiles))
            Next
            TextBox6.Text = Nothing
        End If
        ComboBox1.SelectedItem = "PorDefecto"
    End Sub

    Private Sub TabPage7_Click(sender As Object, e As EventArgs)

    End Sub

    Private Sub Button31_Click(sender As Object, e As EventArgs) Handles Button31.Click
        Dim borrado As String = Nothing
        If TextBox20.Text = Nothing Then
        ElseIf ComboBox1.SelectedItem = "PorDefecto" Then
            MsgBox("Este es el perfil por defecto no puedes borrarlo!!")
        Else
            borrado = esusservera3 & "\" & TextBox20.Text
            Try
                My.Computer.FileSystem.DeleteDirectory(borrado, FileIO.UIOption.AllDialogs, FileIO.RecycleOption.SendToRecycleBin)
                ComboBox1.Items.Clear()
                For Each perfiles As String In My.Computer.FileSystem.GetDirectories _
     (esusservera3)
                    ComboBox1.Items.Add(System.IO.Path.GetFileNameWithoutExtension(perfiles))
                Next
                TextBox20.Text = Nothing
            Catch ex As Exception
            End Try
        End If
        ComboBox1.SelectedItem = "PorDefecto"
    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        Dim renombrarperfil As String = Nothing
        Dim renombrarusers As String = Nothing
        Dim renombrardificultad As String = Nothing

        If TextBox20.Text = Nothing Then
        ElseIf ComboBox1.SelectedItem = "PorDefecto" Then
            MsgBox("Este es el perfil por defecto no puedes cambiarle el nombre!!")
        Else
            renombrarperfil = esusservera3 & "\" & ComboBox1.SelectedItem.ToString
            renombrarusers = esusservera3 & "\" & ComboBox1.SelectedItem.ToString & "\Users" & "\" & ComboBox1.SelectedItem.ToString
            renombrardificultad = esusservera3 & "\" & ComboBox1.SelectedItem.ToString & "\Users" & "\" & ComboBox1.SelectedItem.ToString & "\" & ComboBox1.SelectedItem.ToString & ".Arma3Profile"
            Try

                My.Computer.FileSystem.RenameFile(renombrardificultad,
TextBox20.Text & ".Arma3Profile")
                My.Computer.FileSystem.RenameDirectory(renombrarusers,
    TextBox20.Text)
                My.Computer.FileSystem.RenameDirectory(renombrarperfil,
    TextBox20.Text)
            Catch ex As Exception
            End Try


            ComboBox1.Items.Clear()
            For Each perfiles As String In My.Computer.FileSystem.GetDirectories _
 (esusservera3)
                ComboBox1.Items.Add(System.IO.Path.GetFileNameWithoutExtension(perfiles))
            Next
            TextBox20.Text = Nothing
        End If
        ComboBox1.SelectedItem = "PorDefecto"
    End Sub

    Private Sub Button24_Click(sender As Object, e As EventArgs) Handles Button24.Click
        Dim ORIGEN As String = esusservera3
        Dim CARPETA As String = ORIGEN.Remove(0, ORIGEN.LastIndexOf("\"))
        If FolderBrowserDialog1.ShowDialog = Windows.Forms.DialogResult.OK Then
            Dim DESTINO As String = FolderBrowserDialog1.SelectedPath & CARPETA
            If My.Computer.FileSystem.DirectoryExists(DESTINO) = False Then
                My.Computer.FileSystem.CreateDirectory(DESTINO)
            Else
                MessageBox.Show("Ya has exportado tu carpeta de perfiles al escritorio, por favor borrala para hacer otra copia.")
            End If
            Try
                My.Computer.FileSystem.CopyDirectory(ORIGEN, DESTINO)
            Catch ex As Exception
            End Try
        End If
    End Sub

    Private Sub Button18_Click(sender As Object, e As EventArgs) Handles Button18.Click
        VisordeLogs.Show()
    End Sub

    Private Sub TextBox45_TextChanged(sender As Object, e As EventArgs) Handles TextBox45.TextChanged
    End Sub

    Private Sub CheckBox15_CheckedChanged(sender As Object, e As EventArgs) Handles CheckBox15.CheckedChanged
        If CheckBox15.Checked = True Then
            TextBox60.Visible = True
            TextBox62.Visible = True
            TextBox63.Visible = True
            TextBox18.Visible = True
            TextBox61.Visible = True
            Label64.Visible = True
            Label65.Visible = True
            Label66.Visible = True
            Label67.Visible = True
            Label84.Visible = True
        Else
            TextBox60.Visible = False
            TextBox62.Visible = False
            TextBox63.Visible = False
            TextBox18.Visible = False
            TextBox61.Visible = False
            Label64.Visible = False
            Label65.Visible = False
            Label66.Visible = False
            Label67.Visible = False
            Label84.Visible = False
        End If
    End Sub

    Private Sub TabPage5_Click(sender As Object, e As EventArgs) Handles TabPage5.Click

    End Sub

    Private Sub Button22_Click(sender As Object, e As EventArgs) Handles Button22.Click
        Call hc()
        If CheckBox7.Checked = True Then
            Dim process As New Process()
            process.StartInfo.FileName = exearma3
            process.StartInfo.Arguments = TextBox19.Text
            process.StartInfo.Verb = "runas"
            process.StartInfo.UseShellExecute = True
            process.Start()
        Else
            Process.Start(exearma3, TextBox19.Text)
        End If
    End Sub

    Private Sub CheckBox3_CheckedChanged(sender As Object, e As EventArgs) Handles CheckBox3.CheckedChanged
        If CheckBox3.Checked = True Then
            TextBox60.Enabled = False
            TextBox60.Text = Nothing
            TextBox62.Enabled = False
            TextBox62.Text = "127.0.0.1"
            TextBox63.Enabled = False
            TextBox61.Text = Nothing
            TextBox18.Text = Nothing
            TextBox63.Text = Nothing
            TextBox18.Enabled = False
            TextBox61.Enabled = False
            Call hc()
        Else
            TextBox60.Enabled = True
            TextBox62.Enabled = True
            TextBox62.Text = Nothing
            TextBox63.Enabled = True
            TextBox18.Enabled = True
            TextBox61.Enabled = True
        End If
    End Sub

    Private Sub TextBox42_TextChanged(sender As Object, e As EventArgs) Handles TextBox42.TextChanged
        Dim A$, I&
        A = TextBox42.Text
        Do
            I = Len(A)
            A = Replace(A, vbCrLf & vbCrLf, vbCrLf)
        Loop While Len(A) <> I
        TextBox42.Text = A
    End Sub
    Private Sub TextBox42_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox42.KeyPress
        If (TextBox42.Lines.Length + 1 > 10) And AscW(e.KeyChar) = Keys.Enter Then
            e.KeyChar = String.Empty
            MsgBox("Has llegado al limite máximo que puedes escribir!")
        End If
    End Sub

    Private Sub Button20_Click(sender As Object, e As EventArgs) Handles Button20.Click
        Call hc()
        Try
            Clipboard.SetText(TextBox19.Text)
        Catch ex As Exception
            MessageBox.Show("No hay nada para copiar")
        End Try
    End Sub

    Private Sub GroupBox13_Enter(sender As Object, e As EventArgs) Handles GroupBox13.Enter

    End Sub

    Private Sub Button21_Click(sender As Object, e As EventArgs) Handles Button21.Click
        CheckedListBox3.Visible = False
        CheckedListBox2.Items.Clear()
        CheckedListBox2.Visible = True
        For Each File In System.IO.Directory.GetFiles(mpmision, "*.pbo")
            Dim primeraparte As String = Path.GetFileName(File).Substring(0, Path.GetFileName(File).LastIndexOf("."))
            CheckedListBox2.Items.Add(Path.GetFileName(primeraparte))
        Next
        For Each folder In System.IO.Directory.GetDirectories(mpmision)
            CheckedListBox2.Items.Add(Path.GetFileName(folder))
        Next
        Call leerperfilconfig()
        ListBox4.Items.Clear()
        For Each File In System.IO.Directory.GetFiles(mpmision, "*.pbo")
            Dim entre As Integer = Path.GetFileName(File).IndexOf(".")
            Dim mapas As String = Path.GetFileName(File).Substring(entre + 1)
            Dim upper1 As String = mapas.ToUpper()
            ListBox4.Items.Add(Path.GetFileNameWithoutExtension(upper1))
            ListBox4.Items.Remove("PBO")
        Next
        For Each folder In System.IO.Directory.GetDirectories(mpmision)
            Dim entre As Integer = Path.GetFileName(folder).IndexOf(".")
            Dim mapas As String = Path.GetFileName(folder).Substring(entre + 1)
            Dim upper1 As String = mapas.ToUpper()
            ListBox4.Items.Add(Path.GetFileNameWithoutExtension(upper1))
        Next
        Call quitarmapasduplicados()
    End Sub


    Private Sub Button26_Click(sender As Object, e As EventArgs)

    End Sub

    Private Sub Button29_Click_2(sender As Object, e As EventArgs) Handles Button29.Click
        CargarMods.Show()
    End Sub


    Private Sub CopiarToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles CopiarToolStripMenuItem.Click
        Dim builder As New StringBuilder()
        For Each i As Object In ListBox1.Items
            i = i + vbNewLine
            builder.Append(i.ToString())
        Next
        TextBox41.Text = builder.ToString()
        Try
            Clipboard.SetText(TextBox41.Text)
        Catch ex As Exception
            MessageBox.Show("No hay nada para copiar")
        End Try
    End Sub

    Private Sub Button25_Click(sender As Object, e As EventArgs) Handles Button25.Click
        ComboBox1.Items.Clear()
        For Each perfiles As String In My.Computer.FileSystem.GetDirectories _
(esusservera3)
            ComboBox1.Items.Add(System.IO.Path.GetFileNameWithoutExtension(perfiles))
        Next
        TextBox20.Text = Nothing
        ComboBox1.SelectedItem = "PorDefecto"
    End Sub




    Private Sub Button37_Click(sender As Object, e As EventArgs) Handles Button37.Click
        Changelog.Show()
    End Sub

    Private Sub Button38_Click(sender As Object, e As EventArgs) Handles Button38.Click
        Dim perfil As String = Nothing
        perfil = esusservera3 & "\" & ComboBox1.SelectedItem.ToString
        Dim contador As Integer = 0
        Try
            For Each File In System.IO.Directory.GetFiles(perfil, "*.rpt")
                My.Computer.FileSystem.DeleteFile(File, FileIO.UIOption.OnlyErrorDialogs, FileIO.RecycleOption.DeletePermanently)
                contador += 1
            Next
        Catch ex As Exception

        End Try
        MsgBox("Se han eliminado " + contador.ToString + " archivos residuales de RPT.")

    End Sub

    Private Sub Button39_Click(sender As Object, e As EventArgs) Handles Button39.Click
        Dim perfil As String = Nothing
        perfil = esusservera3 & "\" & ComboBox1.SelectedItem.ToString
        Dim contador As Integer = 0
        Try
            For Each File In System.IO.Directory.GetFiles(perfil, "*.log")
                My.Computer.FileSystem.DeleteFile(File, FileIO.UIOption.OnlyErrorDialogs, FileIO.RecycleOption.DeletePermanently)
                contador += 1
            Next
        Catch ex As Exception

        End Try
        MsgBox("Se han eliminado " + contador.ToString + " archivos residuales de Consola.")

    End Sub

    Private Sub Button40_Click(sender As Object, e As EventArgs) Handles Button40.Click
        Dim perfil As String = Nothing
        perfil = esusservera3 & "\" & ComboBox1.SelectedItem.ToString
        Process.Start(perfil)
    End Sub

    Private Sub CheckBox1_CheckedChanged(sender As Object, e As EventArgs) Handles CheckBox1.CheckedChanged
        Call construirparametro()
    End Sub



    Private Sub TextBox5_TextChanged(sender As Object, e As EventArgs) Handles TextBox5.TextChanged
        Call construirparametro()

    End Sub

    Private Sub TextBox16_TextChanged(sender As Object, e As EventArgs) Handles TextBox16.TextChanged
        Call construirparametro()

    End Sub

    Private Sub TextBox17_TextChanged(sender As Object, e As EventArgs) Handles TextBox17.TextChanged
        Call construirparametro()

    End Sub

    Private Sub TextBox2_TextChanged(sender As Object, e As EventArgs) Handles TextBox2.TextChanged
        Call construirparametro()

    End Sub

    Private Sub GroupBox7_Enter(sender As Object, e As EventArgs)

    End Sub

    Private Sub Button26_Click_1(sender As Object, e As EventArgs) Handles Button26.Click
        Dim fd As OpenFileDialog = New OpenFileDialog()
        Dim strFileName As String
        fd.Title = "Selecciona la ruta de Arma 3 Server.exe"
        fd.Filter = "All files (*.*)|*.*|All files (*.*)|*.*"
        fd.FilterIndex = 2
        fd.RestoreDirectory = True
        If fd.ShowDialog() = DialogResult.OK Then
            strFileName = fd.FileName
            TextBox3.Text = strFileName
        End If
    End Sub

    Private Sub Button32_Click(sender As Object, e As EventArgs) Handles Button32.Click
        Dim fd As OpenFileDialog = New OpenFileDialog()
        Dim strFileName As String
        fd.Title = "Selecciona la ruta de Arma 3 .exe"
        fd.Filter = "All files (*.*)|*.*|All files (*.*)|*.*"
        fd.FilterIndex = 2
        fd.RestoreDirectory = True
        If fd.ShowDialog() = DialogResult.OK Then
            strFileName = fd.FileName
            TextBox4.Text = strFileName
        End If
    End Sub

    Private Sub Button33_Click(sender As Object, e As EventArgs) Handles Button33.Click
        FolderBrowserDialog1.Description = "Selecciona un directorio de addons de Arma 3"
        If FolderBrowserDialog1.ShowDialog() = DialogResult.OK Then
            TextBox44.Text = FolderBrowserDialog1.SelectedPath
        End If
    End Sub

    Private Sub CheckBox10_CheckedChanged(sender As Object, e As EventArgs) Handles CheckBox10.CheckedChanged
        If CheckBox10.Checked = True Then
            TextBox44.Enabled = True
            TextBox4.Enabled = True
            TextBox3.Enabled = True
            Label83.Visible = False
            Label85.Visible = False
            Label86.Visible = False
            Label83.Enabled = False
            Label85.Enabled = False
            Label86.Enabled = False
            Button26.Enabled = True
            Button32.Enabled = True
            Button33.Enabled = True
        Else
            TextBox44.Enabled = False
            TextBox4.Enabled = False
            TextBox3.Enabled = False
            TextBox44.Text = Nothing
            TextBox4.Text = Nothing
            TextBox3.Text = Nothing
            Label83.Visible = True
            Label85.Visible = True
            Label86.Visible = True
            Label83.Enabled = True
            Label85.Enabled = True
            Label86.Enabled = True
            Button26.Enabled = False
            Button32.Enabled = False
            Button33.Enabled = False
        End If
    End Sub

    Private Sub TextBox43_TextChanged(sender As Object, e As EventArgs) Handles TextBox43.TextChanged
        For Each spaceline As String In TextBox43.Text
            TextBox43.Text = TextBox43.Text.Replace(vbCrLf, "")
        Next 'this is for the space line


        For Each space As String In TextBox43.Text
            TextBox43.Text = TextBox43.Text.Replace(" ", "")
        Next
    End Sub




    Private Sub CheckedListBox7_SelectedIndexChanged(sender As Object, e As EventArgs) Handles CheckedListBox7.SelectedIndexChanged

    End Sub




    Sub inicioperfil()
        If ComboBox10.Visible = True Then
            Dim treintayuno As String = Nothing
            Dim treintaydos As String = Nothing
            Dim treintaytres As String = Nothing
            Dim treintaycuatro As String = Nothing
            Dim sesentaycinco As String = Nothing
            Dim sesentayseis As String = Nothing
            Dim sesentaysiete As String = Nothing
            Dim sesentayocho As String = Nothing
            Dim noventaynueve As String = Nothing
            Dim cien As String = Nothing
            Dim cientouno As String = Nothing
            Dim cientodos As String = Nothing

            Dim cientrotreintaynueve As String = Nothing
            Dim index6 = ComboBox10.SelectedIndex
            Select Case index6
                Case 0
                    TextBox22.Text = "0.50"
                    TextBox26.Text = "0.20"
                    TextBox22.Enabled = False
                    TextBox23.Enabled = False
                    TextBox24.Enabled = False
                    TextBox25.Enabled = False
                    TextBox26.Enabled = False
                    TextBox27.Enabled = False
                    TextBox28.Enabled = False
                    TextBox29.Enabled = False
                    TextBox22.Visible = True
                    TextBox26.Visible = True
                    TextBox23.Visible = False
                    TextBox24.Visible = False
                    TextBox25.Visible = False
                    TextBox27.Visible = False
                    TextBox28.Visible = False
                    TextBox29.Visible = False
                Case 1
                    TextBox23.Text = "0.70"
                    TextBox27.Text = "0.50"
                    TextBox22.Enabled = False
                    TextBox23.Enabled = False
                    TextBox24.Enabled = False
                    TextBox25.Enabled = False
                    TextBox26.Enabled = False
                    TextBox27.Enabled = False
                    TextBox28.Enabled = False
                    TextBox29.Enabled = False
                    TextBox22.Visible = False
                    TextBox26.Visible = False
                    TextBox23.Visible = True
                    TextBox24.Visible = False
                    TextBox25.Visible = False
                    TextBox27.Visible = True
                    TextBox28.Visible = False
                    TextBox29.Visible = False
                Case 2
                    TextBox24.Text = "0.80"
                    TextBox28.Text = "0.70"
                    TextBox22.Enabled = False
                    TextBox23.Enabled = False
                    TextBox24.Enabled = False
                    TextBox25.Enabled = False
                    TextBox26.Enabled = False
                    TextBox27.Enabled = False
                    TextBox28.Enabled = False
                    TextBox29.Enabled = False
                    TextBox22.Visible = False
                    TextBox26.Visible = False
                    TextBox23.Visible = False
                    TextBox24.Visible = True
                    TextBox25.Visible = False
                    TextBox27.Visible = False
                    TextBox28.Visible = True
                    TextBox29.Visible = False
                Case 3
                    Dim cientotreintaycuatro As String
                    Dim cientotreintaycinco As String
                    cientotreintaycuatro = System.IO.File.ReadAllLines(dificultad)(134)
                    cientotreintaycinco = System.IO.File.ReadAllLines(dificultad)(135)
                    cientotreintaycuatro = Replace(cientotreintaycuatro, "skillAI=", "")
                    cientotreintaycuatro = Replace(cientotreintaycuatro, ";", "")
                    cientotreintaycinco = Replace(cientotreintaycinco, "precisionAI=", "")
                    cientotreintaycinco = Replace(cientotreintaycinco, ";", "")
                    cientotreintaycuatro = System.Text.RegularExpressions.Regex.Replace(cientotreintaycuatro, "\s{2,}", " ")
                    cientotreintaycinco = System.Text.RegularExpressions.Regex.Replace(cientotreintaycinco, "\s{2,}", " ")
                    If cientotreintaycuatro.Contains("}") Then
                        Dim cientotreintayuno As String
                        Dim cientotreintaydos As String
                        cientotreintayuno = System.IO.File.ReadAllLines(dificultad)(131)
                        cientotreintaydos = System.IO.File.ReadAllLines(dificultad)(132)
                        cientotreintayuno = Replace(cientotreintayuno, "skillAI=", "")
                        cientotreintayuno = Replace(cientotreintayuno, ";", "")
                        cientotreintaydos = Replace(cientotreintaydos, "precisionAI=", "")
                        cientotreintaydos = Replace(cientotreintaydos, ";", "")
                        cientotreintayuno = System.Text.RegularExpressions.Regex.Replace(cientotreintayuno, "\s{2,}", " ")
                        cientotreintaydos = System.Text.RegularExpressions.Regex.Replace(cientotreintaydos, "\s{2,}", " ")

                    Else

                    End If

                    TextBox22.Enabled = True
                    TextBox23.Enabled = True
                    TextBox24.Enabled = True
                    TextBox25.Enabled = True
                    TextBox26.Enabled = True
                    TextBox27.Enabled = True
                    TextBox28.Enabled = True
                    TextBox29.Enabled = True
                    TextBox22.Visible = False
                    TextBox26.Visible = False
                    TextBox23.Visible = False
                    TextBox24.Visible = False
                    TextBox25.Visible = True
                    TextBox27.Visible = False
                    TextBox28.Visible = False
                    TextBox29.Visible = True
            End Select
        End If
        If ComboBox9.Visible = True Then
            Dim treintayuno As String = Nothing
            Dim treintaydos As String = Nothing
            Dim treintaytres As String = Nothing
            Dim treintaycuatro As String = Nothing
            Dim sesentaycinco As String = Nothing
            Dim sesentayseis As String = Nothing
            Dim sesentaysiete As String = Nothing
            Dim sesentayocho As String = Nothing
            Dim noventaynueve As String = Nothing
            Dim cien As String = Nothing
            Dim cientouno As String = Nothing
            Dim cientodos As String = Nothing

            Dim cientrotreintaynueve As String = Nothing
            Dim index6 = ComboBox9.SelectedIndex
            Select Case index6
                Case 0
                    TextBox22.Text = "0.50"
                    TextBox26.Text = "0.20"
                    TextBox22.Enabled = False
                    TextBox23.Enabled = False
                    TextBox24.Enabled = False
                    TextBox25.Enabled = False
                    TextBox26.Enabled = False
                    TextBox27.Enabled = False
                    TextBox28.Enabled = False
                    TextBox29.Enabled = False
                    TextBox22.Visible = True
                    TextBox26.Visible = True
                    TextBox23.Visible = False
                    TextBox24.Visible = False
                    TextBox25.Visible = False
                    TextBox27.Visible = False
                    TextBox28.Visible = False
                    TextBox29.Visible = False
                Case 1
                    TextBox23.Text = "0.70"
                    TextBox27.Text = "0.50"
                    TextBox22.Enabled = False
                    TextBox23.Enabled = False
                    TextBox24.Enabled = False
                    TextBox25.Enabled = False
                    TextBox26.Enabled = False
                    TextBox27.Enabled = False
                    TextBox28.Enabled = False
                    TextBox29.Enabled = False
                    TextBox22.Visible = False
                    TextBox26.Visible = False
                    TextBox23.Visible = True
                    TextBox24.Visible = False
                    TextBox25.Visible = False
                    TextBox27.Visible = True
                    TextBox28.Visible = False
                    TextBox29.Visible = False
                Case 2
                    TextBox24.Text = "0.80"
                    TextBox28.Text = "0.70"
                    TextBox22.Enabled = False
                    TextBox23.Enabled = False
                    TextBox24.Enabled = False
                    TextBox25.Enabled = False
                    TextBox26.Enabled = False
                    TextBox27.Enabled = False
                    TextBox28.Enabled = False
                    TextBox29.Enabled = False
                    TextBox22.Visible = False
                    TextBox26.Visible = False
                    TextBox23.Visible = False
                    TextBox24.Visible = True
                    TextBox25.Visible = False
                    TextBox27.Visible = False
                    TextBox28.Visible = True
                    TextBox29.Visible = False
                Case 3
                    Dim cientotreintaycuatro As String
                    Dim cientotreintaycinco As String
                    cientotreintaycuatro = System.IO.File.ReadAllLines(dificultad)(134)
                    cientotreintaycinco = System.IO.File.ReadAllLines(dificultad)(135)
                    cientotreintaycuatro = Replace(cientotreintaycuatro, "skillAI=", "")
                    cientotreintaycuatro = Replace(cientotreintaycuatro, ";", "")
                    cientotreintaycinco = Replace(cientotreintaycinco, "precisionAI=", "")
                    cientotreintaycinco = Replace(cientotreintaycinco, ";", "")
                    cientotreintaycuatro = System.Text.RegularExpressions.Regex.Replace(cientotreintaycuatro, "\s{2,}", " ")
                    cientotreintaycinco = System.Text.RegularExpressions.Regex.Replace(cientotreintaycinco, "\s{2,}", " ")
                    If cientotreintaycuatro.Contains("}") Then
                        Dim cientotreintayuno As String
                        Dim cientotreintaydos As String
                        cientotreintayuno = System.IO.File.ReadAllLines(dificultad)(131)
                        cientotreintaydos = System.IO.File.ReadAllLines(dificultad)(132)
                        cientotreintayuno = Replace(cientotreintayuno, "skillAI=", "")
                        cientotreintayuno = Replace(cientotreintayuno, ";", "")
                        cientotreintaydos = Replace(cientotreintaydos, "precisionAI=", "")
                        cientotreintaydos = Replace(cientotreintaydos, ";", "")
                        cientotreintayuno = System.Text.RegularExpressions.Regex.Replace(cientotreintayuno, "\s{2,}", " ")
                        cientotreintaydos = System.Text.RegularExpressions.Regex.Replace(cientotreintaydos, "\s{2,}", " ")

                    Else

                    End If
                    TextBox22.Enabled = True
                    TextBox23.Enabled = True
                    TextBox24.Enabled = True
                    TextBox25.Enabled = True
                    TextBox26.Enabled = True
                    TextBox27.Enabled = True
                    TextBox28.Enabled = True
                    TextBox29.Enabled = True
                    TextBox22.Visible = False
                    TextBox26.Visible = False
                    TextBox23.Visible = False
                    TextBox24.Visible = False
                    TextBox25.Visible = True
                    TextBox27.Visible = False
                    TextBox28.Visible = False
                    TextBox29.Visible = True
            End Select
        End If
        If ComboBox11.Visible = True Then
            Dim treintayuno As String = Nothing
            Dim treintaydos As String = Nothing
            Dim treintaytres As String = Nothing
            Dim treintaycuatro As String = Nothing
            Dim sesentaycinco As String = Nothing
            Dim sesentayseis As String = Nothing
            Dim sesentaysiete As String = Nothing
            Dim sesentayocho As String = Nothing
            Dim noventaynueve As String = Nothing
            Dim cien As String = Nothing
            Dim cientouno As String = Nothing
            Dim cientodos As String = Nothing

            Dim cientrotreintaynueve As String = Nothing
            Dim index6 = ComboBox11.SelectedIndex
            Select Case index6
                Case 0
                    TextBox22.Text = "0.50"
                    TextBox26.Text = "0.20"
                    TextBox22.Enabled = False
                    TextBox23.Enabled = False
                    TextBox24.Enabled = False
                    TextBox25.Enabled = False
                    TextBox26.Enabled = False
                    TextBox27.Enabled = False
                    TextBox28.Enabled = False
                    TextBox29.Enabled = False
                    TextBox22.Visible = True
                    TextBox26.Visible = True
                    TextBox23.Visible = False
                    TextBox24.Visible = False
                    TextBox25.Visible = False
                    TextBox27.Visible = False
                    TextBox28.Visible = False
                    TextBox29.Visible = False
                Case 1
                    TextBox23.Text = "0.70"
                    TextBox27.Text = "0.50"
                    TextBox22.Enabled = False
                    TextBox23.Enabled = False
                    TextBox24.Enabled = False
                    TextBox25.Enabled = False
                    TextBox26.Enabled = False
                    TextBox27.Enabled = False
                    TextBox28.Enabled = False
                    TextBox29.Enabled = False
                    TextBox22.Visible = False
                    TextBox26.Visible = False
                    TextBox23.Visible = True
                    TextBox24.Visible = False
                    TextBox25.Visible = False
                    TextBox27.Visible = True
                    TextBox28.Visible = False
                    TextBox29.Visible = False
                Case 2
                    TextBox24.Text = "0.80"
                    TextBox28.Text = "0.70"
                    TextBox22.Enabled = False
                    TextBox23.Enabled = False
                    TextBox24.Enabled = False
                    TextBox25.Enabled = False
                    TextBox26.Enabled = False
                    TextBox27.Enabled = False
                    TextBox28.Enabled = False
                    TextBox29.Enabled = False
                    TextBox22.Visible = False
                    TextBox26.Visible = False
                    TextBox23.Visible = False
                    TextBox24.Visible = True
                    TextBox25.Visible = False
                    TextBox27.Visible = False
                    TextBox28.Visible = True
                    TextBox29.Visible = False
                Case 3
                    Dim cientotreintaycuatro As String
                    Dim cientotreintaycinco As String
                    cientotreintaycuatro = System.IO.File.ReadAllLines(dificultad)(134)
                    cientotreintaycinco = System.IO.File.ReadAllLines(dificultad)(135)
                    cientotreintaycuatro = Replace(cientotreintaycuatro, "skillAI=", "")
                    cientotreintaycuatro = Replace(cientotreintaycuatro, ";", "")
                    cientotreintaycinco = Replace(cientotreintaycinco, "precisionAI=", "")
                    cientotreintaycinco = Replace(cientotreintaycinco, ";", "")
                    cientotreintaycuatro = System.Text.RegularExpressions.Regex.Replace(cientotreintaycuatro, "\s{2,}", " ")
                    cientotreintaycinco = System.Text.RegularExpressions.Regex.Replace(cientotreintaycinco, "\s{2,}", " ")
                    If cientotreintaycuatro.Contains("}") Then
                        Dim cientotreintayuno As String
                        Dim cientotreintaydos As String
                        cientotreintayuno = System.IO.File.ReadAllLines(dificultad)(131)
                        cientotreintaydos = System.IO.File.ReadAllLines(dificultad)(132)
                        cientotreintayuno = Replace(cientotreintayuno, "skillAI=", "")
                        cientotreintayuno = Replace(cientotreintayuno, ";", "")
                        cientotreintaydos = Replace(cientotreintaydos, "precisionAI=", "")
                        cientotreintaydos = Replace(cientotreintaydos, ";", "")
                        cientotreintayuno = System.Text.RegularExpressions.Regex.Replace(cientotreintayuno, "\s{2,}", " ")
                        cientotreintaydos = System.Text.RegularExpressions.Regex.Replace(cientotreintaydos, "\s{2,}", " ")

                    Else

                    End If
                    TextBox22.Enabled = True
                    TextBox23.Enabled = True
                    TextBox24.Enabled = True
                    TextBox25.Enabled = True
                    TextBox26.Enabled = True
                    TextBox27.Enabled = True
                    TextBox28.Enabled = True
                    TextBox29.Enabled = True
                    TextBox22.Visible = False
                    TextBox26.Visible = False
                    TextBox23.Visible = False
                    TextBox24.Visible = False
                    TextBox25.Visible = True
                    TextBox27.Visible = False
                    TextBox28.Visible = False
                    TextBox29.Visible = True
            End Select
        End If
        If ComboBox8.Visible = True Then
            Dim treintayuno As String = Nothing
            Dim treintaydos As String = Nothing
            Dim treintaytres As String = Nothing
            Dim treintaycuatro As String = Nothing
            Dim sesentaycinco As String = Nothing
            Dim sesentayseis As String = Nothing
            Dim sesentaysiete As String = Nothing
            Dim sesentayocho As String = Nothing
            Dim noventaynueve As String = Nothing
            Dim cien As String = Nothing
            Dim cientouno As String = Nothing
            Dim cientodos As String = Nothing

            Dim cientrotreintaynueve As String = Nothing
            Dim index6 = ComboBox8.SelectedIndex
            Select Case index6
                Case 0
                    TextBox22.Text = "0.50"
                    TextBox26.Text = "0.20"
                    TextBox22.Enabled = False
                    TextBox23.Enabled = False
                    TextBox24.Enabled = False
                    TextBox25.Enabled = False
                    TextBox26.Enabled = False
                    TextBox27.Enabled = False
                    TextBox28.Enabled = False
                    TextBox29.Enabled = False
                    TextBox22.Visible = True
                    TextBox26.Visible = True
                    TextBox23.Visible = False
                    TextBox24.Visible = False
                    TextBox25.Visible = False
                    TextBox27.Visible = False
                    TextBox28.Visible = False
                    TextBox29.Visible = False
                Case 1
                    TextBox23.Text = "0.70"
                    TextBox27.Text = "0.50"
                    TextBox22.Enabled = False
                    TextBox23.Enabled = False
                    TextBox24.Enabled = False
                    TextBox25.Enabled = False
                    TextBox26.Enabled = False
                    TextBox27.Enabled = False
                    TextBox28.Enabled = False
                    TextBox29.Enabled = False
                    TextBox22.Visible = False
                    TextBox26.Visible = False
                    TextBox23.Visible = True
                    TextBox24.Visible = False
                    TextBox25.Visible = False
                    TextBox27.Visible = True
                    TextBox28.Visible = False
                    TextBox29.Visible = False
                Case 2
                    TextBox24.Text = "0.80"
                    TextBox28.Text = "0.70"
                    TextBox22.Enabled = False
                    TextBox23.Enabled = False
                    TextBox24.Enabled = False
                    TextBox25.Enabled = False
                    TextBox26.Enabled = False
                    TextBox27.Enabled = False
                    TextBox28.Enabled = False
                    TextBox29.Enabled = False
                    TextBox22.Visible = False
                    TextBox26.Visible = False
                    TextBox23.Visible = False
                    TextBox24.Visible = True
                    TextBox25.Visible = False
                    TextBox27.Visible = False
                    TextBox28.Visible = True
                    TextBox29.Visible = False
                Case 3
                    Dim cientotreintaycuatro As String
                    Dim cientotreintaycinco As String
                    cientotreintaycuatro = System.IO.File.ReadAllLines(dificultad)(134)
                    cientotreintaycinco = System.IO.File.ReadAllLines(dificultad)(135)
                    cientotreintaycuatro = Replace(cientotreintaycuatro, "skillAI=", "")
                    cientotreintaycuatro = Replace(cientotreintaycuatro, ";", "")
                    cientotreintaycinco = Replace(cientotreintaycinco, "precisionAI=", "")
                    cientotreintaycinco = Replace(cientotreintaycinco, ";", "")
                    cientotreintaycuatro = System.Text.RegularExpressions.Regex.Replace(cientotreintaycuatro, "\s{2,}", " ")
                    cientotreintaycinco = System.Text.RegularExpressions.Regex.Replace(cientotreintaycinco, "\s{2,}", " ")
                    If cientotreintaycuatro.Contains("}") Then
                        Dim cientotreintayuno As String
                        Dim cientotreintaydos As String
                        cientotreintayuno = System.IO.File.ReadAllLines(dificultad)(131)
                        cientotreintaydos = System.IO.File.ReadAllLines(dificultad)(132)
                        cientotreintayuno = Replace(cientotreintayuno, "skillAI=", "")
                        cientotreintayuno = Replace(cientotreintayuno, ";", "")
                        cientotreintaydos = Replace(cientotreintaydos, "precisionAI=", "")
                        cientotreintaydos = Replace(cientotreintaydos, ";", "")
                        cientotreintayuno = System.Text.RegularExpressions.Regex.Replace(cientotreintayuno, "\s{2,}", " ")
                        cientotreintaydos = System.Text.RegularExpressions.Regex.Replace(cientotreintaydos, "\s{2,}", " ")

                    ElseIf cientotreintaycuatro.Contains("skillAI=") Then

                    Else

                    End If
                    TextBox22.Enabled = True
                    TextBox23.Enabled = True
                    TextBox24.Enabled = True
                    TextBox25.Enabled = True
                    TextBox26.Enabled = True
                    TextBox27.Enabled = True
                    TextBox28.Enabled = True
                    TextBox29.Enabled = True
                    TextBox22.Visible = False
                    TextBox26.Visible = False
                    TextBox23.Visible = False
                    TextBox24.Visible = False
                    TextBox25.Visible = True
                    TextBox27.Visible = False
                    TextBox28.Visible = False
                    TextBox29.Visible = True
            End Select
        End If

    End Sub

    Private Sub ComboBox10_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ComboBox10.SelectedIndexChanged
        Call inicioperfil()
    End Sub

    Private Sub ComboBox8_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ComboBox8.SelectedIndexChanged
        Call inicioperfil()

    End Sub

    Private Sub ComboBox11_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ComboBox11.SelectedIndexChanged
        Call inicioperfil()

    End Sub

    Private Sub ComboBox9_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ComboBox9.SelectedIndexChanged
        Call inicioperfil()

    End Sub

    Private Sub CheckBox9_CheckedChanged(sender As Object, e As EventArgs) Handles CheckBox9.CheckedChanged

    End Sub

    Private Sub TextBox24_TextChanged(sender As Object, e As EventArgs) Handles TextBox24.TextChanged

    End Sub

    Private Sub CheckBox11_CheckedChanged(sender As Object, e As EventArgs) Handles CheckBox11.CheckedChanged
        Call hc()
    End Sub

    Private Sub GroupBox15_Enter(sender As Object, e As EventArgs) Handles GroupBox15.Enter

    End Sub

    Private Sub Button34_Click(sender As Object, e As EventArgs) Handles Button34.Click
        Dim rptúltimo As String
        rptúltimo = rutacarpetaa3 & "\EsusServerA3" & "\" & ComboBox1.SelectedItem.ToString
        Dim leido As String = Nothing
        Dim dirinfo As DirectoryInfo

        Dim allFiles() As FileInfo

        dirinfo = New DirectoryInfo(rptúltimo)

        allFiles = dirinfo.GetFiles("*.rpt")

        Array.Sort(allFiles, New clsCompareFileInfo)

        For Each fl As FileInfo In allFiles

            leido = fl.FullName.ToString()
        Next
        Try
            Process.Start(leido)
        Catch ex As Exception


        End Try
    End Sub

    Private Sub Button35_Click(sender As Object, e As EventArgs) Handles Button35.Click
        Dim rptúltimo As String
        rptúltimo = rutacarpetaa3 & "\EsusServerA3" & "\" & ComboBox1.SelectedItem.ToString
        Dim leido As String = Nothing
        Dim dirinfo As DirectoryInfo

        Dim allFiles() As FileInfo

        dirinfo = New DirectoryInfo(rptúltimo)

        allFiles = dirinfo.GetFiles("*.log")

        Array.Sort(allFiles, New clsCompareFileInfo)

        For Each fl As FileInfo In allFiles

            leido = fl.FullName.ToString()
        Next
        Try
            Process.Start(leido)
        Catch ex As Exception


        End Try
    End Sub

    Private Sub Button36_Click(sender As Object, e As EventArgs) Handles Button36.Click
        Process.Start(rutaaddons)

    End Sub

    Private Sub TextBox62_TextChanged(sender As Object, e As EventArgs) Handles TextBox62.TextChanged
        Call hc()

    End Sub

    Private Sub TextBox63_TextChanged(sender As Object, e As EventArgs) Handles TextBox63.TextChanged
        Call hc()
    End Sub

    Private Sub TextBox60_TextChanged(sender As Object, e As EventArgs) Handles TextBox60.TextChanged
        Call hc()

    End Sub

    Private Sub TextBox61_TextChanged(sender As Object, e As EventArgs) Handles TextBox61.TextChanged
        Call hc()

    End Sub

    Private Sub TextBox18_TextChanged(sender As Object, e As EventArgs) Handles TextBox18.TextChanged
        Call hc()

    End Sub

    Private Sub CheckBox21_CheckedChanged(sender As Object, e As EventArgs) Handles CheckBox21.CheckedChanged
        Call construirparametro()
    End Sub

    Private Sub lanzarserver_Click(sender As Object, e As EventArgs) Handles lanzarserver.Click
        Call construirparametro()
        If CheckBox9.Checked = True Then
            Dim process As System.Diagnostics.Process = Nothing
            Dim processStartInfo As System.Diagnostics.ProcessStartInfo
            processStartInfo = New System.Diagnostics.ProcessStartInfo()
            processStartInfo.FileName = exearma3server
            If System.Environment.OSVersion.Version.Major >= 6 Then ' Windows Vista or higher
                processStartInfo.Verb = "runas"
            Else
                ' No need to prompt to run as admin
            End If
            processStartInfo.Arguments = TextBox1.Text
            processStartInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Normal
            processStartInfo.UseShellExecute = True
            Try
                process = System.Diagnostics.Process.Start(processStartInfo)
            Catch ex As Exception

            Finally

                If Not (process Is Nothing) Then
                    process.Dispose()
                End If
            End Try
        Else
            Process.Start(exearma3server, TextBox1.Text)
        End If
    End Sub


    Private Sub Button41_Click(sender As Object, e As EventArgs) 

    End Sub
End Class

