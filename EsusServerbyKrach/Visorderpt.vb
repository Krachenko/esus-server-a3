﻿Imports System.IO
Imports System.Text

Public Class VisordeRpt
    Dim steam As String = My.Computer.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\Valve\Steam", "InstallPath", Nothing)
    Dim rutaarma As String = steam & "\steamapps\common\Arma 3"
    Dim esusservera3 As String
    Private Sub VisordeRpt_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        If Form1.CheckBox10.Checked = True Then
            rutaarma = Form1.TextBox44.Text
        Else
            steam = My.Computer.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\Valve\Steam", "InstallPath", Nothing)
            rutaarma = steam & "\steamapps\common\Arma 3"
        End If
        esusservera3 = rutaarma & "\EsusServerA3" & "\" & Form1.ComboBox1.SelectedItem.ToString
        If CheckBox1.Checked = True Then
            Timer1.Interval = 2000 '3 seconds
            Timer1.Enabled = True
        Else
            Timer1.Stop()
            Timer1.Enabled = False
        End If
        Call leerrpt()
    End Sub
    Sub leerrpt()

        Try
            Dim leido As String = Nothing
            Dim dirinfo As DirectoryInfo

            Dim allFiles() As FileInfo

            dirinfo = New DirectoryInfo(esusservera3)

            allFiles = dirinfo.GetFiles("*.rpt")

            Array.Sort(allFiles, New clsCompareFileInfo)

            For Each fl As FileInfo In allFiles

                leido = fl.FullName.ToString()
            Next

            Dim LogFileStream As FileStream
            Dim LogFileReader As StreamReader

            LogFileStream = New FileStream(leido, FileMode.Open, FileAccess.Read, FileShare.ReadWrite)
            LogFileReader = New StreamReader(LogFileStream)
            RichTextBox1.Text = LogFileReader.ReadToEnd()
            LogFileReader.Close()
            RichTextBox1.SelectionStart = RichTextBox1.TextLength
            RichTextBox1.ScrollToCaret()
        Catch ex As Exception

        End Try

    End Sub

    Private Sub CheckBox1_CheckedChanged(sender As Object, e As EventArgs) Handles CheckBox1.CheckedChanged
        If CheckBox1.Checked = True Then
            Timer1.Interval = 2000 '3 seconds
            Timer1.Enabled = True
        Else
            Timer1.Stop()
            Timer1.Enabled = False
        End If

        Call leerrpt()
    End Sub

    Private Sub Timer1_Tick(sender As Object, e As EventArgs) Handles Timer1.Tick
        Call leerrpt()
    End Sub

    Private Sub CheckBox1_CheckedChanged_1(sender As Object, e As EventArgs) Handles CheckBox1.CheckedChanged
        My.Settings.refrescarrpt = CheckBox1.CheckState
        My.Settings.Save()
    End Sub



End Class